<?php

class EquipeController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $dados = array();

        // $funcionario = new Funcionario();

        //BUSCAR OS DIRETORES
        // $funcionarios = $funcionario->getFuncionaros(1);
        // $atendimentoNovaSerrana = $funcionario->getFuncionaros(2);
        // $atendimentoNovaPerdigao = $funcionario->getFuncionaros(3);
        // $corretores = $funcionario->getFuncionaros(4);
        // $vistoriadores = $funcionario->getFuncionaros(5);
        // $financeiro = $funcionario->getFuncionaros(7);
        // $atendimento = $funcionario->getFuncionaros(8);
        // $reclamacao = $funcionario->getFuncionaros(6);
        // $ti = $funcionario->getFuncionaros(9);




        // $dados['funcionarios'] = $funcionarios;
        // $dados['atendimentoNovaSerrana'] = $atendimentoNovaSerrana;
        // $dados['atendimentoNovaPerdigao'] = $atendimentoNovaPerdigao;
        // $dados['corretores'] = $corretores;
        // $dados['vistoriadores'] = $vistoriadores;
        // $dados['financeiro'] = $financeiro;
        // $dados['atendimento'] = $atendimento;
        // $dados['ti'] = $ti;
        // $dados['reclamacao'] = $reclamacao;

        $dados['titlePagina'] = 'Nossa Equipe - '.  TITULO_AUXILIAR;

        $this->loadTemplate('equipe', $dados);
    }

    public function getFuncionario($func,$cargo) {
        

        $dados = array('funcionario' =>'','titlePagina'=>'');
   
        $fun= new Funcionario();
        
        $dados['funcionario'] = $fun->getFuncionario($func,$cargo);
        $dados['titlePagina'] = 'Jomar Imobiliária - Aluguel e Venda de Imóveis | ' .$dados['funcionario']['nome_funcioario'];

        

        $this->loadTemplate('equipe-detalhe', $dados);
    }

}
