<?php

class HomeController extends Controller {
    
    //CLIENTE JOMAR
    public $chave = CHAVE;

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $dados = array();
        
        $dados['titlePagina'] = TITULO_AUXILIAR;

        $this->loadView('home2', $dados);
    }

}
