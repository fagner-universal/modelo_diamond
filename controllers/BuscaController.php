<?php

class BuscaController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $dados = array();

        $dados['titlePagina'] = 'Buscar Imóvel - '. TITULO_AUXILIAR ;

        $this->loadView('busca', $dados);
    }

    public function busca2() {

        $dados = array();


        $dados['titlePagina'] = TITULO_AUXILIAR;

        $this->loadTemplate('busca2', $dados);
    }

    public function filtrar() {

        $parametro = $_POST['texto'];

        $dados = array();

        $busca = new Buscar();
        $dados = $busca->buscar($parametro);


        header("Content-Type:application/json");
        echo json_encode($dados);
    }

}
