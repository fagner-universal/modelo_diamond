<?php

class SobreController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $dados = array();
        
        
        //DEPOIMENTOS DE CLIENTES
        // $depoimentos = new Depoimentos_model();
        // $dados['depoimentos']  = $depoimentos->getDepoimentos();
        
        
        
        $dados['titlePagina'] ='Sobre a '. TITULO_AUXILIAR ;

        $this->loadTemplate('sobre', $dados);
    }

}
