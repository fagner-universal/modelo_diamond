<?php

class DestaquesController extends Controller {

    public $finalidade = 1;
    //CLIENTE JOMAR
    public $chave = CHAVE;


    public function __construct() {
        parent::__construct();
    }

    public function getDestaqueAluguel() {

        $imoveis = new Destaques_model(new Api(), 1);

        // $imoveis->numeroRegistros(10);
        $imoveis->setFinalidade(1); //VENDA
        $imoveisDestaqueVenda = $imoveis->getImoveisDestaques();
        $imoveisDestaqueVenda = json_decode($imoveisDestaqueVenda);
        $imoveisDestaqueVenda = (array) $imoveisDestaqueVenda;

        $objTitulo = new UrlAmigavel();

        foreach ($imoveisDestaqueVenda['lista'] as $key => $value) {

            $titulo = $objTitulo->gerarTitulo($value->titulo);
  
            $imoveisDestaqueVenda['lista'][$key]->titulo = $titulo;
        }
  

        header('Content-Type: application/json');
        echo json_encode($imoveisDestaqueVenda);

    
    }

    public function getDestaquesVenda(){

        $dados = array();

        $imoveis = new Destaques_model(new Api(), 2);
        // $imoveis->numeroRegistros(10);
        $imoveis->setFinalidade(2); //VENDA
        $imoveisDestaqueVenda = $imoveis->getImoveisDestaques();
        $imoveisDestaqueVenda = json_decode($imoveisDestaqueVenda);
        $imoveisDestaqueVenda = (array) $imoveisDestaqueVenda;

        $objTitulo = new UrlAmigavel();

        foreach ($imoveisDestaqueVenda['lista'] as $key => $value) {

            $titulo = $objTitulo->gerarTitulo($value->titulo);
  
            $imoveisDestaqueVenda['lista'][$key]->titulo = $titulo;
        }

        
        header('Content-Type: application/json');
        echo json_encode($imoveisDestaqueVenda);

        
    }

    public function getLancamentos(){

        $dados = array();

        $imoveis = new Destaques_model(new Api(), 2);
        // $imoveis->numeroRegistros(10);
        $imoveis->setFinalidade(2); //VENDA
        $imoveisDestaqueVenda = $imoveis->getImoveisLancamentos();
        $imoveisDestaqueVenda = json_decode($imoveisDestaqueVenda);
        $imoveisDestaqueVenda = (array) $imoveisDestaqueVenda;

        $objTitulo = new UrlAmigavel();

        foreach ($imoveisDestaqueVenda['lista'] as $key => $value) {

            $titulo = $objTitulo->gerarTitulo($value->titulo);
  
            $imoveisDestaqueVenda['lista'][$key]->titulo = $titulo;
        }

        
        header('Content-Type: application/json');
        echo json_encode($imoveisDestaqueVenda);
  
    }

}
