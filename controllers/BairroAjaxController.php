<?php

class BairroAjaxController extends Controller {
    
    //paramentro para remover acentos
    public   $low = array("Á" => "á", "É" => "é", "Í" => "í", "Ó" => "ó", "Ú" => "ú", "Ü" => "ü", "não" => "não","Ç" => "ç");

    //CLIENTE JOMAR
    public $chave = CHAVE;
    

    //CHAVE DEMO
   // public $chave = 'RZZuhI4HKPnkrUuToq3j8A==';
    
    public $finalidade = 0;

    public function __construct() {
        parent::__construct();
    }

    public function getBairro() {
        
    //$cidade = $_GET['cidade'];
        
    
        $api = new Api();
        $bairro = $api->GET(''.URLAPI.'/Imovel/RetornarBairrosDisponiveis?parametros={"finalidade":"'
                . $this->finalidade .'"}',''.$this->chave.'');

        $bairro = json_decode($bairro);
        

        //COLOCA TODAS AS LETRAS EM MINUSCULOS
        $array = array();
        foreach ($bairro->lista as $key => $value) {
       
            $array[] = array( 'nome' => str_replace('ç','c',$this->tirarAcentos($value->nome)), 'codigo' => $value->codigo,'cidade'=> strtolower($value->cidade));
        }
        
 
        //TIRA TODOS OS ACENTOS
        $array2 = array();
        foreach ($array as $key => $value2) {
            $array2[] = array('nome' =>  mb_strtolower(strtr($value2['nome'],$this->low),'UTF-8'), 'codigo' => $value2['codigo'],'cidade'=> $this->tirarAcentos($value2['cidade']));
        }

        
       
        //SUBSTITUE OS ESPAÇOES POR "-"
        $array3 = array();
        foreach ($array2 as $key => $value3) {
            $array3[] = array('nome' => str_replace(' ', '-', $value3['nome']), 'codigo' => $value3['codigo'],'cidade'=> str_replace(' ', '-',$value3['cidade']));
        }
        

 
        //INSERIR NOME ORIGINAL NO ARRAY
        foreach ($array3 as $key => $value) {
            $array3[$key]['nomeOriginal'] = $bairro->lista[$key]->nome;
            $array3[$key]['nomeCidadeOriginal'] = $bairro->lista[$key]->cidade;
        }
        

     
        header('Content-Type: application/json');
        echo json_encode($array3);
    }
    
    
    function getBairroOroginal(){
        
        $api = new Api();
        $bairro = $api->GET(''.URLAPI.'Imovel/RetornarBairrosDisponiveis?parametros={"finalidade":"'
                . $this->finalidade .'"}',''.$this->chave.'');

      
        header('Content-Type: application/json');
        echo json_encode($bairro);
        
        
    }

    public function tirarAcentos($string){
    return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/","/(Ç)/","/(ç)/"),explode(" ","a A e E i I o O u U n N C ç"),$string);
    }

}
