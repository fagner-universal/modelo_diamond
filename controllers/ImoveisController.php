<?php

class ImoveisController extends Controller {

    public $finalidade = 1;
    //CLIENTE JOMAR
    public $chave = CHAVE;

    //CHAVE DEMO
    // public $chave = 'RZZuhI4HKPnkrUuToq3j8A==';

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        //NAO UTILIZADO NO MOMENTO
    }

    public function aluguel($tipo = '', $cidade = '', $bairro = '', $quarto = 0, $numerovagas = 0, $numerobanhos = 0, $valorde = 0, $valorate = 0) {

        $resultado = array();


        $this->loadView('listagem-imoveis', $resultado);
    }

//                                              $tipo = '', $cidade = '', $bairro = '', $quarto = 0, $numerovagas = 0, $numerobanhos = 0, $valorde = 0, $valorate = 0
    public function aluguelAjax($finalidade = '', $tipo = '', $cidade = '', $bairro = '', $quarto = '', $numerovagas = '', $numerobanhos = '') {


        //----------------END URL PARA TODOS
        $valorde = 0;
        $valorate = 0;
        $pagina = 1;

        //PEGAR PAGINAÇÃO
        if (isset($_GET['pagina']) && !empty($_GET['pagina'])) {

            $pagina = $_GET['pagina'];
        }

        if (isset($_GET['valorminimo']) && !empty($_GET['valorminimo'])) {

            $valorde = $_GET['valorminimo'];
        }
        if (isset($_GET['valormaximo']) && !empty($_GET['valormaximo'])) {
            $valorate = $_GET['valormaximo'];
        }

        if(isset($_GET['ordenacao']) && !empty($_GET['ordenacao'])){

            $ordenacao = $_GET['ordenacao'];

        }else{

            $ordenacao = 'valorasc';
        }





        if ($finalidade == 'aluguel') {
            $finalidade = 1;
        } else if ($finalidade == 'venda') {
            $finalidade = 2;
        }





        $title = new Title($finalidade, $tipo, $cidade, $bairro, $quarto, $numerovagas, $numerobanhos, $valorde, $valorate);

        $tituloPagina = $title->montarTitleListagem();


        //------QUANDO URL É ACESSADO COMO TODOS----------
        //QUANDO TIPO DE IMOVEL ESTÁ COMO TODOS
        if ($tipo == 'imoveis') {
            $tipo = '';
        }





        //DEFINE O NUMERO DE QUARTOS PELA URL
        if ($quarto == '1-quarto') {

            $quarto = -1;

        } else if ($quarto == '0-quartos') {

            $quarto = 0;

        } else if ($quarto == '2-quartos') {

            $quarto = -2;

        } else if ($quarto == '3-quartos') {

            $quarto = -3;

        } else if ($quarto == '4-quartos') {

            $quarto = 4;

        } else {

            $quarto = 0;
        }





        //DEFINIR NUMERO DE banheiros

        if ($numerobanhos == '1-banheiro') {

            $numerobanhos = -1;

        }  else if ($numerobanhos == '0-banheiros') {

            $numerobanhos = 0;

        }else if ($numerobanhos == '2-banheiros') {

            $numerobanhos = -2;

        } else if ($numerobanhos == '3-banheiros') {

            $numerobanhos = -3;

        } else if ($numerobanhos == '4-banheiros') {

            $numerobanhos = 4;

        } else {
            $numerobanhos = 0;
        }



        //DEFINIR NUMERO DE VAGAS
        if ($numerovagas == '1-vaga') {
            $numerovagas = -1;
        }
        else if ($numerovagas == '0-vagas') {
            $numerovagas = 0;
        }
        else if ($numerovagas == '2-vagas') {
            $numerovagas = -2;
        } else if ($numerovagas == '3-vagas') {
            $numerovagas = -3;
        } else if ($numerovagas == '4-vagas') {
            $numerovagas = 4;
        } else {

            $numerovagas = 0;
        }


        //DEFINE A CIDADE PELA URL QUANDO ESTA MARCADO COMO TODOS
        if ($cidade == 'todas-as-cidades') {
            $cidade = '';
        }

        //BAIRRO QUANDO MARCADO TODOS
        if ($bairro == 'todos-os-bairros') {
            $bairro = '';
        }

        //QUARTOS QUANDO MARCADO TODOS
        if ($quarto == '1-quarto-ou-mais') {
            $quarto = 0;
        }


        //QUARTOS QUANDO MARCADO TODOS
        if ($numerovagas == '1-vaga-ou-mais') {

            $numerovagas = 0;
        }


        //QUARTOS QUANDO MARCADO TODOS
        if ($numerobanhos == '1-banheiro-ou-mais') {
            $numerobanhos = 0;
        }



        //pegar todos os tipos
        $url = new UrlAmigavel();
        $todasUrls = $url->urlTipo('t', 0);



        //VARIAVEL PARA PEGAR TIPO POR EXTENSO
        $tipoPorExtenso = '';
        $tipo = explode('--', $tipo);
        $cont = count($tipo);




        $arrayTipo = '';

        foreach ($tipo as $key => $t) {

            //PEGAR URL PARA TIPO DE IMOVEL
            if ($t != '') {

                foreach ($todasUrls as $key => $value) {

                    if ($t == $value['nome']) {

                        $tipoPorExtenso = $value['nome'];

                        $tipo = $value['codigo'];
                    }
                }
            }






            if ($tipo == '') {

                $tipo = '';
            } elseif (is_numeric($tipo)) {

                $tipo = (string) $tipo;
            }

            if (is_array($tipo)) {

                $tipo = '';
            }


            //gerar string com variso bairros
            $arrayTipo .= $tipo . ',';

//            var_dump($arrayTipo);exit;
        }

        $tipo = $arrayTipo;

//        //VARIAVEL PARA PEGAR TIPO POR EXTENSO
//        $tipoPorExtenso = '';
//
//
//        //PEGAR URL PARA TIPO DE IMOVEL
//        $tipoImovel = '';
//        if ($tipo != '') {
//
//            $url = new UrlAmigavel();
//            $tipoImovel = $url->urlTipo($tipo, $finalidade);
//
//            foreach ($tipoImovel as $key => $value) {
//
//                if ($tipo == $value['nome']) {
//                    $tipoPorExtenso = $value['nome'];
//                    $tipo = $value['codigo'];
//                }
//            }
//        }
//
//
//        if ($tipo == '') {
//            $tipo = '';
//        }


        //VARIAVEL POR EXTENÇO
        $cidadePorExtenso = '';

        //PEGAR URL PARA CIDADE

        $todasCidades = '';
        if ($cidade != '') {
            $url = new UrlAmigavel();

            $todasCidades = $url->urlCidade($finalidade);


            foreach ($todasCidades as $key => $value) {

                if ($cidade == $value['nome']) {

                    $cidadePorExtenso = $value['nome'];
                    $cidade = $value['cidade'];
                }
            }
        }

        if ($cidade == '') {
            $cidade = 0;
        }

        $url = new UrlAmigavel();
        $todasUrls = $url->urlBairro($cidade, 0);



        $bairroExplode = explode('--', $bairro);
        $cont = count($bairroExplode);
        unset($bairroExplode[$cont - 1]);


        $arrayBairros = '';
        foreach ($bairroExplode as $key => $bairro) {

            //PEGAR URL PARA BAIRRO
            if ($bairro != '') {

                foreach ($todasUrls as $key => $value) {

                    if ($bairro == $value['nome']) {
                        $bairro = $value['cidade'];
                    }
                }
            }

            if ($bairro == '') {
                $bairro = '';
            } elseif (is_numeric($bairro)) {

                $bairro = (string) $bairro;
            }

            //gerar string com variso bairros
            $arrayBairros .= $bairro . ',';
        }

        $bairro = $arrayBairros;


//        //PEGAR URL PARA BAIRRO
//        $todosBairros = '';
//        if ($bairro != '') {
//            $url = new UrlAmigavel();
//            $todosBairros = $url->urlBairro($bairro, $finalidade);
//
//
//            foreach ($todosBairros as $key => $value) {
//
//                if ($bairro == $value['nome']) {
//                    $bairro = $value['cidade'];
//                }
//            }
//        }
//
//        if ($bairro == '') {
//            $bairro = '';
//        }



        $api = new Api();
        $resultado = $api->GET('https://api.imoview.com.br/Imovel/RetornarImoveisDisponiveis?parametros={"finalidade":"'
            . $finalidade . '","codigocidade":"' . $cidade . '","codigoTipo":"' . $tipo . '","numeroPagina":"'
            . $pagina . '","numeroquartos":"' . (int) $quarto . '","numerovagas":"' . (int) $numerovagas . '","numerobanhos":"'
            . (int) $numerobanhos . '","valorde":"' .  $valorde . '","valorate":"' .  $valorate . '","codigosbairros":"' . $bairro . '"'
            . ',"opcaoimovel":"4","numeroRegistros":"18","ordenacao":"'.$ordenacao.'"}', '' . $this->chave . '');

        $resultado = json_decode($resultado);
        $resultado = (array) $resultado;




        $totalImoveis = $resultado['quantidade'];


        $quantidadeImoveis = ceil($totalImoveis / 18);
        $resultado['total'] = $totalImoveis;
        $resultado['paginacao'] = $quantidadeImoveis;
        $resultado['finalidade'] = $finalidade;
        $resultado['tipo'] = $tipo;
        $resultado['cidade'] = $cidadePorExtenso;
        $resultado['url'] = 'aluguel/' . $tipoPorExtenso;
        $resultado['titlePagina'] = $tituloPagina;



        header('Content-Type: application/json');
        echo json_encode($resultado);
    }
    
    
     public function getImoveis() {

        $dados = array();

        $this->loadView('listagem-imoveis', $dados);
        
    }

    
    
    public function getImoveisAjax() {

        $imo = $_POST['imovel'];
        $imo = (object) $imo;

        $url = new UrlAmigavel();
//
//        $tipoTratado = $url->urlTipo($tiposRetornados);
//        //converte para código
//        $codigoTipo = $url->converterParaCodigoTipo($tipoTratado, $tipo);
//        //ADD CÓDIGO NO OBJ IMOVEL
//        $imovel->setCodigoTipo($codigoTipo['codigo']);



        if ($imo->finalidade == 'aluguel') {
            $imo->finalidade = 1;
        } elseif ($imo->finalidade == 'venda') {

            $imo->finalidade = 2;
        }


        $imoveis = new Imovel();
        $imoveis->setFinalidade($imo->finalidade);


//        $imoveis->setCodigocidade($imo->codigocidade);
        $objCidade = new Cidade_Model(new Api(), $imo->finalidade);

        $cidades = $url->urlCidade($objCidade->getCidades());


        $codigoCidade = $url->converterParaCodigoCidade($cidades, $imo->codigocidade);


        //ADD NO OBJETO
        $imoveis->setCodigocidade($codigoCidade['codigo']);
        $imoveis->setNumerosuite($imo->numerosuite);
        $imoveis->setNumerobanhos($imo->numerobanhos);
        $imoveis->setNumeroquartos($imo->numeroquartos);
        $euxTipo = '';
        $imoveis->setCodigoCondominio($imo->codigocondominio);

        if (!isset($imo->codigoTipo['codigo']) || $imo->codigoTipo['codigo'][0] == 0 || $imo->codigoTipo == '') {

            $euxTipo = '';
        } else if ($imo->codigoTipo == '') {

            $euxTipo = '';
        } else {

            //PREPARAR TIPO PARA CHAMADA API
            foreach ($imo->codigoTipo['codigo'] as $key => $value) {

                $euxTipo .= $value . ",";
            }
        }


        //ADD CÓDIGO NO OBJ IMOVEL
        $imoveis->setCodigoTipo(substr_replace($euxTipo, '', -1));
        $imoveis->setNumerovagas($imo->numerovagas);



        $euxBairros = '';

        if (!isset($imo->codigosbairros['codigo']) || $imo->codigosbairros['codigo'] == 0 || $imo->codigosbairros == '') {

            $euxTipo = '';
        } else if ($imo->codigosbairros == '') {
            $imo->codigosbairros = '';
        } else {

            //PREPARAR TIPO PARA CHAMADA API
            foreach ($imo->codigosbairros['codigo'] as $key => $value) {

                $euxBairros .= $value . ",";
            }
        }


        $imoveis->setCodigosbairros(substr_replace($euxBairros, '', -1));


        $imoveis->setValorde($imo->valorde);
        $imoveis->setValorate($imo->valorate);



        $tipo_requisicao = 'url';

        $pagina = $imo->pagina;

        $valorde = '';
        $valorate = '';




        if (isset($_GET['url']) && !empty($_GET['url'])) {
            $tipo_requisicao = $_GET['url'];
        }

        if (isset($_GET['pagina']) && !empty($_GET['pagina'])) {
            $pagina = $_GET['pagina'];
        }

        if (isset($_GET['valorminimo']) && !empty($_GET['valorminimo'])) {
            $valorde = $_GET['valorminimo'];
        }

        if (isset($_GET['valormaximo']) && !empty($_GET['valormaximo'])) {
            $valorate = $_GET['valormaximo'];
        }

        
       
        
        $imoveis->setAcademia($imo->academia);
        $imoveis->setAreaPrivativa($imo->areaprivativa);
        $imoveis->setAreaServico($imo->areaservico);
        $imoveis->setArealazer($imo->arealazer);
        $imoveis->setBoxDespejo($imo->boxDespejo);
        $imoveis->setDce($imo->dce);
        $imoveis->setElevador($imo->elevador);
        $imoveis->setMobiliado($imo->mobiliado);
        $imoveis->setPiscina($imo->piscina);
        $imoveis->setPortaria24h($imo->portaria24h);
        
        $imoveis->setSalaofestas($imo->salaofestas);
        
        $imoveis->setVaranda($imo->varanda);
        $imoveis->setVarandaGourmet($imo->varandagourmet);
        
        $imoveis->setCircuitoTv($imo->circuitotv);
        $imoveis->setEndereco($imo->endereco);
        
        $imoveis->setAreaate($imo->areaate);
        $imoveis->setAreade($imo->areade);
        
        
    
 
 
        //BUSCAR TODOS OS REGISTROS
        
        $imovel_model = new Imoveis_model(new Api(), $imoveis->getFinalidade());
        $resultado = $imovel_model->retornarImoveisDisponiveisNovo($imoveis, $imo->pagina);


        $resultado = json_decode($resultado);
        $resultado = (array) $resultado;

    
        
        $objTitulo = new UrlAmigavel();

        foreach ($resultado['lista'] as $key => $value) {

            $titulo = $objTitulo->gerarTitulo($value->titulo);
  
            $resultado['lista'][$key]->titulo = $titulo;
        }

        

//        echo "<pre>";
//        var_dump($resultado);exit;
//        
//        $total = $resultado['quantidade'];
//        $quantidadeImoveis = ceil($total / 10);
//
//
//        $resultado['total'] = $total;
//        $resultado['paginacao'] = $quantidadeImoveis;
//        $resultado['finalidade'] = $finalidade;
//        $resultado['tipo'] = $tipo;
//        $resultado['url'] = 'aluguel/' . $tipoPorExtenso;



        header('Content-Type: application/json');
        echo json_encode($resultado);
    }
    
    public function detalhes($url, $codigo) {


        $imovel = new Imoveis_model(new Api(), 1);
        $resultado = $imovel->getImovel($codigo);

        $resultado = json_decode($resultado);
        $resultado = (array) $resultado;

   
        $finalidade = 0;
        if ($resultado['imovel']->finalidade == 'Aluguel') {
            $finalidade = 1;
        } else {
            $finalidade = 2;
        }



        $imovel = new Imovel();



//        $valorde = preg_replace("/[^0-9,.]/", "", $resultado['imovel']->valor);
//        $valorate = preg_replace("/[^0-9,.]/", "", $resultado['imovel']->valor);
//        
//          var_dump($valorde);
//        var_dump($valorate);exit;
//        $valorde = (float) $valorde + 200;
//        $valorate = (float) $valorate + 200;
//
//        var_dump($valorde);
//        var_dump($valorate);
//        var_dump($resultado['imovel']->codigocidade);
//        var_dump($resultado['imovel']->codigotipo);


        $imovel->setFinalidade($finalidade);
        $imovel->setCodigocidade($resultado['imovel']->codigocidade);
        $imovel->setCodigoTipo($resultado['imovel']->codigotipo);
        $imovel->setValorate(0);
        $imovel->setValorde(0);

        $imovelRelacao = new Imoveis_model(new Api(), $finalidade);
        $imoveisRelacionados = $imovelRelacao->retornarImoveisSimilares($imovel, 1);
        

        $imoveisRelacionados = json_decode($imoveisRelacionados);
        $imoveisRelacionados = (array) $imoveisRelacionados;
        
        $objTitulo = new UrlAmigavel();

        foreach ($imoveisRelacionados['lista'] as $key => $value) {

            $titulo = $objTitulo->gerarTitulo($value->titulo);
  
            $imoveisRelacionados['lista'][$key]->titulo = $titulo;
        }

        $resultado['imoveisRelacionados'] = $imoveisRelacionados['lista'];
        $resultado['titlePagina'] = $resultado['imovel']->titulo;
        


        $this->loadView('detalhes', $resultado);
    }

    public function getImoveisDisponiveis() {


        $dados = $_POST['dados'];


        $imoveis = new Imoveis_model(new Api(), $dados['finalidade']);
        $resultado = $imoveis->getImoveisMapa($dados);

        header('Content-Type: application/json');
        echo json_encode($resultado);
    }

    public function getImovelAjax() {

        $codigo = $_POST['codigo'];

        $imovel = new Imoveis_model(new Api(), 1);
        $resultado = $imovel->getImovel($codigo);

        header('Content-Type: application/json');
        echo json_encode($resultado);
    }
    
    public function getImoveisMapa(){
       
        $dados = $_POST['paramentros_mapa'];

        $imoveis = new Imoveis_model(new Api(), $dados['finalidade']);
        $resultado = $imoveis->getImoveisMapaHome($dados);

        header('Content-Type: application/json');
        echo json_encode($resultado);
    }
    
    
   
    public function getimoveiscodigosHome() {

        $resultado = array();
        $codigo = $_POST['codigo'];
        
     
        
        if($codigo['finalidade'] == 'aluguel' || $codigo['finalidade'] == 1){
            $codigo['finalidade']  = 1;
        }else{
             $codigo['finalidade']  = 2;
        }

        $codigo['finalidade']  = 0;
        

        $imovel_model = new Imoveis_model(new Api(), $codigo['finalidade']);
        $imovel = $imovel_model->getImoveisPorCodigoHome($codigo);

        $imovel = json_decode($imovel);
        $imovel = (array) $imovel;
          
        $objTitulo = new UrlAmigavel();

        foreach ($imovel['lista'] as $key => $value) {

            $titulo = $objTitulo->gerarTitulo($value->titulo);
  
            $imovel['lista'][$key]->titulo = $titulo;
        }


        header('Content-Type: application/json');
        echo json_encode($imovel);
    }


}
