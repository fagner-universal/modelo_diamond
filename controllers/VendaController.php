<?php

class VendaController extends Controller {

    public $finalidade = 2;
    
    //CLIENTE JOMAR
    public $chave = CHAVE;

    //CHAVE DEMO
    // public $chave = 'RZZuhI4HKPnkrUuToq3j8A==';

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        //NAO UTILIZADO NO MOMENTO
    }

    public function venda($tipo = '', $cidade = '', $bairro = '', $quarto = 0, $numerovagas = 0, $numerobanhos = 0, $valorde = 0, $valorate = 0) {

   
        $finalidade = 2;
        $pagina = 1;


//        ------QUANDO URL É ACESSADO COMO TODOS----------
        //QUANDO TIPO DE IMOVEL ESTÁ COMO TODOS
        if ($tipo == 'imoveis') {
            $tipo = '';
        }

        //DEFINE O NUMERO DE QUARTOS PELA URL
        if ($quarto == '1-quartos') {
            $quarto = 1;
        }
        if ($quarto == '2-quartos') {
            $quarto = 2;
        }
        if ($quarto == '3-quartos') {
            $quarto = 3;
        }
        if ($quarto == '4-quartos') {
            $quarto = 4;
        }



        //DEFINIR NUMERO DE banheiros
        if ($numerobanhos == '1-banheiro') {
            $numerobanhos = 1;
        }
        if ($numerobanhos == '2-banehiros') {
            $numerobanhos = 2;
        }
        if ($numerobanhos == '3-banheiros') {
            $numerobanhos = 3;
        }
        if ($numerobanhos == '4-banheiros') {
            $numerobanhos = 4;
        }



        //DEFINIR NUMERO DE VAGAS
        if ($numerovagas == '1-vaga') {
           $numerovagas = 1;
        }
        if ($numerovagas == '2-vaga') {
            $numerovagas = 2;
        }
        if ($numerovagas == '3-vaga') {
            $numerovagas = 3;
        }
        if ($numerovagas == '4-vaga') {
            $numerovagas = 4;
        }




        //DEFINE A CIDADE PELA URL QUANDO ESTA MARCADO COMO TODOS
        if ($cidade == 'todas-as-cidades') {
            $cidade = '';
        }

        //BAIRRO QUANDO MARCADO TODOS
        if ($bairro == 'todos-os-bairros') {
            $bairro = '';
        }

        //QUARTOS QUANDO MARCADO TODOS
        if ($quarto == '1-quarto-ou-mais') {
            $quarto = 0;
        }


        //QUARTOS QUANDO MARCADO TODOS
        if ($numerovagas == '1-vaga-ou-mais') {

            $numerovagas = 0;
        }


        //QUARTOS QUANDO MARCADO TODOS
        if ($numerobanhos == '1-banheiro-ou-mais') {
            $numerobanhos = 0;
        }







        //----------------END URL PARA TODOS
//        
//                       echo $finalidade."<br>";
//             echo "tipo: ".  $tipo."<br>";
//              echo  "cidade: ".$cidade."<br>";
//              echo  "bairro: ".$bairro."<br>";
//              echo  "quarto: ".$quarto."<br>";
//              echo "numerovagas: ". $numerovagas."<br>";
//               echo "numerobanhos: ". $numerobanhos."<br>";
//               echo "areade: ".$valorde."<br>";
//               echo "areaate: ".$valorate;exit;
//       
        //PEGAR PAGINAÇÃO
        if (isset($_GET['pagina']) && !empty($_GET['pagina'])) {
            $pagina = $_GET['pagina'];
        }


        //VARIAVEL PARA PEGAR TIPO POR EXTENSO
        $tipoPorExtenso = '';


        //PEGAR URL PARA TIPO DE IMOVEL
        $tipoImovel = '';
        if ($tipo != '') {

            $url = new UrlAmigavel();
            $tipoImovel = $url->urlTipo($tipo, $finalidade);

            foreach ($tipoImovel as $key => $value) {

                if ($tipo == $value['nome']) {
                    $tipoPorExtenso = $value['nome'];
                    $tipo = $value['codigo'];
                }
            }
        }


        if ($tipo == '') {
            $tipo = '';
        }



        //VARIAVEL POR EXTENÇO
        $cidadePorExtenso = '';

        //PEGAR URL PARA CIDADE

        $todasCidades = '';
        if ($cidade != '') {
            $url = new UrlAmigavel();
            $todasCidades = $url->urlCidade($cidade, $finalidade);


            foreach ($todasCidades as $key => $value) {

                if ($cidade == $value['nome']) {
                    
   
                    $cidadePorExtenso = $value['nome'];
                    $cidade = $value['cidade'];
                    
                   
                }
            }
        }

        if ($cidade == '') {
            $cidade = 0;
        }
        
        
        


        //PEGAR URL PARA BAIRRO
        $todosBairros = '';
        if ($bairro != '') {
            $url = new UrlAmigavel();
            $todosBairros = $url->urlBairro($bairro, $finalidade);


            foreach ($todosBairros as $key => $value) {

                if ($bairro == $value['nome']) {
                    $bairro = $value['cidade'];
                }
            }
        }

        if ($bairro == '') {
            $bairro = '';
        }


        $api = new Api();
        $resultado = $api->GET(''.URLAPI.'/Imovel/RetornarImoveisDisponiveis?parametros={"finalidade":"'
                . $finalidade . '","codigocidade":"' . $cidade . '","codigoTipo":"' . $tipo . '","numeroPagina":"'
                . $pagina . '","numeroquartos":"' . (int) $quarto . '","numerovagas":"' . (int) $numerovagas . '","numerobanhos":"'
                . (int) $numerobanhos . '","valorde":"' . (float) $valorde . '","valorate":"' . (float) $valorate . '","codigosbairros":"' . $bairro . '"'
                . ',"opcaoimovel":"4","numeroRegistros":"20"}', '' . $this->chave . '');

        $resultado = json_decode($resultado);
        $resultado = (array) $resultado;



        $totalImoveis = $resultado['quantidade'];
        $quantidadeImoveis = ceil($totalImoveis / 20);



//        $tipoImovel;
//        $todasCidades;
//        $todosBairros;
//        
//        echo "<pre>";
////      var_dump($quantidadeImoveis);
//        var_dump($todasCidades);exit;
//        var_dump($todosBairros);

        $resultado['total'] = $totalImoveis;
        $resultado['paginacao'] = $quantidadeImoveis;
        $resultado['finalidade'] = $finalidade;
        $resultado['tipo'] = $tipo;
        $resultado['cidade'] = $cidadePorExtenso;
        $resultado['url'] = 'aluguel/' . $tipoPorExtenso;



        $this->loadTemplate('listagem-imoveis', $resultado);
    }

    public function aluguelAjax($finalidade = '', $tipo = '', $cidade = '', $bairro = '', $quarto = '', $numerovagas = '', $numerobanhos = '') {


//        echo $finalidade . "<br>";
//        echo $tipo . "<br>";
//        echo $cidade . "<br>";
//        echo $bairro . "<br>";
//        echo $quarto . "<br>";
//        echo $numerovagas . "<br>";
//        echo $numerobanhos;
//        exit;
        
        
         //PEGAR PAGINAÇÃO

        $pagina = 1;
        $valorde = '';
        $valorate = '';

        if (isset($_GET['pagina']) && !empty($_GET['pagina'])) {
            $pagina = $_GET['pagina'];
        }
        
        if (isset($_GET['valorminimo']) && !empty($_GET['valorminimo'])) {
            $valorde = $_GET['valorminimo'];
        }
        
         if (isset($_GET['valormaximo']) && !empty($_GET['valormaximo'])) {
            $valorate = $_GET['valormaximo'];
        }

        

        // DEFINE O A FINALIADE DIGITADA NA URL
        if ($finalidade == 'aluguel') {
            $finalidade = 1;
        } else {
            $finalidade = 2;
        }



        if ($tipo == 'imoveis') {
            $tipo = '';
        }


        //DEFINE O NUMERO DE QUARTOS PELA URL
        if ($quarto == '1-quartos') {
            $quarto = 1;
        }
        if ($quarto == '2-quartos') {
            $quarto = 2;
        }
        if ($quarto == '3-quartos') {
            $quarto = 3;
        }
        if ($quarto == '4-quartos') {
            $quarto = 4;
        }
        
        
        
              //DEFINIR NUMERO DE banheiros
        if ($numerobanhos == '1-banheiro') {
            $numerobanhos = 1;
        }
        if ($numerobanhos == '2-banehiros') {
            $numerobanhos = 2;
        }
        if ($numerobanhos == '3-banheiros') {
            $numerobanhos = 3;
        }
        if ($numerobanhos == '4-banheiros') {
            $numerobanhos = 4;
        }



        //DEFINIR NUMERO DE VAGAS
        if ($numerovagas == '1-vaga') {
           $numerovagas = 1;
        }
        if ($numerovagas == '2-vaga') {
            $numerovagas = 2;
        }
        if ($numerovagas == '3-vaga') {
            $numerovagas = 3;
        }
        if ($numerovagas == '4-vaga') {
            $numerovagas = 4;
        }

        
        


        //DEFINE A CIDADE PELA URL QUANDO ESTA MARCADO COMO TODOS
        if ($cidade == 'todas-as-cidades') {
            $cidade = 0;
        }


        //BAIRRO QUANDO MARCADO TODOS
        if ($bairro == 'todos-os-bairros') {
            $bairro = '';
        }


        //QUARTOS QUANDO MARCADO TODOS
        if ($quarto == '1-quarto-ou-mais') {
            $quarto = 0;
        }

        //QUARTOS QUANDO MARCADO TODOS
        if ($numerovagas == '1-vaga-ou-mais') {

            $numerovagas = 0;
        }

        //QUARTOS QUANDO MARCADO TODOS
        if ($numerobanhos == '1-banheiro-ou-mais') {
            $numerobanhos = 0;
        }




               
       

        //VARIAVEL PARA PEGAR TIPO POR EXTENSO
        $tipoPorExtenso = '';

        //PEGAR URL PARA TIPO DE IMOVEL
        if ($tipo != '') {

            $url = new UrlAmigavel();
            $todasUrls = $url->urlTipo($tipo, $finalidade);

            foreach ($todasUrls as $key => $value) {

                if ($tipo == $value['nome']) {
                    $tipoPorExtenso = $value['nome'];
                    $tipo = $value['codigo'];
                }
            }
        }
        
     

        if ($tipo == '') {
            $tipo = '';
        }


        if ($cidade != '') {
            $url = new UrlAmigavel();
            $todasUrls = $url->urlCidade($cidade, $finalidade);


            foreach ($todasUrls as $key => $value) {
                if ($cidade == $value['nome']) {
                    $cidade = $value['cidade'];
                }
            }
        }

        if ($cidade == '') {
            $cidade = 0;
        }


        
        
        //PEGAR URL PARA BAIRRO
        if ($bairro != '') {
            $url = new UrlAmigavel();
            $todasUrls = $url->urlBairro($bairro, $finalidade);


            foreach ($todasUrls as $key => $value) {

                if ($bairro == $value['nome']) {
                    $bairro = $value['cidade'];
                }
            }
        }

        if ($bairro == '') {
            $bairro = '';
        } elseif (is_numeric($bairro)) {

            $bairro =(string)$bairro;
        }
        
  
        
//        
//               echo $finalidade."<br>";;
//             echo "tipo: ".  $tipo."<br>";
//              echo  "cidade: ".$cidade."<br>";
//              echo  "bairro: ".$bairro."<br>";
//              echo  "quarto: ".$quarto."<br>";
//              echo "numerovagas: ". $numerovagas."<br>";
//               echo "numerobanhos: ". $numerobanhos."<br>";
//               echo "areade: ".$valorde."<br>";
//               echo "areaate: ".$valorate;





        $api = new Api();
        $resultado = $api->GET(''.URLAPI.'Imovel/RetornarImoveisDisponiveis?parametros={"finalidade":"'
                . $finalidade . '","codigocidade":"' . $cidade . '","codigoTipo":"'.$tipo.'","numeroPagina":"'
                . $pagina . '","numeroquartos":"' . (int) $quarto . '","numerovagas":"' . (int) $numerovagas . '","numerobanhos":"'
                . (int) $numerobanhos . '","valorde":"' . (float) $valorde . '","valorate":"' . (float) $valorate . '","codigosbairros":"' . $bairro . '"'
                . ',"opcaoimovel":"4","numeroRegistros":"20"}', '' . $this->chave . '');



        $resultado = json_decode($resultado);
        $resultado = (array) $resultado;


        $total = $resultado['quantidade'];
        $quantidadeImoveis = ceil($total / 20);


        $resultado['total'] = $total;
        $resultado['paginacao'] = $quantidadeImoveis;



//        $resultado['finalidade'] = $finalidade;
//        $resultado['tipo'] = $tipo;
//        $resultado['url'] = 'aluguel/' . $tipoPorExtenso;




        header('Content-Type: application/json');
        echo json_encode($resultado);
    }

}
