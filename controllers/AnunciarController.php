<?php
class AnunciarController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
    
        $dados = array();

           
         $dados['titlePagina'] = 'Anunciar Imóveis - ' . TITULO_AUXILIAR ;

        $this->loadTemplate('anunciar', $dados);
    }
    
    
    public function enviarForm(){
        
        
        $imovel = $_POST['imovel'];
    
        $email = new Email();
        $retorno = $email->enviarImoveil($imovel);
        
        
   
        header('Content-Type: application/json');
        echo json_encode($retorno);
        
        
        
    }

}