<?php

class CodigoImovelAjaxController extends Controller {

    //CLIENTE JOMAR
    public $chave = CHAVE;
    public $finalidade = 0;

    public function __construct() {
        parent::__construct();
    }

    public function getImovel() {
        
        $finalidade = 0;
        
        
 
         if (isset($_GET['codigo']) && !empty($_GET['codigo'])) {
            $codigo = $_GET['codigo'];
        }
        
        if (isset($_GET['finalidade']) && !empty($_GET['finalidade'])) {
            $finalidade = $_GET['finalidade'];
        }
        
        
        
        $codigo = $_GET['codigo'];
        $finalidade = $_GET['finalidade'];
     
        
        
        if($finalidade == 'aluguel'){
            $finalidade = 1;
        }else{
                $finalidade = 2;
        }



        $imoveis = new Imoveis_model(new Api(),0);
        $imovel = $imoveis->getImoveisPorCodigo($codigo);

        $imovel = json_decode($imovel);
        $imovel = (array)$imovel;

        $objTitulo = new UrlAmigavel();

        foreach ($imovel['lista'] as $key => $value) {

            $titulo = $objTitulo->gerarTitulo($value->titulo);
  
            $imovel['lista'][$key]->titulo = $titulo;
        }

        header('Content-Type: application/json');
        echo json_encode($imovel);
        
    }

}
