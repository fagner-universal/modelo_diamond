<?php

class TipoAjaxController extends Controller {
    
    //paramentro para remover acentos
    public   $low = array("Á" => "á", "É" => "é", "Í" => "í", "Ó" => "ó", "Ú" => "ú", "Ü" => "ü", "não" => "não","Ç" => "ç");

    //CLIENTE JOMAR
    public $chave = CHAVE;
    

    //CHAVE DEMO
   // public $chave = 'RZZuhI4HKPnkrUuToq3j8A==';
    
    public $finalidade = 1;

    public function __construct() {
        parent::__construct();
    }

    public function getTipo() {
        
//        $cidade = $_GET['cidade'];
        

        $api = new Api();
        $tipo = $api->GET(''.URLAPI.'Imovel/RetornarTiposImoveisDisponiveis?parametros={"finalidade":"0"}',''.$this->chave.'');

   
        $bairro = json_decode($tipo);
        

        

        //COLOCA TODAS AS LETRAS EM MINUSCULOS
        $array = array();
        foreach ($bairro->lista as $key => $value) {
       
            $array[] = array('nome' => $this->tirarAcentos($value->nome), 'codigo' => $value->codigo,'nome2'=> strtolower($value->nome));
        }
        
 
        //TIRA TODOS OS ACENTOS
        $array2 = array();
        foreach ($array as $key => $value2) {
            $array2[] = array('nome' =>  mb_strtolower(strtr($value2['nome'],$this->low),'UTF-8'), 'codigo' => $value2['codigo'],'nome2'=> $this->tirarAcentos($value2['nome']));
        }

        
       
        //SUBSTITUE OS ESPAÇOES POR "-"
        $array3 = array();
        foreach ($array2 as $key => $value3) {
            $array3[] = array('nome' => str_replace(' ', '-', $value3['nome']), 'codigo' => $value3['codigo'],'nome2'=> str_replace(' ', '-',$value3['nome']));
        }
        

 
        //INSERIR NOME ORIGINAL NO ARRAY
        foreach ($array3 as $key => $value) {
            $array3[$key]['nomeOriginal'] = $bairro->lista[$key]->nome;
        }



        header('Content-Type: application/json');
        echo json_encode($array3);
    }

    public function tirarAcentos($string){
    return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/","/(Ç)/","/(ç)/"),explode(" ","a A e E i I o O u U n N C ç"),$string);
    }

}
