<?php

class CidadeAjaxController extends Controller {

    public $finalidade = 0;
    
    //paramentro para remover acentos
    public   $low = array("Á" => "á", "É" => "é", "Í" => "í", "Ó" => "ó", "Ú" => "ú", "Ü" => "ü", "não" => "não","Ç" => "ç");
    
    //CLIENTE JOMAR
    public $chave = CHAVE;
    
    //CHAVE DEMO
   // public $chave = 'RZZuhI4HKPnkrUuToq3j8A==';

    public function __construct() {
        parent::__construct();
    }

    public function getCidades() {


         $api = new Api();
        $cidades = $api->GET(''.URLAPI.'Imovel/RetornarCidadesDisponiveis?parametros={"finalidade":"'
                                . $this->finalidade.'"}', ''.$this->chave.'');

        $cidades = json_decode($cidades);
        
            
  
       
        //COLOCA TODAS AS LETRAS EM MINUSCULOS
        $array = array();
        foreach ($cidades->lista as $key => $value) {
            $array[] = array('nome' => $this->tirarAcentos($value->nome),'nomeOriginal' => $value->nome ,'codigo' => $value->codigo);
        }
       
        
        //TIRA TODOS OS ACENTOS
        $array2 = array();
        foreach ($array as $key => $value2) {
             $array2[] = array('nome' =>  mb_strtolower(strtr($value2['nome'],$this->low),'UTF-8'),'nomeOriginal' => $value2['nome'], 'codigo' => $value2['codigo']);
        }
        

        //SUBSTITUE OS ESPAÇOES POR "-"
        $array3 = array();
        foreach ($array2 as $key => $value3) {
             $array3[] = array('nome' =>  str_replace('ç','c',str_replace(' ','-',$value3['nome'])),'nomeOriginal' => $value3['nome'], 'codigo' => $value3['codigo']);
        }

        
        //INSERIR NOME ORIGINAL NO ARRAY
        foreach ($array3 as $key => $value) {
            $array3[$key]['nomeOriginal'] =  $cidades->lista[$key]->nome;
        }

        header('Content-Type: application/json');
        echo json_encode($array3);
    }
    
    
    
    function getCidadesOriginal(){
        
        $api = new Api();
        $cidades = $api->GET('https://api.imoview.com.br/Imovel/RetornarCidadesDisponiveis?parametros={"finalidade":"'
                                . $this->finalidade.'"}', ''.$this->chave.'');
        
        header('Content-Type: application/json');
        echo json_encode($cidades);

    }


    public function tirarAcentos($string){
    return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/","/(Ç)/","/(ç)/"),explode(" ","a A e E i I o O u U n N C ç"),$string);
    }
    

}
