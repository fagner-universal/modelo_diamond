<?php

class CondController extends Controller {

    public $chave = CHAVE;

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        echo 'index';
    }

    public function getCondominios() {

        $array = array();
        $finalidade = $_POST['finalidade'];
        
        if ($finalidade == 'aluguel') {
            $finalidade = 1;
        } else if ($finalidade == 'venda') {
            $finalidade = 2;
        } else {
            $finalidade = 0;
        }


        $condominio = new Condominio_model(new Api(), $finalidade);
        $condominios = $condominio->getCondominios();



        $url = new UrlAmigavel();
        $condominios = $url->urlCondominio($condominios);

        header('Content-Type: application/json');
        echo json_encode($condominios);
    }

    public function getCondominiosDadosCompletos() {


        $array = array();
        $finalidade = 0;


        $condominio = new Condominio_model(new Api(), $finalidade);
        $condominios = $condominio->getCondominios();
   
        $url = new UrlAmigavel();
        $conUrl = $url->urlCondominio($condominios);
        $conUrl = (array) $conUrl;
    
        
        $array['condUrl'] = $conUrl;
        $array['condominios'] = $condominios;
        
        
        

        header('Content-Type: application/json');
        echo json_encode($array);
    }

}
