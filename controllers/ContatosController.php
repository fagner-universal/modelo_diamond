<?php

class ContatosController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $dados = array();

        $this->loadTemplate('contatos', $dados);
    }

    public function faleConosco() {


        $dados = array();


        $dados['titlePagina'] = 'Contato - '. TITULO_AUXILIAR;

        $this->loadTemplate('contatos', $dados);
    }

    public function enviar() {
        
        $contato = $_POST['contato'];

        $email = new Email();
        $retorno = $email->enviarContato($contato);
        
        
        header('Content-Type: application/json');
        echo json_encode($retorno);


    }

}
