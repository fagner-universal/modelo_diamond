/*
 * @returns {undefined}
 * Carregar imoveis
 */

var paramentros = imovel;



//CARREGAR BAIRROS
function carregarBairrosBusca() {


    $.ajax({
        method: "POST",
        url: base_url + 'bairroAjax', //ImovelController
        async: true,
        data: { 'cidade': paramentros.codigocidade },
        data: {},
        beforeSend: function() {

        }
    }).done(function(bairros) {


        //REMOVE O SELECT
        $('#bairro-input-busca').empty();

        var cidadeSelecionadaBusca = $('#cidade-input-busca').val();

        $('#bairro-input-busca').append('<option codigo="0" value="todos-os-bairros">Todos</option>');

        $.each(bairros, function(key2, bairro) {

            if (cidadeSelecionadaBusca == bairro.cidade) {

                $('#bairro-input-busca').append('<option codigo="' + bairro.codigo + '" value="' + bairro.nome + '">' + bairro.nomeOriginal + '</option>');
            }
        });
        //ADICIONA A CLASS SELECT  "selectpicker" APOS SELECT PREENCHIDO

    }).always(function() {

    });
}

/*
 *==========================
 *CARREGAR TODAS AS CIDADES
 *=========================
 */

$.ajax({
    method: "POST",
    url: base_url + 'cidadeAjax', //ImovelController
    async: true,
    data: { imovel: imovel },
    beforeSend: function() {

        $('#cidade-input-busca').empty();
        $('#cidade-input-busca').append('<option codigo="0" value="todas-as-cidades">Todas as cidades</option>');
    }

}).done(function(cidades) {

    $.each(cidades, function(key, cidade) {

        $('#cidade-input-busca').append('<option codigo="' + cidade.codigo + '" value="' + cidade.nome + '">' + cidade.nomeOriginal + '</option>');
    });


    //ADICIONA A CLASS SELECT  "selectpicker" APOS SELECT PREENCHIDO


}).fail(function(cidades) {

    alert('erro ao caregar cidades');

}).always(function(cidades) {

});


//QUANDO CIDADE É SELECIONADA PREENCHA O BAIRRO
$('#cidade-input-busca').on('change', function() {

    carregarBairrosBusca();

});

/*
 *=========================
 *CARREGAR TODOS OS TIPO
 *==========================
 */

function carregarTipos() {

    $.ajax({
        method: "POST",
        url: base_url + 'tipoAjax', //ImovelController
        async: true,
        data: { 'finalidade': paramentros.finalidade },
        beforeSend: function() {

        }
    }).done(function(tipoImovel) {



        $('#tipo-input-busca').empty();

        $('#tipo-input-busca').append('<option id="imoveis" value="todos-os-tipos">Todos</option>');

        $.each(tipoImovel, function(key, tipo) {

            $('#tipo-input-busca').append('<option id="' + tipo.codigo + '" value="' + tipo.nome + '">' + tipo.nomeOriginal + '</option>');

        });

    }).then(function() {

        carregarPaginaUrl();

    }).fail(function() {

        console.log('erro no carregar tipo');

    }).always(function() {

    });

}


/*
 * ========================================
 *
 * PEGAR DADOS SELECIONADOS NA BUSCA
 *
 * ========================================
 */

//cria o objeto que sera passado para o pagina de busca
var imovel_busca_home = imovel;


/*
 * ========================================
 *
 * CARREGAR IMOVEIS BUSCA PRINCIPA
 *
 * ========================================
 *
 */

//
function carregarImoveis() {

    paramentros.destaque = 0;

    $.ajax({
        method: "POST",
        url: base_url + 'imoveis/ajax/', //ImovelController
        async: true,
        data: { 'imovel': paramentros },
        beforeSend: function() {

            $('.load-gif-imoveis').css('display', 'block');
            $('#load-gif').css('display', 'block');


            //LIMPA TODOS OS IMOVEIS
            $('.corpo-imoveis-busca').empty();

        }

    }).fail(function() {

        alert("error ao carregaso os imoveis Url");

    }).done(function(dados) {

        $('.corpo-imoveis-busca').empty();
        $('#total-imoveis').empty();
        $('#total-imoveis').text(dados.quantidade);

        //    var imoveis_busca = jQuery.parseJSON(dados);;

        var imoveis_busca = dados;

        //ATUALIZA O TORAL DE IMOVEIS DA DIV
        $('.heading-icon').text(imoveis_busca.quantidade);

        /*
         *
         * LIMPA PAGINACAO
         * RECARREGA PAGINACAO
         *
         */
        $('.container-botoes').empty();



        //LIMPA TODOS OS IMOVEIS

        //ISERE OS IMOVEIS DA DIV
        $.each(imoveis_busca.lista, function(key, imoveis) {



            let imoveis_encontrados =
                '<div class="row card-imovel" style=" margin-bottom: 20px;">' +
                '<div class="col-12 col-sm-12 col-md-12 col-xl-5 img-square-wrapper" style="padding: 0px">' +
                '<a target="_blank" href="' + base_url + 'imoveis/' + imoveis.titulo + '/' + imoveis.codigo + '/">' +
                '<img src="' + imoveis.urlfotoprincipalp + '" style="width: 100%;height:300px; border-radius: 8px 0 0 8px;" >' +
                '</a>' +
                '<div class="ficha-destaque">' +
                imoveis.finalidade +
                '</div>' +
                '<div class="ficha-preco">COD. ' +
                imoveis.codigo +
                '</div>' +
                //                    '<div class="fixa-tipo">' +
                //                    imoveis.tipo +
                //                    '</div>' +
                '</div>' +
                '<div class="col-12 col-sm-12 col-md-12 col-md-7 col-xl-7" style="padding: 5px;padding-top: 27px">' +
                '<div class="row">' +
                '<div class="col-12">' +
                '<a href="' + base_url + 'imoveis/' + imoveis.titulo + '/' + imoveis.codigo + '/" target="_blank">' +
                '<h5 class="text-center" style="color:var(--cor-primaria);font-weight:bold">' + imoveis.tipo + (imoveis.finalidade == 'Aluguel' ? ' para alugar no ' : ' à venda no ') + imoveis.bairro + '</h5>' +
                // (imoveis.finalidade == 'Aluguel' ? '<p class="text-center" style="font-size:12px;margin: 5px"><i class="ion-map"></i>  ' + imoveis.endereco + ' - N° ' + imoveis.numero + '</p>' : '') +
                '</a>' +
                '</div>' +
                '</div>' +
                '<div class="row d-md-flex justify-content-center " style="padding: 0px">' +
                '<div class="item-imovel-result-footer icons-items"> ' +

                '<span class="caracteristicas-card-imoveis"><i class="flaticon-bed"></i><b>' + imoveis.numeroquartos + ' quarto(s)</b></span>' +

                '<span class="caracteristicas-card-imoveis"><i class="flaticon-holidays"></i><b>' + imoveis.numerobanhos + ' banh.</b></span>' +
                '<span class="caracteristicas-card-imoveis"><i class="flaticon-vehicle"></i><b>' + imoveis.numerovagas + ' vaga(s)</b></span>' +
                '<span class="caracteristicas-card-imoveis d-none d-sm-inline-block"><i class="flaticon-square-layouting-with-black-square-in-east-area"></i><b>' + imoveis.areaprincipal + ' área</b></span>' +
                '</div>' +
                '</div>' +
                //                    '<div class="row flex-row-reverse" style="padding-top: 0px;padding-bottom: 0px;padding-right: 10px">' +
                //                    '<button type="button" class="btn btn-danger btn-sm" style="margin: 5px">Contatar</button>' +
                //                    '<a href="' + base_url + 'detalhe-imovel/' + imoveis.codigo + '/"  class="btn btn-danger  btn-sm" style="margin: 5px">Ver Detalhe</a>' +
                //                    '</div>' +
                '<div class="row mt-md-5" style="padding: 10px">' +
                '<div class="col-12">' +
                '<p class="text-center caracteristicas-card-imoveis my-auto" style="color:var(--cor-primaria);font-weight:bold;font-size:20px"><span class="small">Valor: </span>' + imoveis.valor + '</p>' +
                '</div>' +
                '<div class="col-12">' +
                '<a target="_blank" href="' + base_url + 'imoveis/' + imoveis.titulo + '/' + imoveis.codigo + '/"  class="btn btn-danger  btn-block" style="margin: 5px">Ver Detalhe</a>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';


            $('.corpo-imoveis-busca').append(imoveis_encontrados);

        });


        if (imoveis_busca.quantidade == 0) {

            $('.corpo-imoveis-busca').append('<h2 class="text-center">No momento não há imóveis disponíveis no local Selecionado</h2>');

        }



    }).then(function(dados) {



        gerarUrl(url_dinamiaca);
        carregarPaginacao();



        $('#load-gif').css('display', 'none');
        $('.load-gif-imoveis').css('display', 'none');


    }).always(function() {

        $('.load-gif-imoveis').css('display', 'none');
        $('#load-gif').css('display', 'none');

    });

}




//BUSCAR PELO STRING ENDERECO
$('#endereco-input').change(function() {
    paramentros.endereco = $(this).val();
    carregarImoveis();

});


/* OBRIGATÓRIO - Enviar 1 para ALUGUEL ou 2 para VENDA; */
$('#finalidade-input-busca').on('change', function() {

    paramentros.finalidade = $(this).val();

    console.log(paramentros.finalidad);
    

    if (paramentros.finalidade == 1) {
        url_dinamiaca.finalidade = 'aluguel';

    } else {
        url_dinamiaca.finalidade = 'venda';

    }


    //RETORNA PARA PAGINA 1
    paramentros.pagina = 1;
    url_dinamiaca.pagina = 1;

    gerarUrl(url_dinamiaca);
    carregarImoveis();

});

/* OBRIGATÓRIO - Enviar 1 para ALUGUEL ou 2 para VENDA; */

$('.aba-finalidade').click(function(e) {
    e.preventDefault();


    paramentros.finalidade = $(this).val();

    if ($(this).text() == 'Comprar') {
        $('#aba-finalidade-aluguel').removeClass('active');
        $(this).addClass('active');
        paramentros.finalidade = 2;
        paramentros.codigocondominio = 0;
        carregarCondominios();
    }


    if ($(this).text() == 'Alugar') {
        $('#aba-finalidade-venda').removeClass('active');
        $(this).addClass('active');
        paramentros.finalidade = 1;
        paramentros.codigocondominio = 0;
        carregarCondominios();
    }

    if (paramentros.finalidade == 1) {
        url_dinamiaca.finalidade = 'aluguel';

    } else {
        url_dinamiaca.finalidade = 'venda';

    }


    //RETORNA PARA PAGINA 1
    paramentros.pagina = 1;
    url_dinamiaca.pagina = 1;

    gerarUrl(url_dinamiaca);
    carregarImoveis();



});



//OBJETO TIPO E CÓDIGO TIPO
var objtipo = {};
objtipo.tipo = [];
objtipo.codigo = [];

var objAux = {};

function setTipoBusca() {

    //console.log(paramentros);
    //var objtipo = paramentros.codigoTipo;

    objAux = $(this);
    objtipo.tipo.push(objAux.val());


    //PEGAR O CÓDIGO DE TODOS OS TIPO DE IMOVEIS
    let arrayCodigoTipo = [];
    let arraynomeTipo = [];
    for (var i = 0; i < objtipo.tipo.length; i++) {

        $.each(objAux.children(), function(index, value) {
            if ($(value).val() == objtipo.tipo[i]) {
                arrayCodigoTipo.push($(value).attr('id'));
                arraynomeTipo.push($(value).val());
            }
        });

    }


    paramentros.codigoTipo = {
        'codigo': arrayCodigoTipo,
        'nome': objtipo.tipo
    };


    url_dinamiaca.tipo = paramentros.codigoTipo;

    //RETORNA PARA PAGINA 1
    paramentros.pagina = 1;
    url_dinamiaca.pagina = 1;

    gerarUrl(paramentros.codigoTipo);
    carregarImoveis();
}



$('#codigo-input').on('change', function() {

    var imo = {};
    imo.finalidade = $('#finalidade-input-busca').val();
    imo.codigo = $(this).val();

    busarCodigo(imo);

});


$('#tipo-input-busca').on('change', function() {

    objAux = $(this);

    if (objtipo.tipo[0] == "imoveis") {

        objtipo.tipo = [];
    }

    objtipo.tipo.push(objAux.val());

    //PEGAR O CÓDIGO DE TODOS OS TIPO DE IMOVEIS
    let arrayCodigoTipo = [];
    let arraynomeTipo = [];


    for (var i = 0; i < objtipo.tipo.length; i++) {

        $.each(objAux.children(), function(index, value) {

            if ($(value).val() == objtipo.tipo[i]) {
                arrayCodigoTipo.push($(value).attr('id'));
                arraynomeTipo.push($(value).val());
            }
        });

    }


    paramentros.codigoTipo = {
        'codigo': arrayCodigoTipo,
        'nome': objtipo.tipo
    };



    url_dinamiaca.tipo = paramentros.codigoTipo;

    //RETORNA PARA PAGINA 1
    paramentros.pagina = 1;
    url_dinamiaca.pagina = 1;


    gerarUrl(paramentros.codigoTipo);
    carregarImoveis();


    //    let tipo = $(this).val();
    //    paramentros.codigoTipo = tipo;
    //
    //    url_dinamiaca.tipo = tipo;
    //    carregarImoveis();
    //



});

// CARREGAR CONDOMÍNIOS
function carregarCondominios() {


    $.ajax({
        method: "POST",
        url: base_url + 'condominios', //ImovelController
        async: true,
        data: { 'finalidade': paramentros.finalidade },
        beforeSend: function() {

            $('.corpo-cond-busca').empty();

        }
    }).done(function(condominio) {


        let obj_cond_web = $('#corpo-cond-busca');
        obj_cond_web.attr('data-live-search', 'true')
            .attr('data-live-search-placeholder', 'Buscar Tipo')
            .on('change', setCondominio);

        obj_cond_web.append('<option id="0" value="todos-os-condominios">Todos</option>');

        $.each(condominio, function(key, cond) {

            obj_cond_web.append('<option id="' + cond.codigo + '" value="' + cond.nome + '">' + cond.nomeOriginal + '</option>');
        });


        obj_cond_web.addClass('selectpicker search-fields');
        $('.corpo-cond-busca').append('<label style="color:white;">Condomínios </label>');
        $('.corpo-cond-busca').append(obj_cond_web);
        


    }).then(function() {

        
        $('.corpo-cond-busca').fadeIn(3000);


    }).fail(function(e) {


        alert('erro no carregamento dos condomínios: '+e);

    }).always(function() {



    });

}

//Buscar condominio
// $("#corpo-cond-busca").change(function(){
//     paramentros.codigocondominio = $(this).val()
//     carregarImoveis()
// })
function setCondominio() {

    var objCond = {};
    objCond.cond = [];
    objCond.codigo = [];

    var objAux = {};


    objAux = $(this);
    objCond.cond.push(objAux.val());

    //PEGAR O CÓDIGO DE TODOS OS TIPO DE IMOVEIS
    let arrayCodCond = [];
    let arraynomeCond = [];
    for (var i = 0; i < objCond.cond.length; i++) {

        $.each(objAux.children(), function(index, value) {
            if ($(value).val() == objCond.cond[i]) {
                arrayCodCond.push($(value).attr('id'));
                arraynomeCond.push($(value).val());
            }
        });

    }

    paramentros.condominio = {
        'codigo': arrayCodCond,
        'nome': objCond.cond
    };

    url_dinamiaca.condominio = paramentros.condominio['nome'][0];
    paramentros.codigocondominio = paramentros.condominio['codigo'][0];


    //RETORNA PARA PAGINA 1
    paramentros.pagina = 1;
    url_dinamiaca.pagina = 1;

    gerarUrl(url_dinamiaca);
    carregarImoveis();

}

carregarCondominios();

$('#cidade-input-busca').on('change', function() {

    url_dinamiaca.bairro = {
        'nome': 0,
        'codigo': 0
    }

    paramentros.codigosbairros = {
        'nome': 0,
        'codigo': []
    };


    let cidade = $(this);

    url_dinamiaca.cidade = cidade.val();

    $.each(cidade.children(), function(index, value) {

        if ($(value).val() == url_dinamiaca.cidade) {

            paramentros.codigocidade = $(value).attr('codigo');

        }
    });


    if (paramentros.codigocidade == 0) {
        paramentros.codigocidade = '';
    }




    //RETORNA PARA PAGINA 1
    paramentros.pagina = 1;
    url_dinamiaca.pagina = 1;


    //    carregarBairrosBusca('desktop');;

    gerarUrl(url_dinamiaca);
    carregarImoveis();
});



/*
 * ===========================
 * SELECTIONAR BAIRROS
 * ==========================
 */



var arrayBairro = {
    'nome': [],
    'codigo': []
};


$('#bairro-input-busca').on('change', function() {




    if (typeof arrayBairro === 'undefined') {


        //        var arrayBairro = {
        //            'nome': [],
        //            'codigo': []
        //        };

    }



    let bairro = $(this);
    let bairroSelecionado = bairro.val();

    //PEGAR O CÓDIGO DE TODOS OS TIPO DE IMOVEIS

    $.each(bairro.children(), function(index, value) {

        if ($(value).val() == bairroSelecionado) {

            if (arrayBairro.codigo == 0) {
                arrayBairro.codigo = [];

            }

            arrayBairro.codigo.push($(value).attr('codigo'));
            arrayBairro.nome.push($(value).val());

        }
    });


    url_dinamiaca.bairro = arrayBairro;
    paramentros.codigosbairros = arrayBairro;

    //RETORNA PARA PAGINA 1
    paramentros.pagina = 1;
    url_dinamiaca.pagina = 1;

    gerarUrl(url_dinamiaca);
    carregarImoveis();

});


$('.botao-quarto').on('click', function() {

    $('.botao-quarto').removeClass('active_buttom');
    $(this).addClass('active_buttom');

    let quarto = $(this).text();

    if (quarto == '5+') {
        quarto = '5';
    }


    url_dinamiaca.quarto = quarto + '-quartos';
    paramentros.numeroquartos = quarto;


    //RETORNA PARA PAGINA 1
    paramentros.pagina = 1;
    url_dinamiaca.pagina = 1;

    gerarUrl(url_dinamiaca);
    carregarImoveis();
});


$('.suite-vagas').on('click', function() {


    $('.suite-vagas').removeClass('active_buttom');
    $(this).addClass('active_buttom');

    let suite = $(this).text();

    if (suite == '5+') {
        suite = '5';
    }


    url_dinamiaca.suite = suite + '-suite-ou-mais';
    paramentros.numerosuite = suite;

    //RETORNA PARA PAGINA 1
    paramentros.pagina = 1;
    url_dinamiaca.pagina = 1;

    gerarUrl(url_dinamiaca);
    carregarImoveis();
});


$('.botao-banheiro').on('click', function() {

    $('.botao-banheiro').removeClass('active_buttom');
    $(this).addClass('active_buttom');


    let banheiro = $(this).text();

    if (banheiro == '5+') {
        banheiro = '5';
    }

    url_dinamiaca.banheiro = banheiro + '-banheiro-ou-mais';
    paramentros.numerobanhos = banheiro;

    //RETORNA PARA PAGINA 1
    paramentros.pagina = 1;
    url_dinamiaca.pagina = 1;

    gerarUrl(url_dinamiaca);
    carregarImoveis();

});

$('.botao-vagas').on('click', function() {

    $('.botao-vagas').removeClass('active_buttom');

    $(this).addClass('active_buttom');



    let vagas = $(this).text();

    if (vagas == '5+') {
        vagas = '5';
    }

    url_dinamiaca.vagas = vagas + '-vaga-ou-mais';
    paramentros.numerovagas = vagas;

    gerarUrl(url_dinamiaca);
    carregarImoveis();
});

$('#valor-minimo').on('change', function() {

    let valorde = $(this).val();

    if (valorde == '') {
        valorde = 0;
    }

    //    valorde = valorde.replace(/[^\d]+/g, '');


    url_dinamiaca.valorde = valorde;
    paramentros.valorde = valorde;

    //RETORNA PARA PAGINA 1
    paramentros.pagina = 1;
    url_dinamiaca.pagina = 1;

    gerarUrl(url_dinamiaca);
    carregarImoveis();
});

$('#valor-minimo').mask("#.##0,00", { reverse: true });


$('#valor-maximo').on('change', function() {

    let valorate = $(this).val();


    if (valorate == '') {
        valorate = 0;
    }

    //RETORNA PARA PAGINA 1
    paramentros.pagina = 1;
    url_dinamiaca.pagina = 1;

    //    valorate = valorate.replace(/[^\d]+/g, '');


    url_dinamiaca.valorate = valorate;
    paramentros.valorate = valorate;

    gerarUrl(url_dinamiaca);
    carregarImoveis();
});



//AREA INTERNA
$('#area-minimo').on('change', function() {

    let areade = $(this).val();


    if (areade == '') {
        areade = 0;
    }

    //RETORNA PARA PAGINA 1
    paramentros.pagina = 1;
    url_dinamiaca.pagina = 1;

    //    valorate = valorate.replace(/[^\d]+/g, '');


    url_dinamiaca.areade = areade;
    paramentros.areade = areade;

    gerarUrl(url_dinamiaca);
    carregarImoveis();
});

//AREA INTERNA
$('#area-maximo').on('change', function() {

    let areaate = $(this).val();


    if (areaate == '') {
        areaate = 0;
    }

    //RETORNA PARA PAGINA 1
    paramentros.pagina = 1;
    url_dinamiaca.pagina = 1;

    //    valorate = valorate.replace(/[^\d]+/g, '');


    url_dinamiaca.areaate = areaate;
    paramentros.areaate = areaate;

    gerarUrl(url_dinamiaca);
    carregarImoveis();
});






//PORTARIA SELECIONADA
$('#academia').click(function() {

    //PORTARIA 24 HORAS
    if ($("#academia").is(':checked')) {
        $("#academia").attr("checked", true);

        paramentros.academia = true;
        url_dinamiaca.academia = 'academia';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;


    } else {

        $("#academia").attr("checked", false);
        paramentros.academia = false;


        paramentros.academia = false;
        url_dinamiaca.academia = 'academia';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;

    }

    gerarUrl(url_dinamiaca);
    carregarImoveis();

});





//PORTARIA ELEVADOR
$('#elevador').click(function() {


    //PORTARIA 24 HORAS
    if ($("#elevador").is(':checked')) {
        $("#elevador").attr("checked", true);

        paramentros.elevador = true;
        url_dinamiaca.elevador = 'elevador';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;


    } else {

        $("#elevador").attr("checked", false);
        paramentros.elevador = false;


        paramentros.elevador = false;
        url_dinamiaca.elevador = 'elevador';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;

    }

    gerarUrl(url_dinamiaca);
    carregarImoveis();

});



//PORTARIA ELEVADOR
$('#varanda').click(function() {


    //PORTARIA 24 HORAS
    if ($("#varanda").is(':checked')) {
        $("#varanda").attr("checked", true);

        paramentros.varanda = true;
        url_dinamiaca.varanda = 'varanda';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;


    } else {

        $("#varanda").attr("checked", false);
        paramentros.varanda = false;


        paramentros.varanda = false;
        url_dinamiaca.varanda = 'varanda';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;

    }

    gerarUrl(url_dinamiaca);
    carregarImoveis();

});


//PORTARIA ELEVADOR
$('#varandagourmet').click(function() {


    //PORTARIA 24 HORAS
    if ($("#varandagourmet").is(':checked')) {
        $("#varandagourmet").attr("checked", true);

        paramentros.varandagourmet = true;
        url_dinamiaca.varandagourmet = 'varandagourmet';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;


    } else {

        $("#varandagourmet").attr("checked", false);
        paramentros.varandagourmet = false;


        paramentros.varandagourmet = false;
        url_dinamiaca.varandagourmet = 'varanda';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;

    }

    gerarUrl(url_dinamiaca);
    carregarImoveis();

});





//SAlAO  DE FESTAS
$('#salaofestas').click(function() {

    //PORTARIA 24 HORAS
    if ($("#salaofestas").is(':checked')) {
        $("#salaofestas").attr("checked", true);

        paramentros.salaofestas = true;
        url_dinamiaca.salaofestas = 'salaofestas';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;


    } else {

        $("#salaofestas").attr("checked", false);
        paramentros.salaofestas = false;


        paramentros.salaofestas = false;
        url_dinamiaca.salaofestas = 'salaofestas';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;

    }

    gerarUrl(url_dinamiaca);
    carregarImoveis();

});






//PMOBILIADO
$('#mobiliado ').click(function() {


    //PORTARIA 24 HORAS
    if ($("#mobiliado").is(':checked')) {
        $("#mobiliado").attr("checked", true);

        paramentros.mobiliado = true;
        url_dinamiaca.mobiliado = 'mobiliado';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;


    } else {

        $("#mobiliado ").attr("checked", false);
        paramentros.mobiliado = false;


        paramentros.mobiliado = false;
        url_dinamiaca.mobiliado = 'mobiliado';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;

    }

    gerarUrl(url_dinamiaca);
    carregarImoveis();

});



//PMOBILIADO
$('#piscina').click(function() {


    //PORTARIA 24 HORAS
    if ($("#piscina").is(':checked')) {
        $("#piscina").attr("checked", true);

        paramentros.piscina = true;
        url_dinamiaca.piscina = 'piscina';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;


    } else {

        $("#piscina").attr("checked", false);
        paramentros.piscina = false;


        paramentros.piscina = false;
        url_dinamiaca.piscina = 'piscina';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;

    }

    gerarUrl(url_dinamiaca);
    carregarImoveis();

});




//PORTARIA SELECIONADA
$('#portaria24h').click(function() {

    //PORTARIA 24 HORAS
    if ($("#portaria24h").is(':checked')) {
        $("#portaria24h").attr("checked", true);

        paramentros.portaria24h = true;
        url_dinamiaca.portaria24h = 'portaria-24horas';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;


    } else {

        $("#portaria24h").attr("checked", false);
        paramentros.portaria24h = false;


        paramentros.portaria24h = false;
        url_dinamiaca.portaria24h = 'sem-portaria-24horas';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;

    }

    gerarUrl(url_dinamiaca);
    carregarImoveis();

});


//PORTARIA arealazer
$('#arealazer').click(function() {

    //PORTARIA 24 HORAS
    if ($("#arealazer").is(':checked')) {
        $("#arealazer").attr("checked", true);

        paramentros.arealazer = true;
        url_dinamiaca.arealazer = 'area-lazer';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;


    } else {

        $("#arealazer").attr("checked", false);
        paramentros.arealazer = false;


        paramentros.arealazer = false;
        url_dinamiaca.arealazer = 'sem-area-lazer';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;

    }

    gerarUrl(url_dinamiaca);
    carregarImoveis();

});


//PORTARIA arealazer
$('#dce').click(function() {

    //PORTARIA 24 HORAS
    if ($("#dce").is(':checked')) {
        $("#dce").attr("checked", true);

        paramentros.dce = true;
        url_dinamiaca.dce = 'dce';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;


    } else {

        $("#dce").attr("checked", false);
        paramentros.dce = false;


        paramentros.dce = false;
        url_dinamiaca.dce = 'sem-dce';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;

    }

    gerarUrl(url_dinamiaca);
    carregarImoveis();

});




//PORTARIA MOBILIA
$('#mobiliado').click(function() {

    //PORTARIA 24 HORAS
    if ($("#mobiliado").is(':checked')) {
        $("#mobiliado").attr("checked", true);

        paramentros.mobiliado = true;
        url_dinamiaca.mobiliado = 'mobiliado';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;


    } else {

        $("#mobiliado").attr("checked", false);
        paramentros.mobiliado = false;


        paramentros.mobiliado = false;
        url_dinamiaca.mobiliado = 'sem-mobiliado';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;

    }

    gerarUrl(url_dinamiaca);
    carregarImoveis();

});




//PORTARIA AREA PRIVATIVA
$('#areaprivativa').click(function() {

    //PORTARIA 24 HORAS
    if ($("#areaprivativa").is(':checked')) {
        $("#areaprivativa").attr("checked", true);

        paramentros.areaprivativa = true;
        url_dinamiaca.areaprivativa = 'areaprivativa';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;


    } else {

        $("#areaprivativa").attr("checked", false);
        paramentros.areaprivativa = false;


        paramentros.areaprivativa = false;
        url_dinamiaca.areaprivativa = 'sem-areaprivativa';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;

    }

    gerarUrl(url_dinamiaca);
    carregarImoveis();

});



//PORTARIA AREA PRIVATIVA
$('#areaservico').click(function() {

    //PORTARIA 24 HORAS
    if ($("#areaservico").is(':checked')) {
        $("#areaservico").attr("checked", true);

        paramentros.areaservico = true;
        url_dinamiaca.areaservico = 'areaservico';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;


    } else {

        $("#areaservico").attr("checked", false);
        paramentros.areaservico = false;


        paramentros.areaservico = false;
        url_dinamiaca.areaservico = 'sem-areaservico';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;

    }

    gerarUrl(url_dinamiaca);
    carregarImoveis();

});



//PORTARIA AREA PRIVATIVA
$('#boxDespejo').click(function() {

    //PORTARIA 24 HORAS
    if ($("#boxDespejo").is(':checked')) {
        $("#boxDespejo").attr("checked", true);

        paramentros.boxDespejo = true;
        url_dinamiaca.boxDespejo = 'box-despejo';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;


    } else {

        $("#boxDespejo").attr("checked", false);
        paramentros.boxDespejo = false;


        paramentros.boxDespejo = false;
        url_dinamiaca.boxDespejo = 'sem-box-despejo';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;

    }

    gerarUrl(url_dinamiaca);
    carregarImoveis();

});



//PORTARIA AREA PRIVATIVA
$('#circuitotv').click(function() {

    //PORTARIA 24 HORAS
    if ($("#circuitotv").is(':checked')) {
        $("#circuitotv").attr("checked", true);

        paramentros.circuitotv = true;
        url_dinamiaca.boxDespejo = 'circuito-tv';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;


    } else {

        $("#circuitotv").attr("checked", false);
        paramentros.circuitotv = false;


        paramentros.circuitotv = false;
        url_dinamiaca.circuitotv = 'sem-circuito-tv';

        //RETORNA PARA PAGINA 1
        paramentros.pagina = 1;
        url_dinamiaca.pagina = 1;

    }

    gerarUrl(url_dinamiaca);
    carregarImoveis();

});






//$('#valor-maximo').mask("#.##0,00", {reverse: true});




//----------------------PAGINAÇAO----------------------/

// -------  PEGA OS PARAMENTRO DA URL
function getParamentros() {


    var vars = [],
        hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }

    return vars;
}




function carregarPaginacao() {



    $('.pagination').empty();



    var total = $('#total-imoveis').text();
    let numeroPagian = Math.ceil(parseInt(total) / 20);

    var pg = getParamentros();

    //    console.log(pg.pagina);


    let setaInicio = '<li onclick="alterarPagina(1)" id= class="page-item"><a class="page-link"  tabindex="-1"><i class="fa ion-arrow-left-b" style="color:var(--cor-primaria)"></i><i class="fa ion-arrow-left-b" style="color:var(--cor-primaria)"></i></a></li>';
    let setaAnterior = '<li onclick="alterarPagina(' + (parseInt(pg.pagina) - 1) + ')" class="page-item"><a class="page-link"  tabindex="-1"><i class="fa ion-arrow-left-b" style="color:var(--cor-primaria)"></i></a></li>';

    $('.pagination').append(setaInicio).append(setaAnterior);



    for (var i = 1; i <= numeroPagian; i++) {

        if ((i + 2) == parseInt(pg.pagina) || (i + 1) == parseInt(pg.pagina) || i == parseInt(pg.pagina) || (i - 2) == parseInt(pg.pagina) || (i - 1) == parseInt(pg.pagina)) {

            $('.pagination').append(' <li  onclick="alterarPagina(' + i + ')" class="page-item"><a  style="' + (i == parseInt(pg.pagina) ? 'background-color:var(--cor-primaria);color:#FFF' : '') + '" class="page-link" >' + i + '</a></li>');

        }

    }

    let setaProximo = '<li onclick="alterarPagina(' + (parseInt(pg.pagina) + 1) + ')" class="page-item"><a class="page-link"  tabindex="-1"><i class="fa ion-arrow-right-b" style="color:var(--cor-primaria)"></i></a></li>';
    let setaUltimo = '<li onclick="alterarPagina(' + numeroPagian + ')" class="page-item"><a class="page-link"  tabindex="-1"><i class="fa ion-arrow-right-b" style="color:var(--cor-primaria)"></i><i class="fa ion-arrow-right-b" style="color:var(--cor-primaria)"></i></a></li>'



    $('.pagination').append(setaProximo).append(setaUltimo);

}


function alterarPagina(pag) {


    var total = $('#total-imoveis').text();
    let numeroPagian = Math.ceil(parseInt(total) / 20);

    if (pag <= 1) {
        pag = 1;
    }

    if (pag >= numeroPagian) {
        pag = numeroPagian;
    }


    //FAZ O SROLL PARA O TOPO DA PAGINA
    $("html, body").animate({ scrollTop: 0 }, "slow");

    paramentros.pagina = pag;
    url_dinamiaca.pagina = pag;


    carregarImoveis();

}

/*
 *
 *
 * Carregamento dos imoveis via URL
 *
 *
 */

function carregarPaginaUrl() {

    //PEGAR URL DIGITADA
    var url = [];
    paramAux = {};

    var url = window.location.href.slice(window.location.href.indexOf('test') + 1).split('/');

    paramAux.finalidade = url['1'];
    paramAux.codigoTipo = url['2'];
    paramAux.codigocidade = url['3'];
    paramAux.codigosbairros = url['4'];
    paramAux.numeroquartos = url['5'];
    paramAux.numerosuite = url['6'];
    paramAux.numerovagas = url['7'];
    paramAux.numerobanhos = url['8'];
    paramAux.portaria24h = url['9'];
    paramAux.arealazer = url['10'];
    paramAux.dce = url['11'];
    paramAux.mobiliado = url['12'];
    paramAux.areaprivativa = url['13'];
    paramAux.areaservico = url['14'];
    paramAux.boxDespejo = url['15'];
    paramAux.circuitotv = url['16'];

    paramentros.finalidade = paramAux.finalidade;
    url_dinamiaca.finalidade = paramAux.finalidade;


    var parametrosURL = getParamentros();

    //ATUALIZAR VALORES MINIMOS E MAXIMOS
    $('#valor-minimo').val(parametrosURL['valorminimo']);
    $('#valor-maximo').val(parametrosURL['valormaximo']);


    if (parametrosURL['valorminimo'] === undefined || parametrosURL['valorminimo'] == '') {

        url_dinamiaca.valorde = 0;
        paramentros.valorde = 0;

    } else {

        url_dinamiaca.valorde = parametrosURL['valorminimo'];
        paramentros.valorde = parametrosURL['valorminimo'];
    }


    if (parametrosURL['valormaximo'] === undefined || parametrosURL['valormaximo'] == '') {

        url_dinamiaca.valorate = 0;
        paramentros.valorate = 0;

    } else {

        url_dinamiaca.valorate = parametrosURL['valormaximo'];
        paramentros.valorate = parametrosURL['valormaximo'];
    }

    if (parametrosURL['pagina'] == undefined || parametrosURL['pagina'] == '') {

        url_dinamiaca.pagina = 1;
        paramentros.pagina = 1;

    } else {

        url_dinamiaca.pagina = parametrosURL['pagina'];
        paramentros.pagina = parametrosURL['pagina'];
    }


    //PEGAR NUMERO DE QUARTOS

    if (paramAux.numeroquartos == '0-quartos' || paramAux.numeroquartos == '0-quartos' || paramAux.numeroquartos == '' || paramAux.numeroquartos === undefined) {
        paramentros.numeroquartos = '0-quartos';
        url_dinamiaca.quarto = '0-quartos';


    }

    if (paramAux.numeroquartos == '1-quartos') {
        paramentros.numeroquartos = '1-quartos';
        url_dinamiaca.quarto = '1-quartos';

        $('#quarto_check_1').addClass('active_buttom');
    }

    if (paramAux.numeroquartos == '2-quartos') {
        paramentros.numeroquartos = '2-quartos';
        url_dinamiaca.quarto = '2-quartos';

        $('#quarto_check_2').addClass('active_buttom');
    }
    if (paramAux.numeroquartos == '3-quartos') {
        paramentros.numeroquartos = '3-quartos';
        url_dinamiaca.quarto = '3-quartos';

        $('#quarto_check_3').addClass('active_buttom');
    }
    if (paramAux.numeroquartos == '4-quartos') {
        paramentros.numeroquartos = '4-quartos';
        url_dinamiaca.quarto = '4-quartos';

        $('#quarto_check_4').addClass('active_buttom');
    }



    //PEGAR NUMERO DE VAGAS

    if (paramAux.numerovagas == '0-vaga-ou-mais' || paramAux.numerovagas == '0-vaga' || paramAux.numerovagas == '' || paramAux.numerovagas === undefined) {
        paramentros.numerovagas = '0-vaga-ou-mais';
        url_dinamiaca.vagas = '0-vaga-ou-mais';

    }

    if (paramAux.numerovagas == '1-vaga-ou-mais') {
        paramentros.numerovagas = '1-vaga-ou-mais';
        url_dinamiaca.vagas = '1-vaga-ou-mais';

        $('#vagas_check_1').addClass('active_buttom');
    }

    if (paramAux.numerovagas == '2-vaga-ou-mais') {
        paramentros.numerovagas = '2-vaga-ou-mais';
        url_dinamiaca.vagas = '3-vaga-ou-mais';

        $('#vagas_check_2').addClass('active_buttom');

    }
    if (paramAux.numerovagas == '3-vaga-ou-mais') {
        paramentros.numerovagas = '3-vaga-ou-mais';
        url_dinamiaca.vagas = '3-vaga-ou-mais';

        $('#vagas_check_3').addClass('active_buttom');
    }
    if (paramAux.numerovagas == '4-vaga-ou-mais') {
        paramentros.numerovagas = '4-vaga-ou-mais';
        url_dinamiaca.vagas = '4-vaga-ou-mais';

        $('#vagas_check_4').addClass('active_buttom');

    }



    //PEGAR NUMERO DE SUITES

    if (paramAux.numerosuite == '0-suite-ou-mais' || paramAux.numerosuite == '' || paramAux.numeroquartos === undefined) {
        paramentros.numerosuite = '0-suite-ou-mais';
        url_dinamiaca.numerosuite = '0-suite-ou-mais';


    }
    if (paramAux.numerosuite == '1-suite-ou-mais') {
        paramentros.numerosuite = '1-suite-ou-mais';
        url_dinamiaca.suite = '1-suite-ou-mais';

        $('#suite_check_1').addClass('active_buttom');

    }
    if (paramAux.numerosuite == '2-suite-ou-mais') {
        paramentros.numerosuite = '2-suite-ou-mais';
        url_dinamiaca.suite = '2-suite-ou-mais';

        $('#suite_check_2').addClass('active_buttom');
    }
    if (paramAux.numerosuite == '3-suite-ou-mais') {
        paramentros.numerosuite = '3-suite-ou-mais';
        url_dinamiaca.suite = '3-suite-ou-mais';


        $('#suite_check_3').addClass('active_buttom');
    }
    if (paramAux.numerosuite == '4-suite-ou-mais') {
        paramentros.numerosuite = '4-suite-ou-mais';
        url_dinamiaca.suite = '4-suite-ou-mais';


        $('#suite_check_4').addClass('active_buttom');

    }




    //SE O BANHEIRO FOR INDEFINIDO  EXECUTE
    if (paramAux.numerobanhos !== undefined) {

        paramAux.numerobanhos = paramAux.numerobanhos.split('?');

        if (paramAux.numerobanhos[0] == '0-banheiro-ou-mais' || paramAux.numerobanhos == '0-banheiro-ou-mais' || paramAux.numerobanhos == '' || paramAux.numerobanhos === undefined) {
            paramentros.numerobanhos = '0-banheiro-ou-mais';
            url_dinamiaca.banheiro = '0-banheiro-ou-mais';

        }
        if (paramAux.numerobanhos[0] == '1-banheiro-ou-mais') {

            paramentros.numerobanhos = '1-banheiro-ou-mais';
            url_dinamiaca.banheiro = '1-banheiro-ou-mais';

            $('#benheiro_check_1').addClass('active_buttom');
        }
        if (paramAux.numerobanhos[0] == '2-banheiro-ou-mais') {
            paramentros.numerobanhos = '2-banheiro-ou-mais';
            url_dinamiaca.banheiro = '2-banheiro-ou-mais';

            $('#benheiro_check_2').addClass('active_buttom');
        }
        if (paramAux.numerobanhos[0] == '3-banheiro-ou-mais') {
            paramentros.numerobanhos = '3-banheiro-ou-mais';
            url_dinamiaca.banheiro = '3-banheiro-ou-mais';

            $('#benheiro_check_3').addClass('active_buttom');
        }
        if (paramAux.numerobanhos[0] == '4-banheiro-ou-mais') {
            paramentros.numerobanhos = '4-banheiro-ou-mais';
            url_dinamiaca.banheiro = '4-banheiro-ou-mais';

            $('#benheiro_check_4').addClass('active_buttom');
        }

        if (paramAux.numerobanhos[0] == '' || paramAux.numerobanhos[0] == undefined) {
            paramentros.numerobanhos = '0-banheiro-ou-mais';
            url_dinamiaca.banheiro = '0-banheiro-ou-mais';


        }

        paramAux.numerobanhos = paramAux.numerobanhos[0];

    } else {

        paramentros.numerobanhos = '0-banheiro-ou-mais';
    }




    // ------------  MAIS FILTROS ,



    url_dinamiaca.portaria24h = paramAux.portaria24h;
    url_dinamiaca.arealazer = paramAux.arealazer;
    url_dinamiaca.dce = paramAux.dce;
    url_dinamiaca.mobiliado = paramAux.mobiliado;
    url_dinamiaca.areaprivativa = paramAux.areaprivativa;
    url_dinamiaca.areaservico = paramAux.areaservico;
    url_dinamiaca.boxDespejo = paramAux.boxDespejo;
    url_dinamiaca.circuitotv = paramAux.circuitotv;
    url_dinamiaca.circuitotv = paramAux.circuitotv.split('?');
    url_dinamiaca.circuitotv = url_dinamiaca.circuitotv[0];



    //ACADEMIA
    if ($("#academia").is(':checked') == false && url_dinamiaca.academia == 'academia') {

        $("#academia").attr("checked", true);
        paramentros.academia = true;

    } else if ($("#academia").is(':checked') == false && url_dinamiaca.academia == 'academia') {

        $("#academia").attr("checked", false);
        paramentros.academia = false;
    }




    //PORTARIA 24 HORAS
    if ($("#portaria24h").is(':checked') == false && url_dinamiaca.portaria24h == 'portaria-24horas') {

        $("#portaria24h").attr("checked", true);
        paramentros.portaria24h = true;

    } else if ($("#portaria24h").is(':checked') == false && url_dinamiaca.portaria24h == 'sem-portaria-24horas') {

        $("#portaria24h").attr("checked", false);
        paramentros.portaria24h = false;
    }


    //AREA DE LAZER
    if ($("#arealazer").is(':checked') == false && url_dinamiaca.arealazer == 'area-lazer') {

        paramentros.arealazer = true;
        $("#arealazer").attr("checked", true);


    } else if ($("#arealazer").is(':checked') == false && url_dinamiaca.arealazer == 'sem-area-lazer') {

        paramentros.arealazer = false;
        $("#arealazer").attr("checked", false);
    }


    // DCE
    if ($("#dce").is(':checked') == false && url_dinamiaca.dce == 'dce') {

        $("#dce").attr("checked", true);

        paramentros.dce = true;


    } else if ($("#dce").is(':checked') == false && url_dinamiaca.dce == 'sem-dce') {

        $("#dce").attr("checked", false);
        paramentros.dce = false;
    }



    //AREA  MOBILIADO
    if ($("#mobiliado").is(':checked') == false && url_dinamiaca.mobiliado == 'mobiliado') {

        $("#mobiliado").attr("checked", true);
        paramentros.mobiliado = true;

    } else if ($("#mobiliado").is(':checked') == false && url_dinamiaca.mobiliado == 'sem-mobilia') {

        $("#mobiliado").attr("checked", false);

        paramentros.mobiliado = false;
    }


    //AREA  areaprivativa
    if ($("#areaprivativa").is(':checked') == false && url_dinamiaca.areaprivativa == 'area-privativa') {

        $("#areaprivativa").attr("checked", true);

        paramentros.areaprivativa = true;

    } else if ($("#areaprivativa").is(':checked') == false && url_dinamiaca.areaprivativa == 'sem-areaprivativa') {

        $("#areaprivativa").attr("checked", false);

        paramentros.areaprivativa = false;
    }



    //AREA  areaservico
    if ($("#areaservico").is(':checked') == false && url_dinamiaca.areaservico == 'area-servico') {

        $("#areaservico").attr("checked", true);

        paramentros.areaservico = true;

    } else if ($("#areaservico").is(':checked') == false && url_dinamiaca.areaservico == 'sem-area-servico') {

        $("#areaservico").attr("checked", false);

        paramentros.areaservico = false;
    }

    //AREA  areaservico
    if ($("#boxDespejo").is(':checked') == false && url_dinamiaca.boxDespejo == 'box-despejo') {

        $("#boxDespejo").attr("checked", true);

        paramentros.boxDespejo = true;

    } else if ($("#areaservico").is(':checked') == false && url_dinamiaca.boxDespejo == 'sem-box-despejo') {

        $("#boxDespejo").attr("checked", false);

        paramentros.boxDespejo = false;
    }


    //AREA  areaservico
    if ($("#circuitotv").is(':checked') == false && url_dinamiaca.circuitotv == 'circuito-tv') {

        $("#circuitotv").attr("checked", true);

        paramentros.circuitotv = true;


    } else if ($("#circuitotv").is(':checked') == false && url_dinamiaca.circuitotv == 'sem-circuito-tv') {

        $("#circuitotv").attr("checked", false);

        paramentros.circuitotv = false;
    }





    //BANHEIRO
    $('#benheiro_check button').each(function(i, button) {

        //        let banheiro = $(button).text();
        //        
        //        if(paramAux.numerobanhos == ){
        //            
        //        }
        //        
        //        alert(banheiro);

    });



    //    paramentros.finalidade = paramAux.finalidade;
    //    url_dinamiaca.finalidade = paramAux.finalidade;



    // $('#finalidade-input-busca option').removeAttr("selected");;



    //  $('#finalidade-input-busca option')[0].removeAttr("selected");
    //  $('#finalidade-input-busca option')[1].removeAttr("selected");
    //    $("option:selected").prop("selected", false)


    //Atualizar formulario de 
    $('#finalidade-input-busca option').each(function(index, fin) {


        if (paramentros.finalidade == 'venda') {

            $(fin).attr('selected', "selected");

            $('#aba-finalidade-venda').addClass('active');
            $('#aba-finalidade-aluguel').removeClass('active');
            


            return false;

        } else if (paramentros.finalidade == 'aluguel') {
            $(fin).attr('selected', "selected");

            $('#aba-finalidade-aluguel').addClass('active');
            $('#aba-finalidade-venda').removeClass('active');

            return false;

        }

    });

    //ATUALIZAÇÃO DA ABA FINALIADE
    if (paramentros.finalidade == 'aluguel') {
        $('#aba-finalidade-venda').removeClass('active');
        $('#aba-finalidade-aluguel').addClass('active');
        
    }

    if (paramentros.finalidade == 'aluguel') {
        $('#aba-finalidade-venda').removeClass('active');
        $('#aba-finalidade-aluguel').addClass('active');
    }


    //QUEBRA URL EM ARRAY
    if (paramAux.codigoTipo == undefined || paramAux.codigoTipo == '') {

        paramAux.codigoTipo = 'imoveis';

    } else {
        paramAux.codigoTipo = paramAux.codigoTipo.split('--');
    }




    /*
     * 
     * PEGAR O TIPO DOS IMÓVEIS
     * 
     */
    objAux = $('#tipo-input-busca');
    objtipo.tipo = paramAux.codigoTipo;


    let arrayCodigoTipo = [];
    let arraynomeTipo = [];


    

    //PEGAR O CODIGO E NO NOME DO TIPO
    for (var i = 0; i < paramAux.codigoTipo.length; i++) {

        $.each(objAux.children(), function(index, value) {

            if ($(value).val() == objtipo.tipo[i]) {

                arrayCodigoTipo.push($(value).attr('id'));
                arraynomeTipo.push($(value).val());
            }
        });
    }

    //GRAVA NO PARAMETRO O CÓDIGO E O NOME
    paramentros.codigoTipo = {
        'codigo': arrayCodigoTipo,
        'nome': objtipo.tipo
    };

    //SALVA A URL E AUALIZA A URL DO BROWSER
    url_dinamiaca.tipo = paramentros.codigoTipo;
    //    gerarUrl(url_dinamiaca);


    //PEGAR O NOME E O CODIGO DA CIDADE
    varAuxTeste = paramAux.codigocidade;

    let bairroOBJ = $('#cidade-input-busca');

    $('#cidade-input-busca option').each(function(index, valor) {

        if (varAuxTeste == $(valor).val()) {

            $(this).attr('selected', "selected");

            paramAux.codigocidade = $(valor).attr('codigo');
            paramAux.valCidade = $(valor).val();

            url_dinamiaca.cidade = paramAux.valCidade;
            paramentros.codigocidade = paramAux.codigocidade;

        }

    });


    /*
     * 
     * CARREGAMETO DE BAIRROS PELA URL
     * 
     */



    if (paramAux.codigosbairros == undefined || paramAux.codigosbairros == '' || paramAux.codigocidade == '0') {

        paramAux.codigosbairros = 'todos-os-bairros';

    } else {

        //PEGA TODOS OS BAIRROS DE ACORDO COM A CIDADE SELECIONADA


        $.ajax({
            method: "POST",
            url: base_url + 'bairroAjax', //ImovelController
            async: false,
            data: { 'cidade': paramAux.codigocidade },
            data: {},
            beforeSend: function() {

            }
        }).done(function(bairros) {

            //REMOVE O SELECT 
            $('#bairro-input-busca').empty();

            //INSERE UM NOVO SELECT SEM A CLASS "selectpicker"

            //            let bairroWeb = $('<select>');;
            //            bairroWeb.attr("id", "bairro-input-busca")
            //                    .css('width', '100%')
            //                    .addClass('search-fields')
            //                    .attr('name', 'property-types')
            //                    .attr('data-live-search', 'true')
            //                    .attr('data-live-search-placeholder', 'Search')
            //                    .on('change', selecionarBairro);

            var cidadeSelecionadaBusca = paramAux.valCidade;



            $('#bairro-input-busca').append('<option  codigo="0" value="todos-os-bairros" >Todos os bairros</option>');

            $.each(bairros, function(key2, bairro) {

                if (cidadeSelecionadaBusca == bairro.cidade) {
                    $('#bairro-input-busca').append('<option codigo="' + bairro.codigo + '" value="' + bairro.nome + '">' + bairro.nomeOriginal + '</option>');
                }
            });

            //PEGAR O BAIRRO
            let bairro = $("#bairro-input-busca");

            paramAux.codigosbairros = paramAux.codigosbairros.split('--');

            //FILTRAR OS BAIRROS DE ACORDO COM A CIDADE
            let bairroSelecionado = paramAux.codigosbairros;

            for (var i = 0; i < bairroSelecionado.length; i++) {
                $.each(bairro.children(), function(index, value) {

                    if ($(value).val() == bairroSelecionado[i]) {

                        arrayBairro.codigo.push($(value).attr('codigo'));
                        arrayBairro.nome.push($(value).val());
                    }

                    if ($(value).val() == 'todos-os-bairros') {

                        arrayBairro.codigo = [];
                        arrayBairro.nome = [];
                    }
                });
            }

            url_dinamiaca.bairro = arrayBairro
            paramentros.codigosbairros = arrayBairro;

            //            gerarUrl(url_dinamiaca);
            //            carregarImoveis();

        }).always(function() {

        });
    }

    gerarUrl(url_dinamiaca);
    carregarImoveis();

}

carregarTipos();

$('.menu-busca-mobile').click(function() {

    $('#corpo-filtro').toggle('slow');

});


$("html, body").animate({ scrollTop: 0 }, "slow");