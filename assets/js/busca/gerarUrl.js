
var url_dinamiaca = {};
url_dinamiaca.pagina = 1;
url_dinamiaca.valorde = 0;
url_dinamiaca.valorate = 0;

url_dinamiaca.finalidade = 'venda';
url_dinamiaca.tipo = ['imoveis'];
url_dinamiaca.cidade = 'todas-as-cidades';
url_dinamiaca.bairro = ['todos-os-bairros'];
url_dinamiaca.quarto = '0-quartos';
url_dinamiaca.suite = '0-suite-ou-mais';
url_dinamiaca.vagas = '0-vaga';
url_dinamiaca.banheiro = '0-banheiro-ou-mais';
url_dinamiaca.portaria24h = 'sem-portaria-24horas';
url_dinamiaca.arealazer = 'sem-area-lazer';
url_dinamiaca.dce = 'sem-dce';
url_dinamiaca.mobiliado = 'sem-mobilia';
url_dinamiaca.areaprivativa = 'sem-area-privativa';
url_dinamiaca.areaservico = 'sem-area-servico';
url_dinamiaca.boxDespejo = 'sem-box-despejo';
url_dinamiaca.circuitotv = 'sem-circuito-tv';

url_dinamiaca.academia = 'sem-academia';
url_dinamiaca.elevador = 'sem-elevador';
url_dinamiaca.varandagourmet = 'sem-varanda-gurmet';
url_dinamiaca.piscina = 'sem-piscina';
url_dinamiaca.salaofestas = 'salao-festa';




function url_tipo(param_url_tipo) {
//    console.log(param_url_tipo);debugger;



    let t_tipos = '';

    $('#list_tipos').empty();

    for (var i = 0; i < param_url_tipo.nome.length; i++) {


        if (param_url_tipo.nome[i] != 'imoveis') {

            let objSpanBadges = $('<span>')
                    .attr('id', param_url_tipo.codigo[i])
                    .addClass('badge badge-secondary marcador-de-busca')
                    .css('margin-left', '5px')
                    .text(param_url_tipo.nome[i] + ' x')
                    .on('click', removerTIpoBusca);


            $('#list_tipos').append(objSpanBadges);

        }

        t_tipos += param_url_tipo.nome[i] + '--';

    }


    //REMOVE OS DOIS ULTIMOS CARATERES DA STRING
    t_tipos = t_tipos.substring(0, (t_tipos.length - 2));

    return t_tipos;
}


function url_bairro(param_url_bairro) {
//    console.log(param_url_tipo);debugger;


    let t_bairros = '';

    $('#list_bairros').empty();

    for (var i = 0; i < param_url_bairro.nome.length; i++) {


        let objbadges = $('<span>')
                .attr('id', param_url_bairro.codigo[i])
                .addClass('badge badge-secondary marcador-de-busca')
                .css('margin-left', '5px')
                .text(param_url_bairro.nome[i] + ' x')
                .on('click', removerBairroBusca);



        $('#list_bairros').append(objbadges);

        t_bairros += param_url_bairro.nome[i] + '--';
    }


    //REMOVE OS DOIS ULTIMOS CARATERES DA STRING
    t_bairros = t_bairros.substring(0, (t_bairros.length - 2));

    return t_bairros;
}

function gerarUrl(url) {



    let u = '';
    let collection_tipo = '';

    //TRATAR URL BAIRROS


    if (url_dinamiaca.tipo.nome.length > 1 && url_dinamiaca.tipo.nome[0] == 'imoveis') {

        url_dinamiaca.tipo.nome.shift(1);
        url_dinamiaca.tipo.codigo.shift(1);
    }

    if (url_dinamiaca.tipo[0] == 'imoveis') {

        collection_tipo = 'imoveis';

    } else {

        if (url_dinamiaca.tipo.codigo.length > 0) {

            collection_tipo = url_tipo(url_dinamiaca.tipo);

        } else {
            url_dinamiaca.tipo.codigo = 0;
            collection_tipo = 'imoveis';
        }
    }




    //TRATAR URL BAIRROS

    let collection_bairro = '';

    if (url_dinamiaca.bairro[0] == 'todos-os-bairros') {

        collection_bairro = 'todos-os-bairros';

    } else {

        if (url_dinamiaca.bairro.codigo.length > 0) {

            collection_bairro = url_bairro(url_dinamiaca.bairro);

        } else {
            url_dinamiaca.bairro.codigo = 0;
            collection_bairro = 'todos-os-bairros';
        }
    }


    u = url_dinamiaca.finalidade + '/'
            + collection_tipo + '/'
            + url_dinamiaca.cidade + '/'
            + collection_bairro + '/'
            + url_dinamiaca.quarto + '/'
            + url_dinamiaca.suite + '/'
            + url_dinamiaca.vagas + '/'
            + url_dinamiaca.banheiro + '/'
            + url_dinamiaca.portaria24h + '/'
            + url_dinamiaca.arealazer + '/'
            + url_dinamiaca.dce + '/'
            + url_dinamiaca.mobiliado + '/'
            + url_dinamiaca.areaprivativa + '/'
            + url_dinamiaca.areaservico + '/'
            + url_dinamiaca.boxDespejo + '/'
            + url_dinamiaca.circuitotv + '/'
            +'?valorminimo=' + url_dinamiaca.valorde + '&valormaximo=' + url_dinamiaca.valorate + '&pagina=' + url_dinamiaca.pagina + '';

    window.history.replaceState('Object', 'Categoria JavaScript', base_url + u);

}


function removerBairroBusca() {


    let bairroRemove = $(this);
//    let novosTipo = '';
    let novoBairro = {};


    $.each(url_dinamiaca.bairro.codigo, function (b, bairro) {

        if (bairro == bairroRemove.attr('id')) {

            let teste = bairroRemove.remove();
            novoBairro.codigo = bairro;
            novoBairro.nome = url_dinamiaca.bairro.nome[b];
        }

    });

    var novoArrayBairro = {
        codigo: [],
        nome: []
    };


    $.each(url_dinamiaca.bairro.codigo, function (cont, valor) {

        if (valor != novoBairro.codigo) {

            novoArrayBairro.codigo.push(url_dinamiaca.bairro.codigo[cont]);
            novoArrayBairro.nome.push(url_dinamiaca.bairro.nome[cont]);
        }

    });


    url_dinamiaca.bairro = {
        'nome': novoArrayBairro.nome,
        'codigo': novoArrayBairro.codigo
    }



    novoBairro.bairro = url_dinamiaca.bairro.nome;

    arrayBairro = novoArrayBairro;


    if (paramentros.codigosbairros.nome.length <= 1) {

        paramentros.codigosbairros = {
            'nome': 0,
            'codigo': 0
        };

        carregarBairrosBusca();
        $('#list_tipos').empty();


    } else {

        paramentros.codigosbairros = novoArrayBairro;
        url_dinamiaca.bairro = novoArrayBairro;
    }


    paramentros.pagina = 1;
    url_dinamiaca.pagina = 1;

    gerarUrl();
    carregarImoveis();
}


function removerTIpoBusca() {



    let tipoRemove = $(this);

//    let novosTipo = '';
    let novoTipo = {};


    $.each(url_dinamiaca.tipo.codigo, function (t, tipo) {

        if (tipo == tipoRemove.attr('id')) {

            let teste = tipoRemove.remove();
            novoTipo.codigo = tipo;
            novoTipo.nome = url_dinamiaca.tipo.nome[t];
        }

    });

    var novoArrayDeTipo = {
        codigo: [],
        nome: []
    };

    $.each(url_dinamiaca.tipo.codigo, function (cont, valor) {


        if (valor != novoTipo.codigo) {

            novoArrayDeTipo.codigo.push(url_dinamiaca.tipo.codigo[cont]);
            novoArrayDeTipo.nome.push(url_dinamiaca.tipo.nome[cont]);
        }

    });

    url_dinamiaca.tipo = {
        'nome': novoArrayDeTipo.nome,
        'codigo': novoArrayDeTipo.codigo
    }



    objtipo.tipo = url_dinamiaca.tipo.nome;



    if (paramentros.codigoTipo.nome.length <= 1) {

//        $('#tipo-input-busca > option').each(function (i, v) {
//
//            $(v).attr('selected', '');
//
//            if ($(v).attr('id') == '0') {
//
//                $(v).attr('selected', 'selected');
//
//            } else {
//
//                $(v).attr('selected', false);
//            }
//        });

        paramentros.codigoTipo = {
            'nome': 0,
            'codigo': 0
        };

        carregarTipos();

    } else {

        //if (paramentros.codigoTipo.nome[0] == 'imoveis' || paramentros.codigoTipo.nome[1] == 'imoveis') {

        paramentros.codigoTipo = novoArrayDeTipo;
        //}

    }


    gerarUrl();
    carregarImoveis();


}
