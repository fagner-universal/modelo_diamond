var formContato = {};

function enviarFormulario(parametros_form) {

    $.ajax({
        method: "POST",
        url: base_url + 'contatos/enviar', //ImovelController
        async: true,
        data: {contato: parametros_form},
        beforeSend: function () {
        
       
             $('.msg-contato').append('<img width="50px" style="margin-bottom: 10px;" class="rounded mx-auto d-block" src="'+base_url+'assets/images/gif/loading.gif" />');
        }
    }).done(function (dados) {


          $('.msg-contato').empty();


        if (dados) {

            let msg = '<div class="alert alert-success" role="alert">Enviado com sucesso</div>';
            $('.msg-contato').append(msg);

        }

        //ADICIONA A CLASS SELECT  "selectpicker" APOS SELECT PREENCHIDO
        // $('#cidade-input-busca').selectpicker();

    }).fail(function (cidades) {

        console.log('erro ao enviar email');
        alert(console.log('erro ao enviar email'));
       

    }).always(function (cidades) {
        
   

        $('#name_contato').val('');
        $('#email_contato').val('');
        $('#tel_fixo').val('');
        $('#tel_celular').val('');
        $('#msg_contato').val('');
        
       
        setTimeout(function ()
        {
            $('.msg-contato').empty();

        }, 3000);


    });

}

$('#button_forms').on('click', function () {


    formContato.nome = $('#name_contato').val();
    formContato.email = $('#email_contato').val();
    formContato.fixo = $('#tel_fixo').val();
    formContato.celular = $('#tel_celular').val();
    formContato.msg = $('#msg_contato').val();

    let validacao = validarFormContato(formContato);

    if (validacao) {
        enviarFormulario(formContato);
    }

});



//MASCARA PARA INPUT
$('#tel_fixo').mask('(00) 0000-0000');
$('#tel_celular').mask('(00) 0 0000-0000');




function validarFormContato(param) {



    if (formContato.nome == '') {
        alert('Nome nao informado');
        return false;

    } else if (formContato.email == '') {

        alert('E-mail nao informado');
        return false;

    } else if (formContato.msg == '') {

        alert('Nenhuma mensagem informada');
        return false;

    } else if (formContato.celular == '') {

        alert('Celular nao informado');
        return false;

    }

    return true;

}