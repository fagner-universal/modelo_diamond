var formAnunciar = {};

function enviarFormularioAnunciar(parametros_form_anucniar) {


    $.ajax({
        method: "POST",
        url: base_url + 'anunciar/enviarForm', //ImovelController
        async: false,
        data: {imovel: parametros_form_anucniar},
        beforeSend: function () {

            $('.msg-contato').append('<img width="50px" style="margin-bottom: 10px;" class="rounded mx-auto d-block" src="' + base_url + 'assets/images/gif/loading.gif" />');
        }
    }).done(function (dados) {
        
        $('.msg-contato').empty();

        if (dados == true  || dados != '' ) {
          let msg = '<div class="alert alert-success" role="alert">Enviado com sucesso</div>';
           $('.msg-contato').append(msg);

        }
        

    }).fail(function (dados) {
        
        alert('erro ao enviar email');
        alert(console.log('erro ao enviar email'));

    }).always(function (dados) {


        $('#name_anunciar').val('');
        $('#email_anuciar').val('');
        $('#fixo_anuciar').val('');
        $('#celular_anuciar').val('');
        $('#endereco_anunciar').val('');
        $('#n_anunciar').val('');
        $('#bairro_anunciar').val('');
        $('#cidade_anunciar').val('');
        $('#estado_anunciar').val('');
        $('#cep_anunciar').val('');
        $('#tipo_imovel_anunciar').val('');
        $('#tipo_imovel_finalidade').val('');
        $('#mensagem_anunciar').val('');


        setTimeout(function ()
        {
            $('.msg-contato').empty();

        }, 3000);


    });

}



$('#button_forms_anunciar').on('click', function () {

    formAnunciar.nome = $('#name_anunciar').val();
    formAnunciar.email = $('#email_anuciar').val();
    formAnunciar.fixo = $('#fixo_anuciar').val();
    formAnunciar.celular = $('#celular_anuciar').val();
    formAnunciar.endereco = $('#endereco_anunciar').val();
    formAnunciar.numero = $('#n_anunciar').val();
    formAnunciar.bairro = $('#bairro_anunciar').val();
    formAnunciar.cidade = $('#cidade_anunciar').val();
    formAnunciar.estado = $('#estado_anunciar').val();
    formAnunciar.cep = $('#cep_anunciar').val();
    formAnunciar.tipo = $('#tipo_imovel_anunciar').val();
    formAnunciar.finalidade = $('#tipo_imovel_finalidade').val();
    formAnunciar.mensagem = $('#mensagem_anunciar').val();



    let validacao = validarFormAnunciar(formAnunciar);


    if (validacao) {
        enviarFormularioAnunciar(formAnunciar);
    }

});


//MASCARA PARA INPUT
$('#fixo_anuciar').mask('(00) 0000-0000');
$('#celular_anuciar').mask('(00) 0 0000-0000');




function validarFormAnunciar(param) {


    if (param.nome == '') {
        alert('Nome nao informado');
        return false;

    } else if (param.email == '') {

        alert('E-mail nao informado');
        return false;

    } else if (param.celular == '') {

        alert('Celular nao informado');
        return false;
    } else if (param.endereco == '') {

        alert('endereco nao informado');
        return false;

    } else if (param.numero == '') {

        alert('numero nao informado');
        return false;

    } else if (param.bairro == '') {

        alert('bairro nao informado');
        return false;

    } else if (param.cidade == '') {

        alert('cidade nao informada');
        return false;

    } else if (param.estado == '') {

        alert('Estado nao informado');
        return false;

    } else if (param.mensagem == '') {

        alert('Nenhuma mensagem informada');
        return false;

    } else if (param.tipo == '') {

        alert('Tipo de imóvel nao informado');
        return false;

    }


    return true;

}

$.ajax({
    url: '' + base_url + 'tipoAjax',
    type: 'GET',
    data: {},
    success: function (tipos) {

        $.each(tipos, function (todostipo, tipo) {

            $('#tipo_imovel_anunciar').append('<option  value="' + tipo.nome + '">' + tipo.nomeOriginal + '</option>');

        });

    }
}).then(function () {

});
