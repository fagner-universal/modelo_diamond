var busca_imoveis = {};


busca_imoveis.finalidade = 'aluguel';
busca_imoveis.tipo = 'imoveis';
busca_imoveis.cidade = 'todas-as-cidades';
busca_imoveis.valorde = 0;
busca_imoveis.valorate = 0;
busca_imoveis.quartos = 0;
busca_imoveis.bairro = 'todos-os-bairros';

busca_imoveis.suite = 1;
busca_imoveis.vagas = '1-vaga';
busca_imoveis.arealazer = false;
busca_imoveis.quartos = '1-quartos';
busca_imoveis.numeroelevador = 0;


$('.finalidade_home').on('click', function() {
    busca_imoveis.finalidade = $(this).attr('id');
    console.log(busca_imoveis);
});


$('.tipoImoveis_home').on('change', function() {
    busca_imoveis.tipo = $(this).val();
    console.log(busca_imoveis);
});


$('.cidade_home').on('change', function() {
    busca_imoveis.cidade = $(this).val();
    console.log(busca_imoveis);
    carregarBairros();
});

$('.bairro_home').on('change', function() {
    busca_imoveis.bairro = $(this).val();

});

$('.valorde_home').on('change', function() {

    busca_imoveis.valorde = $(this).val();
    console.log(busca_imoveis);

});

$('.valorate_home').on('change', function() {

    busca_imoveis.valorate = $(this).val();
    console.log(busca_imoveis);

});


$('#quartos_home').on('change', function() {

    busca_imoveis.quartos = $(this).val();
    console.log(busca_imoveis);
});





$('.suite_home').on('click', function() {

    if ($(this).is(':checked')) {

        busca_imoveis.suite = 0;

    } else {

        busca_imoveis.suite = 1;
    }

});

$('.suite_garagem').on('click', function() {

    if ($(this).is(':checked')) {

        busca_imoveis.suite = 1;


    } else {

        busca_imoveis.suite = 1;

    }

});

$('.elevador_home').on('click', function() {

    if ($(this).is(':checked')) {

        busca_imoveis.numeroelevador = 0;

    } else {

        busca_imoveis.numeroelevador = 0;
    }

});

$('.area_lazer_home').on('click', function() {

    if ($(this).is(':checked')) {

        busca_imoveis.arealazer = true;

    } else {

        busca_imoveis.arealazer = false;
    }

});





$('.btBuscar_home').on('click', function() {

    var bairro = $('.bairro').val()
    console.log(bairro)
    let url_home = base_url + busca_imoveis.finalidade + "/" + busca_imoveis.tipo + "/" + busca_imoveis.cidade + "/" + bairro + "/0-quartos/0-suite-ou-mais/0-vaga-ou-mais/0-banheiro-ou-mais/sem-portaria-24horas/sem-area-lazer/sem-dce/sem-mobilia/sem-area-privativa/sem-area-servico/sem-box-despejo/sem-circuito-tv/?valorminimo=0&valormaximo=0&pagina=1";

    $(location).attr('href', url_home);
    debugger;

});



//buscar pelo código
$('#button-addon2').click(function() {

    let codigo = $('#codigoImovel').val();

    $.ajax({
        url: '' + base_url + 'codigoImovelAjax/?codigo=' + codigo + '&finalidade=' + busca_imoveis.finalidade + '',
        type: 'GET',
        data: {},
        success: function(codigo) {


            //ABRIR MODAL APÓS A BUSCA NA API
            $('#myModal').modal('toggle');


            //LIMPAR CONTEUDO DO MODAL
            $('.conteudoModal').empty();

            $.each(codigo.lista, function(codInce, codValor) {

                var imagemModal = '';

                if (codValor.urlfotoprincipal == '' || codValor.urlfotoprincipal == null) {

                    imagemModal = 'https://app.imoview.com.br/Front/img/house1.png';

                } else {

                    imagemModal = codValor.urlfotoprincipal;
                }


                //                let linha = '<div class=" col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-bottom: 10px">' +
                //                        '<div class="card" style=" box-shadow:10px 10px 10px 0px #8888884f">' +
                //                        '<img class="card-img-top" src="' + imagemModal + '" alt="Card image cap">' +
                //                        ' <div class="card-body">' +
                //                        ' <h5 class="card-title text-center">' + codValor.finalidade + '</h5>' +
                //                        ' <ul class="list-group">' +
                //                        '  <li class="list-group-item d-flex justify-content-between align-items-center">' +
                //                        ' <h5 class="card-title text-center">' + codValor.codigo + '</h5>' +
                //                        '</li>' +
                //                        '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                //                        '<img class="mr-1" style="width: 25px" src="' + base_url + 'assets/images/icons/quartos.svg" alt="Generic placeholder image">' +
                //                        '<span class="badge badge-primary badge-light">Quartos</span>' +
                //                        ' <span class="badge badge-primary badge-success">' + codValor.numeroquartos + '</span>' +
                //                        '</li>' +
                //                        '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                //                        '   <img class="mr-1" style="width: 25px" src="' + base_url + 'assets/images/icons/car-solid.svg" alt="Generic placeholder image">' +
                //                        ' <span class="badge badge-primary badge-light">Vagas</span>' +
                //                        ' <span class="badge badge-primary badge-success">' + codValor.numerovagas + '</span>' +
                //                        ' </li>' +
                //                        '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                //                        ' <img class="mr-1" style="width: 25px" src="' + base_url + 'assets/images/icons/banheiro1.png" alt="Generic placeholder image">' +
                //                        ' <span class="badge badge-primary badge-light">Banheiros</span>' +
                //                        ' <span class="badge badge-primary badge-success">' + codValor.numerobanhos + '</span>' +
                //                        '</li> ' +
                //                        ' <li class="list-group-item d-flex justify-content-between align-items-center">' +
                //                        ' <img class="mr-1" style="width: 25px" src="' + base_url + 'assets/images/icons/preco.svg" alt="Generic placeholder image">' +
                //                        '<span class="badge badge-primary badge-light">Preço</span>' +
                //                        '<span class="badge badge-primary badge-success">' + codValor.valor + '</span>' +
                //                        ' </li>' +
                //                        '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                //                        '<button type="button" class="btn btn-outline-primary btn-lg btn-block">Ver detalhes</button>' +
                //                        ' </li>' +
                //                        '</ul>' +
                //                        '</div>' +
                //                        '</div>' +
                //                        '</div>';


                let linha =
                    '<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-bottom: 10px" style="box-shadow:10px 10px 10px  0px #8888884f;">' +
                    '<a  href="' + base_url + 'imoveis/' + codValor.titulo + '/' +  codValor.codigo + '">' +
                    '<div class="card" style=" box-shadow:10px 10px 10px  0px #8888884f;">' +
                    //                                    ' <img class="card-img-top" src="' + imagem + '" alt="Card image cap">' +

                    //                                    '<img class="card-img-top" src="'+ base_url + 'assets/images/banner/3.jpg" alt="Card image cap">'+
                    '<img class="card-img-top" src="' + imagemModal + '" alt="Card image cap">' +
                    '<div class="valor-card-destaque">' +
                    '<h5 class="valor-texto">' + codValor.valor + '</h5>' +
                    '</div>' +
                    ' <div class="card-body">' +
                    ' <h5 class="card-title text-center" style="color:black">' + codValor.finalidade + '</h5>' +
                    ' <ul class="list-group list-group-flush corpoListDetalhe">' +
                    '  <li class="list-group-item d-flex justify-content-between align-items-center">' +
                    ' <span class="badge badge-primary badge-light"> </span>' +
                    '<span class="badge badge-primary badge-light">' + codValor.bairro + ' | ' + codValor.cidade + '</span>' +
                    '  <span class="badge badge-primary badge-light"> </span>' +
                    '</li>' +
                    '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                    ' <div class="d-flex justify-content-between align-items-center">' +
                    '  <img class="mr-1 iconeListaCardListagem"  src="' + base_url + 'assets/images/icons/quartos.svg" alt="Generic placeholder image">' +
                    ' <div class="col txt-card-itens">' +
                    '  <span class="badge badge-primary badge-light">' + codValor.numeroquartos + '</span>' +
                    '  <span class="badge badge-primary badge-light text-cards">Quartos</span> ' +
                    '</div>' +
                    ' </div>' +
                    '  <div class="d-flex justify-content-between align-items-center">' +
                    '  <img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/banheiro1.png" alt="Generic placeholder image">' +
                    '  <div class="col txt-card-itens">' +
                    '    <span class="badge badge-primary badge-light">' + codValor.numerobanhos + '</span>  ' +
                    '    <span class="badge badge-primary badge-light text-cards">Banheiros</span>' +
                    ' </div>' +
                    ' </div>' +
                    ' <div class="d-flex justify-content-between align-items-center">' +
                    '  <img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/car-solid.svg" alt="Generic placeholder image">' +
                    ' <div class="col txt-card-itens">' +
                    '<span class="badge badge-primary badge-light">' + codValor.numerovagas + '</span>' +
                    '     <span class="badge badge-primary badge-light text-cards">Vagas</span>' +
                    '  </div>' +
                    ' </div>' +
                    '  </li>' +
                    //                                    '  <li class="list-group-item d-flex justify-content-between align-items-center">' +
                    //                                    '<img class="mr-1 iconeListaCardListagem"  src="' + base_url + 'assets/images/icons/preco.svg" alt="Generic placeholder image">' +
                    //                                    '<span class="badge badge-primary badge-light" style="font-size:20px">R$:' + valor.valor + '</span>' +
                    //                                    '   <span class="badge badge-primary badge-light"></span>' +
                    //                                    '</li>' +
                    '</ul>' +
                    '</div>' +
                    '</a>' +
                    ' </div>';

                $('.conteudoModal').append(linha);

            });


            //MENSAGEM DE DE RETORNTO DA BUSCA POR CÓDIGO
            if (codigo.quantidade == 0) {
                $('.tituloModal').text('Não há imoveis com esse código');
            } else {
                $('.tituloModal').text('Imoveis Encontrados');
            }

        }
    });


});


// carregarCidade();
// carregarTipo();



$("#filtrar").click(function() {

    $('#corpo-filtro').toggle("slow");
    $('#filtrarForm').toggle("fast");
});