var parametros = {
    'finalidade': 2,
    'numeroPagina': 1,
    'codigocidade': 0,
    'codigosbairros': '',
    'codigoTipo': ''
};



$('#venda-mapa').click(function() {

    parametros.finalidade = 2;
    initMap();
});


$('#aluguel-mapa').click(function() {
    parametros.finalidade = 1;

    initMap();
});


function initMap() {

    //  var myLatLng = {lat: -19.869429, lng: -44.986213};
    var myLatLng = { lat: -19.919743, lng: -43.938758 };

    var mapOptions = {
        zoom: 12,
        center: myLatLng

    };

    var map = new google.maps.Map(document.getElementById("map"), mapOptions);


    $.ajax({
        method: 'POST',
        url: base_url + '/imoveisMapa/',
        async: true,
        data: { paramentros_mapa: parametros },
        beforeSend: function() {

        }
    }).fail(function(dados) {

    }).done(function(dados) {

        dados = JSON.parse(dados);


        var markers = dados.lista.map(function(dados, i) {

            var mk = new google.maps.Marker({
                position: { 'lat': parseFloat(dados.latitude), 'lng': parseFloat(dados.longitude) },
                title: "Lívia Machado",
                icon: base_url + "/assets/images/icons/markeer.png"
                    //                label: 
            });


            mk.addListener('click', function() {

                var codigo = {};

                codigo.finalidade = parametros.finalidade;
                codigo.codigo = dados.codigo;

                debugger;


                $.ajax({
                    method: "POST",
                    url: base_url + "imoveis-codigos-home",
                    data: { codigo: codigo },
                    async: true,
                    //timeout:30000000,
                    // complete: function(retorno){
                    // },
                    success: function(retorno) {

                        debugger;


                        //retorno = JSON.parse(retorno);
                        retorno = retorno.lista;


                        console.log(retorno);

                        debugger;


                        var contentString = '<a style="color:black" href="' + base_url + 'imoveis/' + retorno[0].titulo +'/'+ retorno[0].codigo + '">' +
                            '<div class="card" style="width: 15rem;">' +
                            ' <img class="card-img-top" src="' + retorno[0].urlfotoprincipal + '" alt="Card image cap">' +
                            ' <div class="card-body">' +
                            '<h6 class="text-center" class="card-title">' + retorno[0].tipo + ' | ' + retorno[0].finalidade + ' </h6>' +
                            '<ul  class="list-group list-group-flush">' +
                            '<li class="text-center" style=" list-style-type:none"class="list-group-item">' + retorno[0].valor + '</li>' +
                            '</div>' +
                            '</div>' +
                            '</a>';

                        // var contentString = '<a style="color:black" href="' + base_url + 'detalhe-imovel/' + retorno[0].codigo + '">' +
                        //     '<div class="card" style="width: 15rem;">' +
                        //     ' <img class="card-img-top" src="'+base_url+'assets/img/logo/logo.png" alt="Card image cap">' +
                        //     ' <div class="card-body">' +
                        //     '<h6 class="text-center" class="card-title">Encontrou seu imóvel, nós levamos a chave para você.</h6>' +
                        //     '<ul  class="list-group list-group-flush">' +
                        //     '<li class="text-center" style=" list-style-type:none"class="list-group-item">' + retorno[0].valor + '</li>' +
                        //     '</div>' +
                        //     '</div>' +
                        //     '</a>';


                        var infowindow = new google.maps.InfoWindow({
                            content: contentString

                        });

                        infowindow.open(map, mk);

                    }
                });

                //                   markerCluster.addMarker(marker);

            });

            return mk;


        });

        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, markers, { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });


    }).always(function(data) {

    });

}

initMap();