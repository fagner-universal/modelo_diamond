function carregarDestaques() {

    $.ajax({
        method: "POST",
        url: base_url + 'get-destaques', //ImovelController
        async: true,
        data: {},
        beforeSend: function () {



        }
    }).done(function (dados) {

        $('#lancamentos').empty();
        $('#destaqueVenda').empty();
        $('#destaqueAluguel').empty();


        $.each(dados.lancamento.lista, function (x, imo) {

            let lancamento = '<a href="' + base_url + 'imoveis/' + imo.codigo + '">' +
                '<div class="testimonial-item corpo-card">' +
                '<div class="member">' +
                '<div class="pic">' +
                '<img class="img-card" src="' + imo.urlfotoprincipalp + '" alt=""></div>' +
                '<div class="valor-card-destaque">' +
                '<h5 class="valor-texto">' + imo.tipo + '</h5>' +
                '</div>' +
                '<div class="details">' +
                '<div class="social">' +
                '<ul class="list-group list-group-flush corpoListDetalhe">' +
                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                '<span class="badge badge-primary badge-light"> </span>' +
                '<span class="badge badge-primary badge-light">' + imo.tipo + '</span>' +
                '<span class="badge badge-primary badge-light"> </span>' +
                '</li>' +
                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                '<div class="icon">' +
                '<img class="mr-1 iconeListaCardListagem" src="assets/images/icons/mapa.svg" alt="Generic placeholder image">' +
                '</div>' +
                '<span class="badge badge-primary badge-light">' + imo.bairro + ' | ' + imo.cidade + '</span>' +
                '<span class="badge badge-primary badge-light">' + imo.estado + '</span>' +
                '</li>' +
                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                '<div class="d-flex justify-content-between align-items-center">' +
                '<img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/quartos.svg" alt="Generic placeholder image">' +
                '<div class="col txt-card-itens">' +
                '<span class="badge badge-primary badge-light">' + imo.numeroquartos + '</span>' +
                '<span class="badge badge-primary badge-light text-cards">Quartos</span>' +
                '</div>' +
                '</div>' +
                '<div class="d-flex justify-content-between align-items-center">' +
                '<img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/banheiro1.png" alt="Generic placeholder image">' +
                '<div class="col txt-card-itens">' +
                '<span class="badge badge-primary badge-light">' + imo.numerobanhos + '</span>' +
                '<span class="badge badge-primary badge-light text-cards">Banheiros</span>' +
                '</div>' +
                '</div>' +
                '<div class="d-flex justify-content-between align-items-center">' +
                '<img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/car-solid.svg" alt="Generic placeholder image">' +
                '<div class="col txt-card-itens">' +
                '<span class="badge badge-primary badge-light">' + imo.numerovagas + '</span>' +
                '<span class="badge badge-primary badge-light text-cards">Vagas</span>' +
                '</div>' +
                '</div>' +
                '</li>' +
                '</ul>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</a>';


            $('#lancamentos').append(lancamento);

        });





        $.each(dados.destaqueVenda, function (x, imo) {

            let dvenda =
                '<a href="' + base_url + 'imoveis/' + imo.codigo + '">' +
                '<div class="testimonial-item corpo-card">' +
                '<div class="member">' +
                '<div class="pic"><img class="img-card" src="' + imo.urlfotoprincipalp + '" alt=""></div>' +
                '<div class="valor-card-destaque">' +
                '<h5 class="valor-texto">' + imo.tipo + '</h5>' +
                '</div>' +
                '<div class="details">' +
                '<div class="social">' +
                '<ul class="list-group list-group-flush corpoListDetalhe">' +
                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                '<span class="badge badge-primary badge-light"> </span>' +
                '<span class="badge badge-primary badge-light">' + imo.tipo + '</span>' +
                '<span class="badge badge-primary badge-light"> </span>' +
                '</li>' +
                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                '<div class="icon">' +
                '<img class="mr-1 iconeListaCardListagem" src="assets/images/icons/mapa.svg" alt="Generic placeholder image">' +
                '</div>' +
                '<span class="badge badge-primary badge-light">' + imo.cidade + ' | ' + imo.bairro + '</span>' +
                '<span class="badge badge-primary badge-light">' + imo.estado + '</span>' +
                '</li>' +
                '<li class="list-group-item d-flex  justify-content-between align-items-center">' +
                '<span class="badge badge-primary badge-light"></span>' +
                '<span class="badge badge-primary badge-light">' + imo.bairro + '| ' + imo.cidade + ' </span>' +
                '<span class="badge badge-primary badge-light"></span>' +
                '</li>' +
                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                '<div class="d-flex justify-content-between align-items-center">' +
                '<img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/quartos.svg" alt="Generic placeholder image">' +
                '<div class="col txt-card-itens">' +
                '<span class="badge badge-primary badge-light">' + imo.numeroquartos + '</span>' +
                '<span class="badge badge-primary badge-light text-cards">Quartos</span>' +
                '</div>' +
                '</div>' +
                '<div class="d-flex justify-content-between align-items-center">' +
                '<img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/banheiro1.png" alt="Generic placeholder image">' +
                '<div class="col txt-card-itens">' +
                '<span class="badge badge-primary badge-light">' + imo.numerobanhos + '</span>' +
                '<span class="badge badge-primary badge-light text-cards">Banheiros</span>' +
                '</div>' +
                '</div>' +
                '<div class="d-flex justify-content-between align-items-center">' +
                '<img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/car-solid.svg" alt="Generic placeholder image">' +
                '<div class="col txt-card-itens">' +
                '<span class="badge badge-primary badge-light">' + imo.numerovagas + '</span>' +
                '<span class="badge badge-primary badge-light text-cards">Vagas</span>' +
                '</div>' +
                '</div>' +
                '</li>' +
                '</ul>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</a>';


            $('#destaqueVenda').append(dvenda);

        });



        $.each(dados.destaqueAluguel, function (x, imo) {

            let daluguel =
                '<a href="' + base_url + 'imoveis/' + imo.codigo + '">' +
                '<div class="testimonial-item corpo-card">' +
                '<div class="member">' +
                '<div class="pic"><img class="img-card" src="' + imo.urlfotoprincipalp + '" alt=""></div>' +
                '<div class="valor-card-destaque">' +
                '<h5 class="valor-texto">' + imo.tipo + '</h5>' +
                '</div>' +
                '<div class="details">' +
                '<div class="social">' +
                '<ul class="list-group list-group-flush corpoListDetalhe">' +
                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                '<span class="badge badge-primary badge-light"> </span>' +
                '<span class="badge badge-primary badge-light">' + imo.tipo + '</span>' +
                '<span class="badge badge-primary badge-light"> </span>' +
                '</li>' +
                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                '<div class="icon">' +
                '<img class="mr-1 iconeListaCardListagem" src="assets/images/icons/mapa.svg" alt="Generic placeholder image">' +
                '</div>' +
                '<span class="badge badge-primary badge-light">' + imo.cidade + ' | ' + imo.bairro + '</span>' +
                '<span class="badge badge-primary badge-light">' + imo.estado + '</span>' +
                '</li>' +
                '<li class="list-group-item d-flex  justify-content-between align-items-center">' +
                '<span class="badge badge-primary badge-light"></span>' +
                '<span class="badge badge-primary badge-light">' + imo.bairro + '| ' + imo.cidade + ' </span>' +
                '<span class="badge badge-primary badge-light"></span>' +
                '</li>' +
                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                '<div class="d-flex justify-content-between align-items-center">' +
                '<img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/quartos.svg" alt="Generic placeholder image">' +
                '<div class="col txt-card-itens">' +
                '<span class="badge badge-primary badge-light">' + imo.numeroquartos + '</span>' +
                '<span class="badge badge-primary badge-light text-cards">Quartos</span>' +
                '</div>' +
                '</div>' +
                '<div class="d-flex justify-content-between align-items-center">' +
                '<img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/banheiro1.png" alt="Generic placeholder image">' +
                '<div class="col txt-card-itens">' +
                '<span class="badge badge-primary badge-light">' + imo.numerobanhos + '</span>' +
                '<span class="badge badge-primary badge-light text-cards">Banheiros</span>' +
                '</div>' +
                '</div>' +
                '<div class="d-flex justify-content-between align-items-center">' +
                '<img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/car-solid.svg" alt="Generic placeholder image">' +
                '<div class="col txt-card-itens">' +
                '<span class="badge badge-primary badge-light">' + imo.numerovagas + '</span>' +
                '<span class="badge badge-primary badge-light text-cards">Vagas</span>' +
                '</div>' +
                '</div>' +
                '</li>' +
                '</ul>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</a>';


            $('#destaqueAluguel').append(daluguel);

        });


    }).then(function () {

        $('#destaqueVenda').slick({
            dots: false,
            arrows: false,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            autoplay: true,
            centerMode: true,
            centerPadding: '50px',
            responsive: [{
                breakpoint: 1435,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }
            },
            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }
            },
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }

            }, 
            {
                breakpoint: 375,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }

            }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });


        $('#destaqueAluguel').slick({
            dots: false,
            arrows: false,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            autoplay: true,
            centerMode: true,
            centerPadding: '50px',
            responsive: [{
                breakpoint: 1435,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }
            },
            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }
            },
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }

            }, 
            {
                breakpoint: 375,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }

            }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });


    }).always(function () {

    });

}


function getDestaqueAluguel() {

    $.ajax({
        method: "POST",
        url: base_url + 'get-destaques-aluguel', //ImovelController
        async: true,
        data: {},
        beforeSend: function () { }

    }).done(function (dados) {


        $('#destaqueAluguel').empty();

        $.each(dados.lista, function (x, imo) {

            let daluguel = '<a href="' + base_url + 'imoveis/' + imo.titulo + '/' + imo.codigo + '">' +
                '<div class="testimonial-item corpo-card">' +
                '<div class="member">' +
                '<div class="pic">' +
                '<img class="img-card" src="' + imo.urlfotoprincipalp + '" alt=""></div>' +
                '<div class="valor-card-destaque">' +
                '<h5 class="valor-texto">' + imo.tipo + '</h5>' +
                '</div>' +
                '<div class="details">' +
                '<div class="social">' +
                '<ul class="list-group list-group-flush corpoListDetalhe">' +
                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                '<span class="badge badge-primary badge-light"> </span>' +
                '<span class="badge badge-primary badge-light">' + imo.tipo + '</span>' +
                '<span class="badge badge-primary badge-light"> </span>' +
                '</li>' +
                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                '<div class="icon">' +
                '<img class="mr-1 iconeListaCardListagem" src="assets/images/icons/mapa.svg" alt="Generic placeholder image">' +
                '</div>' +
                '<span class="badge badge-primary badge-light">' + imo.bairro + ' | ' + imo.cidade + '</span>' +
                '<span class="badge badge-primary badge-light">' + imo.estado + '</span>' +
                '</li>' +
                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                '<div class="d-flex justify-content-between align-items-center">' +
                '<img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/quartos.svg" alt="Generic placeholder image">' +
                '<div class="col txt-card-itens">' +
                '<span class="badge badge-primary badge-light">' + imo.numeroquartos + '</span>' +
                '<span class="badge badge-primary badge-light text-cards">Quartos</span>' +
                '</div>' +
                '</div>' +
                '<div class="d-flex justify-content-between align-items-center">' +
                '<img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/banheiro1.png" alt="Generic placeholder image">' +
                '<div class="col txt-card-itens">' +
                '<span class="badge badge-primary badge-light">' + imo.numerobanhos + '</span>' +
                '<span class="badge badge-primary badge-light text-cards">Banheiros</span>' +
                '</div>' +
                '</div>' +
                '<div class="d-flex justify-content-between align-items-center">' +
                '<img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/car-solid.svg" alt="Generic placeholder image">' +
                '<div class="col txt-card-itens">' +
                '<span class="badge badge-primary badge-light">' + imo.numerovagas + '</span>' +
                '<span class="badge badge-primary badge-light text-cards">Vagas</span>' +
                '</div>' +
                '</div>' +
                '</li>' +
                '</ul>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</a>';


            $('#destaqueAluguel').append(daluguel);

        });


    }).then(function () {



        $('#destaqueAluguel').slick({
            dots: false,
            arrows: false,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            autoplay: true,
            centerMode: true,
            centerPadding: '50px',
            responsive: [{
                breakpoint: 1435,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }
            },
            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }
            },
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }

            }, 
            {
                breakpoint: 375,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }

            }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });

        //    var desta = $('#carrossel-destaques');
        //
        //    $('.arrow-left').click(function () {
        //        desta.slick('slickPrev');
        //    });
        //
        //    $('.arrow-right').click(function () {
        //        desta.slick('slickNext');
        //    });
        //
        //    desta.on('edge', function (event, slick, direction) {
        //        if (direction == 'left') {
        //
        //        }
        //    });

    });

}

function getDestaqueVendas() {

    $.ajax({
        method: "POST",
        url: base_url + 'get-destaques-vendas', //ImovelController
        async: true,
        data: {},
        beforeSend: function () {

        }
    }).done(function (dados) {

        $('#destaqueVenda').empty();


        $.each(dados.lista, function (x, imo) {

            let dvenda = '<a href="' + base_url + 'imoveis/' + imo.titulo + '/' + imo.codigo + '">' +
                '<div class="testimonial-item corpo-card">' +
                '<div class="member">' +
                '<div class="pic">' +
                '<img class="img-card" src="' + imo.urlfotoprincipalp + '" alt=""></div>' +
                '<div class="valor-card-destaque">' +
                '<h5 class="valor-texto">' + imo.tipo + '</h5>' +
                '</div>' +
                '<div class="details">' +
                '<div class="social">' +
                '<ul class="list-group list-group-flush corpoListDetalhe">' +
                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                '<span class="badge badge-primary badge-light"> </span>' +
                '<span class="badge badge-primary badge-light">' + imo.tipo + '</span>' +
                '<span class="badge badge-primary badge-light"> </span>' +
                '</li>' +
                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                '<div class="icon">' +
                '<img class="mr-1 iconeListaCardListagem" src="assets/images/icons/mapa.svg" alt="Generic placeholder image">' +
                '</div>' +
                '<span class="badge badge-primary badge-light">' + imo.bairro + ' | ' + imo.cidade + '</span>' +
                '<span class="badge badge-primary badge-light">' + imo.estado + '</span>' +
                '</li>' +
                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                '<div class="d-flex justify-content-between align-items-center">' +
                '<img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/quartos.svg" alt="Generic placeholder image">' +
                '<div class="col txt-card-itens">' +
                '<span class="badge badge-primary badge-light">' + imo.numeroquartos + '</span>' +
                '<span class="badge badge-primary badge-light text-cards">Quartos</span>' +
                '</div>' +
                '</div>' +
                '<div class="d-flex justify-content-between align-items-center">' +
                '<img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/banheiro1.png" alt="Generic placeholder image">' +
                '<div class="col txt-card-itens">' +
                '<span class="badge badge-primary badge-light">' + imo.numerobanhos + '</span>' +
                '<span class="badge badge-primary badge-light text-cards">Banheiros</span>' +
                '</div>' +
                '</div>' +
                '<div class="d-flex justify-content-between align-items-center">' +
                '<img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/car-solid.svg" alt="Generic placeholder image">' +
                '<div class="col txt-card-itens">' +
                '<span class="badge badge-primary badge-light">' + imo.numerovagas + '</span>' +
                '<span class="badge badge-primary badge-light text-cards">Vagas</span>' +
                '</div>' +
                '</div>' +
                '</li>' +
                '</ul>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</a>';


            $('#destaqueVenda').append(dvenda);

        });

    }).then(function () {


        $('#destaqueVenda').slick({
            dots: false,
            arrows: false,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            autoplay: true,
            centerMode: true,
            centerPadding: '50px',
            responsive: [{
                breakpoint: 1435,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }
            },
            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }
            },
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }

            }, 
            {
                breakpoint: 375,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }

            }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });

        //    var desta = $('#carrossel-destaques');
        //
        //    $('.arrow-left').click(function () {
        //        desta.slick('slickPrev');
        //    });
        //
        //    $('.arrow-right').click(function () {
        //        desta.slick('slickNext');
        //    });
        //
        //    desta.on('edge', function (event, slick, direction) {
        //        if (direction == 'left') {
        //
        //        }
        //    });
        // getDestaqueAluguel();
    });

}


function getlancamentos() {

    $.ajax({
        method: "POST",
        url: base_url + 'get-lancamentos', //ImovelController
        async: true,
        data: {},
        beforeSend: function () {

        }
    }).done(function (dados) {

        $('#lancamentos').empty();


        $.each(dados.lista, function (x, imo) {

            let lancamento = '<a href="' + base_url + 'imoveis/' + imo.codigo + '">' +
                '<div class="testimonial-item corpo-card">' +
                '<div class="member">' +
                '<div class="pic">' +
                '<img class="img-card" src="' + imo.urlfotoprincipalp + '" alt=""></div>' +
                '<div class="valor-card-destaque">' +
                '<h5 class="valor-texto">' + imo.tipo + '</h5>' +
                '</div>' +
                '<div class="details">' +
                '<div class="social">' +
                '<ul class="list-group list-group-flush corpoListDetalhe">' +
                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                '<span class="badge badge-primary badge-light"> </span>' +
                '<span class="badge badge-primary badge-light">' + imo.tipo + '</span>' +
                '<span class="badge badge-primary badge-light"> </span>' +
                '</li>' +
                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                '<div class="icon">' +
                '<img class="mr-1 iconeListaCardListagem" src="assets/images/icons/mapa.svg" alt="Generic placeholder image">' +
                '</div>' +
                '<span class="badge badge-primary badge-light">' + imo.cidade + ' | ' + imo.bairro + '</span>' +
                '<span class="badge badge-primary badge-light">' + imo.estado + '</span>' +
                '</li>' +
                '<li class="list-group-item d-flex  justify-content-between align-items-center">' +
                '<span class="badge badge-primary badge-light"></span>' +
                '<span class="badge badge-primary badge-light">' + imo.bairro + '| ' + imo.cidade + ' </span>' +
                '<span class="badge badge-primary badge-light"></span>' +
                '</li>' +
                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                '<div class="d-flex justify-content-between align-items-center">' +
                '<img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/quartos.svg" alt="Generic placeholder image">' +
                '<div class="col txt-card-itens">' +
                '<span class="badge badge-primary badge-light">' + imo.numeroquartos + '</span>' +
                '<span class="badge badge-primary badge-light text-cards">Quartos</span>' +
                '</div>' +
                '</div>' +
                '<div class="d-flex justify-content-between align-items-center">' +
                '<img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/banheiro1.png" alt="Generic placeholder image">' +
                '<div class="col txt-card-itens">' +
                '<span class="badge badge-primary badge-light">' + imo.numerobanhos + '</span>' +
                '<span class="badge badge-primary badge-light text-cards">Banheiros</span>' +
                '</div>' +
                '</div>' +
                '<div class="d-flex justify-content-between align-items-center">' +
                '<img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/car-solid.svg" alt="Generic placeholder image">' +
                '<div class="col txt-card-itens">' +
                '<span class="badge badge-primary badge-light">' + imo.numerovagas + '</span>' +
                '<span class="badge badge-primary badge-light text-cards">Vagas</span>' +
                '</div>' +
                '</div>' +
                '</li>' +
                '</ul>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</a>';


            $('#lancamentos').append(lancamento);


        });

    }).then(function () {

        //SLICK CARROSSEL DE DESTAQUES HOME
        $('#lancamentos').slick({
            dots: false,
            arrows: false,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            autoplay: true,
            centerMode: true,
            centerPadding: '50px',
            responsive: [{
                breakpoint: 1435,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }
            },
            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }
            },
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }

            }, 
            {
                breakpoint: 375,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    centerMode: true,
                    centerPadding: '20px',
                }

            }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });

        //    var desta = $('#carrossel-destaques');
        //
        //    $('.arrow-left').click(function () {
        //        desta.slick('slickPrev');
        //    });
        //
        //    $('.arrow-right').click(function () {
        //        desta.slick('slickNext');
        //    });
        //
        //    desta.on('edge', function (event, slick, direction) {
        //        if (direction == 'left') {
        //
        //        }
        //    });

    });

}

getlancamentos();
getDestaqueVendas();
getDestaqueAluguel();