
var geocoder;




function initialize() {
    geocoder = new google.maps.Geocoder();
    // var latlng = new google.maps.LatLng(-34.397, 150.644);
    // var mapOptions = {
    //   zoom: 8,
    //   center: latlng
    // }
    // map = new google.maps.Map(document.getElementById('map'), mapOptions);
}


function preencherFormulario(chave, endereco) {
    //.formatted_address
//    console.log(endereco.formatted_address);
//    console.log(endereco);

    $("#address").val(endereco[0].short_name);
    $(".lst-endereco").hide();
    
    atualizaCidade(endereco[0].short_name);


}


function novaLinhaDeBusca(chave, enderecoLinha) {

    var linha = $("<li>").addClass("list-group-item").attr("id", chave).text(enderecoLinha.formatted_address);

    linha.on('click', function () {

        preencherFormulario(chave, enderecoLinha.address_components);
    });

    $(".lst-endereco").append(linha);
}


function codeAddress() {

    initialize();

    var address = document.getElementById('address').value;
    geocoder.geocode({'address': address}, function (results, status) {

        let cordenadas = [{'endereco': '', 'lat': '', 'lng': ''}];


        if (status == 'OK') {

            $(".lst-endereco").empty();

            $.each(results, function (index, value) {
                novaLinhaDeBusca(index, value);
            });

        } else {
            //console.log('Geocode was not successful for the following reason: ' + status);
        }
    });

}

// codeAddress("Rua Raimunda de Freitas, 62 ,Nascimento, IBirite");

$('#address').keyup(function () {
   $(".lst-endereco").show();
    codeAddress();

});