
//var todosImiveis = [];
var arrayParaOmapa = [];
var markerCluster = [];

var imoveisGlobal = [];

var parametros = {
    'finalidade': 1,
    'numeroPagina': 1,
    'codigocidade': 0,
    'codigosbairros': '',
    'codigoTipo': ''
};


var testemapa = [];


function recarregar(retorno) {

    todosRegistros = jQuery.parseJSON(retorno);

    $.each(todosRegistros.lista, function (y, z) {

        var imagem = '';
        if (z.urlfotoprincipal === "") {
            imagem = 'https://www.imoview.com.br/demo/Front/img/house1.png';
        } else {
            imagem = z.urlfotoprincipal;

        }

        var latTeste = parseFloat(z.latitude);
        var lotTeste = parseFloat(z.longitude);


        arrayParaOmapa.push({
            'endereco': z.endereco, "lat": latTeste, "lng": lotTeste, 'foto': imagem, "finalidade": z.finalidade, "tipo": z.tipo,
            "cidade": z.cidade, "numeroquartos": z.numeroquartos, "numerovagas": z.numerovagas, "numerobanhos": z.numerobanhos, "tipo": z.tipo, "valor": z.valor,
            "codigo": z.codigo, "estado": z.estado, "bairro": z.bairro
        });


        imoveisGlobal = arrayParaOmapa;
        


        var marker = new google.maps.Marker({
            position: {lat: parseFloat(z.latitude), lng: parseFloat(z.longitude)},
            map: map,
            //   content: contentString,
            idImovel: z.codigo,
//                    label: '',
            title: "Lívia Machado",
            icon: base_url+"/assets/images/icons/mark-baixa.png"
        });



        marker.addListener('click', function () {
            var codigo = {};

            codigo.finalidade = parametros.finalidade;
            codigo.codigo = marker.idImovel;
            $.ajax({
                method: "POST",
                url: base_url + "imoveis-codigos-home",
                data: { codigo: codigo },
                async: true,
                //timeout:30000000,
                // complete: function(retorno){
                // },
                success: function (retorno) {

                    //retorno = JSON.parse(retorno);
                    retorno = retorno.lista;


                    var imagem = '';

                    if (retorno[0].urlfotoprincipal === "") {
                        imagem = 'https://www.imoview.com.br/demo/Front/img/house1.png';
                    } else {
                        imagem = retorno[0].urlfotoprincipal;

                    }

                    var contentString = '<a style="color:black" href="' + base_url + 'imoveis/' + retorno[0].titulo + '/' + retorno[0].codigo + '">' +
                        '<div class="card" style="width: 100%;">' +
                        ' <img class="card-img-top" style="width: 100%;height: 150px" src="' + imagem + '" alt="Card image cap">' +
                        ' <div class="card-body">' +
                        '<h6 class="text-center" class="card-title">' + retorno[0].tipo + ' | ' + retorno[0].finalidade + ' </h6>' +
                        '<ul  class="list-group list-group-flush">' +
                        '<li class="text-center" style=" list-style-type:none"class="list-group-item">' + retorno[0].valor + '</li>' +
                        '</div>' +
                        '</div>' +
                        '</a>';


                    var infowindow = new google.maps.InfoWindow({
                        content: contentString,
                        maxWidth: 400

                    });

                    infowindow.open(map, marker);

                }
            });

        });


        markerCluster.addMarker(marker);

    });

}



function initMap() {
 // var myLatLng = {lat: -19.869429, lng: -44.986213};
    var myLatLng = { lat: -19.919743, lng: -43.938758 };



    var mapOptions = {
        zoom: 12,
        center: myLatLng
    };

    var map = new google.maps.Map(document.getElementById("map"), mapOptions);

//    map.addListener('idle', function () {
//            alert('estesdsd');
//    });

    setMarkers(map);
}


function setMarkers(map) {

    var markers = arrayParaOmapa.map(function (location, i) {});



    map.addListener('idle', function () {

        $('.lstImoveis').empty();

        var bounds = map.getBounds();

        var southWest = bounds.getSouthWest();
        var northEast = bounds.getNorthEast();

        var sudoeste_latitude = southWest.lat();
        var sudoeste_longitude = southWest.lng();
        var nordeste_latitude = northEast.lat();
        var nordeste_longitude = northEast.lng();


        var novoArray1 = arrayParaOmapa;
        novoArray = '';


        //PERCORRRE TODOS OS LOCAIS E 
        for (var i = arrayParaOmapa.length - 1; i >= 0; i--) {

            //SE ESTIVER FORA DAS DIMENÇÕES DA ESQUERDA E DE BAIXO ENTRA NO IF
            if (novoArray1[i].lat > sudoeste_latitude) {

                if (novoArray1[i].lng > sudoeste_longitude) {

                    if (novoArray1[i].lat < nordeste_latitude) {

                        if (novoArray1[i].lng < nordeste_longitude) {

                            var imagem = '';
                            if (novoArray1[i].foto === "") {
                                imagem = base_url + '/assets/images/banner/3.jpg';
                            } else {
                                imagem = novoArray1[i].foto;

                            }


                            novoArray += novoArray1[i].codigo + ',';


//                            let linha = '<div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">' +
//                                    '<div class="card" style="width: 18rem;">' +
//                                    ' <img class="card-img-top" src="' + imagem + '" alt="Card image cap">' +
//                                    ' <div class="card-body">' +
//                                    '<h6 class="card-title">Cógio: ' + novoArray1[i].codigo + ' | ' + novoArray1[i].tipo + ' </h6>' +
//                                    '<ul class="list-group list-group-flush">' +
//                                    '<li class="list-group-item">' + novoArray1[i].cidade + ' | ' + novoArray1[i].estado + '</li>' +
//                                    '<li class="list-group-item">' + novoArray1[i].endereco + '</li>' +
//                                    '<li class="list-group-item">' + novoArray1[i].valor + '</li>' +
//                                    '<li class="list-group-item"><a href="' + base_url + 'imoveis/' + novoArray1[i].codigo + '""><button type="button" class="btn btn-outline-info btn-block">Detalhes</button> </li>' +
//                                    ' </ul>' +
//                                    '</div>' +
//                                    '</div>';



//                            let linha =
//                                    '<div class="col-sm-6 col-md-6 col-lg-3 col-xl-3" style="margin-bottom: 10px" style="box-shadow:10px 10px 10px  0px #8888884f;">' +
//                                    '<a  href="' + base_url + 'imoveis/' + novoArray1[i].codigo + '">' +
//                                    '<div class="card" style=" box-shadow:10px 10px 10px  0px #8888884f;">' +
//                                    '<img class="card-img-top" src="' + imagem + '" alt="Card image cap">' +
//                                    '<div class="valor-card-destaque">' +
//                                    '<h5 class="valor-texto">' + novoArray1[i].valor + '</h5>' +
//                                    '</div>' +
//                                    ' <div class="card-body">' +
//                                    ' <h5 class="card-title text-center" style="color:black">' + novoArray1[i].finalidade + '</h5>' +
//                                    ' <ul class="list-group list-group-flush corpoListDetalhe">' +
//                                    '  <li class="list-group-item d-flex justify-content-between align-items-center">' +
//                                    ' <span class="badge badge-primary badge-light"> </span>' +
//                                    '<span class="badge badge-primary badge-light">' + novoArray1[i].bairro + ' | ' + novoArray1[i].cidade + '</span>' +
//                                    '  <span class="badge badge-primary badge-light"> </span>' +
//                                    '</li>' +
//                                    '<li class="list-group-item d-flex justify-content-between align-items-center">' +
//                                    ' <div class="d-flex justify-content-between align-items-center">' +
//                                    '  <img class="mr-1 iconeListaCardListagem"  src="' + base_url + 'assets/images/icons/quartos.svg" alt="Generic placeholder image">' +
//                                    ' <div class="col txt-card-itens">' +
//                                    '  <span class="badge badge-primary badge-light">' + novoArray1[i].numeroquartos + '</span>' +
//                                    '  <span class="badge badge-primary badge-light text-cards">Quartos</span> ' +
//                                    '</div>' +
//                                    ' </div>' +
//                                    '  <div class="d-flex justify-content-between align-items-center">' +
//                                    '  <img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/banheiro1.png" alt="Generic placeholder image">' +
//                                    '  <div class="col txt-card-itens">' +
//                                    '    <span class="badge badge-primary badge-light">' + novoArray1[i].numerobanhos + '</span>  ' +
//                                    '    <span class="badge badge-primary badge-light text-cards">Banheiros</span>' +
//                                    ' </div>' +
//                                    ' </div>' +
//                                    ' <div class="d-flex justify-content-between align-items-center">' +
//                                    '  <img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/car-solid.svg" alt="Generic placeholder image">' +
//                                    ' <div class="col txt-card-itens">' +
//                                    '<span class="badge badge-primary badge-light">' + novoArray1[i].numerovagas + '</span>' +
//                                    '     <span class="badge badge-primary badge-light text-cards">Vagas</span>' +
//                                    '  </div>' +
//                                    ' </div>' +
//                                    '  </li>' +
//                                    '</ul>' +
//                                    '</div>' +
//                                    '</a>' +
//                                    ' </div>';




//                            $('.lstImoveis').append(linha);

                        }

                    }

                }

            }

        }



        $.ajax({
            method: "POST",
            url: base_url + "codigoImovelAjax",
            data: {codigo: novoArray, finalidade:1},
            async: true,
            //timeout:30000000,
            // complete: function(retorno){
            // },
            success: function (retorno) {



//                retorno = JSON.parse(retorno);
               
            


                $.each(retorno.lista, function (valor, imovel ) {
                 
                     

                    let linha = '<div class="col-sm-6 col-md-6 col-lg-3 col-xl-3" style="margin-bottom: 10px" style="box-shadow:10px 10px 10px  0px #8888884f;">' +
                            '<a  href="' + base_url + 'imoveis/' + imovel.codigo + '">' +
                            '<div class="card" style=" box-shadow:10px 10px 10px  0px #8888884f;">' +
                            '<img class="card-img-top" src="' + imovel.urlfotoprincipal +'" alt="Card image cap">' +
                            '<div class="valor-card-destaque">' +
                            '<h5 class="valor-texto">' + imovel.valor + '</h5>' +
                            '</div>' +
                            ' <div class="card-body">' +
                            ' <h5 class="card-title text-center" style="color:black">' + imovel.finalidade + '</h5>' +
                            ' <ul class="list-group list-group-flush corpoListDetalhe">' +
                            '  <li class="list-group-item d-flex justify-content-between align-items-center">' +
                            ' <span class="badge badge-primary badge-light"> </span>' +
                            '<span class="badge badge-primary badge-light">' + imovel.bairro + ' | ' + imovel.cidade + '</span>' +
                            '  <span class="badge badge-primary badge-light"> </span>' +
                            '</li>' +
                            '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                            ' <div class="d-flex justify-content-between align-items-center">' +
                            '  <img class="mr-1 iconeListaCardListagem"  src="' + base_url + 'assets/images/icons/quartos.svg" alt="Generic placeholder image">' +
                            ' <div class="col txt-card-itens">' +
                            '  <span class="badge badge-primary badge-light">' + imovel.numeroquartos + '</span>' +
                            '  <span class="badge badge-primary badge-light text-cards">Quartos</span> ' +
                            '</div>' +
                            ' </div>' +
                            '  <div class="d-flex justify-content-between align-items-center">' +
                            '  <img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/banheiro1.png" alt="Generic placeholder image">' +
                            '  <div class="col txt-card-itens">' +
                            '    <span class="badge badge-primary badge-light">' + imovel.numerobanhos + '</span>  ' +
                            '    <span class="badge badge-primary badge-light text-cards">Banheiros</span>' +
                            ' </div>' +
                            ' </div>' +
                            ' <div class="d-flex justify-content-between align-items-center">' +
                            '  <img class="mr-1 iconeListaCardListagem" src="' + base_url + 'assets/images/icons/car-solid.svg" alt="Generic placeholder image">' +
                            ' <div class="col txt-card-itens">' +
                            '<span class="badge badge-primary badge-light">' + imovel.numerovagas + '</span>' +
                            '     <span class="badge badge-primary badge-light text-cards">Vagas</span>' +
                            '  </div>' +
                            ' </div>' +
                            '  </li>' +
                            '</ul>' +
                            '</div>' +
                            '</a>' +
                            ' </div>';





                    $('.lstImoveis').append(linha);

                });




            }
        });


    });

    var mcOptions = {gridSize: 50, maxZoom: 15, imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'};
    markerCluster = new MarkerClusterer(map, markers, mcOptions);

}



function carregarImoveisAPI() {

    $(".lstImoveis").empty();

//    console.log(parametros);;

    $.ajax({
        method: "POST",
        url: base_url + "/imoveisMapa/",
        data: {paramentros_mapa: parametros},
        async: true,
        //timeout:30000000,
        // complete: function(retorno){
        // },
        success: function (retorno) {

            todosRegistros = jQuery.parseJSON(retorno);

            console.log(todosRegistros);

            recarregar(retorno);

            if (todosRegistros.quantidade > parametros.numeroPagina * 1000) {

                parametros.numeroPagina++;

                carregarImoveisAPI();
            }

        }

    }).always(function () {
        //ESCONDE O GIF
        $('#spinner').fadeOut("slow");
    });

}

carregarImoveisAPI();










//var markers = novoArray1.map(function (location, i) {
//
//        let contentString = '<div style="width:300px;height:300px"><div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-bottom: 10px">' +
//                '<div class="card" style=" box-shadow:10px 10px 10px 0px #8888884f">' +
//                '<img style="height:100px" class="card-img-top" src="' + location.foto + '" alt="Card image cap">' +
//                ' <div class="card-body">' +
//                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
//                '<img class="mr-1" style="width: 25px" src="' + base_url + 'assets/images/icons/home.svg" alt="Generic placeholder image">' +
//                '<span class="badge badge-primary badge-light">' + location.tipo + '</span>' +
//                '<i class="far fa-heart"></i>' +
//                ' </li>' +
//                ' <li class="list-group-item d-flex justify-content-between align-items-center">' +
//                ' <img class="mr-1" style="width: 25px" src="' + base_url + 'assets/images/icons/mapa.svg" alt="Generic placeholder image">' +
//                ' <span class="badge badge-primary badge-light">' + location.cidade + '</span>' +
//                '<span class="badge badge-primary badge-light">' + location.estado + '</span>' +
//                ' </li>' +
//                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
//                '<a href="' + base_url + 'imoveis/' + location.codigo + '" > <button type="button" class="btn btn-outline-primary btn-lg btn-block">Ver detalhes</button> </a> ' +
//                ' </li>' +
//                '</ul>' +
//                '</div>' +
//                '</div>';
//        
//        
//        var infowindow = new google.maps.InfoWindow({
//            content: contentString
//        });
//        var m = new google.maps.Marker({
//            position: location,
//            mapTypeId: 'roadmap',
//            content: contentString,
//            //label: "Novo titulo",
//            title: location.endereco,
//            map: map,
//            // icon: 'http://icon-icons.com/icons2/567/PNG/512/marker_icon-icons.com_54388.png'
//        });
//        m.addListener('click', function () {
//            infowindow.open(map, m);
//        });
////         m.addListener('mouseout', function () {
////            infowindow.close(map, m);
////        });
//
//        return m;
//    });
//    



//
//function gerarImovel(parametro) {
//
//    if (parametro.foto === undefined) {
//        imagem = '' + base_url + '/assets/images/banner/3.jpg';
//    } else {
//        imagem = parametro.foto;
//
//    }
//
//    let linha = '<div class=" col-sm-6 col-md-6 col-lg-4 col-xl-4" style="margin-bottom: 10px">' +
//            '<div class="card" style=" box-shadow:10px 10px 10px 0px #8888884f">' +
//            '<img class="card-img-top" src="' + imagem + '" alt="Card image cap">' +
//            ' <div class="card-body">' +
//            ' <h5 class="card-title text-center">' + parametro.finalidade + '</h5>' +
//            ' <ul class="list-group">' +
//            '  <li class="list-group-item d-flex justify-content-between align-items-center">' +
//            ' <h5 class="card-title text-center">Codigo: ' + parametro.codigo + '</h5>' +
//            '</li>' +
//            '<li class="list-group-item d-flex justify-content-between align-items-center">' +
//            '<img class="mr-1" style="width: 25px" src="' + base_url + 'assets/images/icons/home.svg" alt="Generic placeholder image">' +
//            '<span class="badge badge-primary badge-light">' + parametro.tipo + '</span>' +
//            '<i class="far fa-heart"></i>' +
//            ' </li>' +
//            ' <li class="list-group-item d-flex justify-content-between align-items-center">' +
//            ' <img class="mr-1" style="width: 25px" src="' + base_url + 'assets/images/icons/mapa.svg" alt="Generic placeholder image">' +
//            ' <span class="badge badge-primary badge-light">' + parametro.cidade + '</span>' +
//            '<span class="badge badge-primary badge-light">' + parametro.estado + '</span>' +
//            ' </li>' +
//            '<li class="list-group-item d-flex justify-content-between align-items-center">' +
//            '<img class="mr-1" style="width: 25px" src="' + base_url + 'assets/images/icons/quartos.svg" alt="Generic placeholder image">' +
//            '<span class="badge badge-primary badge-light">Quartos</span>' +
//            ' <span class="badge badge-primary badge-success">' + parametro.numeroquartos + '</span>' +
//            '</li>' +
//            '<li class="list-group-item d-flex justify-content-between align-items-center">' +
//            '   <img class="mr-1" style="width: 25px" src="' + base_url + 'assets/images/icons/car-solid.svg" alt="Generic placeholder image">' +
//            ' <span class="badge badge-primary badge-light">Vagas</span>' +
//            ' <span class="badge badge-primary badge-success">' + parametro.numerovagas + '</span>' +
//            ' </li>' +
//            '<li class="list-group-item d-flex justify-content-between align-items-center">' +
//            ' <img class="mr-1" style="width: 25px" src="' + base_url + 'assets/images/icons/banheiro1.png" alt="Generic placeholder image">' +
//            ' <span class="badge badge-primary badge-light">Banheiros</span>' +
//            ' <span class="badge badge-primary badge-success">' + parametro.numerobanhos + '</span>' +
//            '</li> ' +
//            ' <li class="list-group-item d-flex justify-content-between align-items-center">' +
//            ' <img class="mr-1" style="width: 25px" src="' + base_url + 'assets/images/icons/preco.svg" alt="Generic placeholder image">' +
//            '<span class="badge badge-primary badge-light">Preço</span>' +
//            '<span class="badge badge-primary badge-success">' + parametro.valor + '</span>' +
//            ' </li>' +
//            '<li class="list-group-item d-flex justify-content-between align-items-center">' +
//            '<a href="' + base_url + 'imoveis/' + parametro.codigo + '" > <button type="button" class="btn btn-outline-primary btn-lg btn-block">Ver detalhes</button> </a> ' +
//            ' </li>' +
//            '</ul>' +
//            '</div>' +
//            '</div>' +
//            '</div>';
//
//
//    return linha;
//
//}
//
//
//function AddImoveisNaLista(addLista) {
//
//    $(".lstImoveis").empty();
//
//    $.each(addLista, function (key, value) {
//        let param = gerarImovel(value);
//        $(".lstImoveis").append(param);
//    });
//
//}
//
//
//var parametros = {
//    finalidade: 1,
//    tipo: 'todos',
//    preco: 'todos',
//    numeroPagina: 1,
//    city: 'Belo Horizonte',
//    banheiros: 'todos',
//    codigo: 'todos',
//    quartos: 'todos',
//    preco: 'todos',
//    garagem: 'todos',
//    arealote: 'todos',
//    areainterna: 'todos'
//
//
//
//};
//
//function getParametros() {
//    return parametros;
//}
//
//
//$('#getIMoveis').on('click', function () {
//    
//    
//    $('#spinner').fadeOut("slow");
//
//    $.ajax({
//
//        method: "POST",
//        url: '' + base_url + 'imoveisMapa',
//        data: {parametros},
//        async: false,
//        //timeout:30000000,
//        // complete: function(retorno){
//        // },
//        success: function (retorno) {
//
//            todosRegistros = jQuery.parseJSON(retorno);
//
//
//            todosRegistros = formatarDados(todosRegistros);
//            console.log(todosRegistros);
//
////          
////            arrayTeste = todosRegistros.lista;
//           
//
//
//        }
//
//    }).always(function () {
//        $('#spinner').fadeOut("slow");
//         initMap(todosRegistros);
//    });
//
//
//});
//
//
//
//
//
//function initMap(novoArray) {
//    
//    console.log(novoArray);debugger;
////    novoArray = arrayTeste;
//    var map = objetoPadraoMapa();
//    // Create an array of alphabetical characters used to label the markers.
//    var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
//    // Add some markers to the map.
//    // Note: The code uses the JavaScript Array.prototype.map() method to
//    // create an array of markers based on a given "locations" array.
//    // The map() method here has nothing to do with the Google Maps API.
//
//    let novoArray1 = [];
//    $.each(novoArray, function (y, z) {
//        novoArray1.push({
//            'endereco': z.endereco, "lat": z.latitude, "lng": z.longitude, 'foto': z.fotos[0].url, "finalidade": z.finalidade, "tipo": z.tipo,
//            "cidade": z.cidade, "numeroquartos": z.numeroquartos, "numerovagas": z.numerovagas, "numerobanhos": z.numerobanhos, "tipo": z.tipo, "valor": z.valor,
//            "codigo": z.codigo, "estado": z.estado
//        });
//    });
//
//
//    var markers = novoArray1.map(function (location, i) {
//
//
////        var contentString = '<div id="content"><h1 style="font-size:14px">' + location.endereco + '</h1><img style="width:100px" src="' + location.foto + '" alt="" /></div>';;
//
//        let contentString = '<div style="width:300px;height:300px"><div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style="margin-bottom: 10px">' +
//                '<div class="card" style=" box-shadow:10px 10px 10px 0px #8888884f">' +
//                '<img style="height:100px" class="card-img-top" src="' + location.foto + '" alt="Card image cap">' +
//                ' <div class="card-body">' +
//                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
//                '<img class="mr-1" style="width: 25px" src="' + base_url + 'assets/images/icons/home.svg" alt="Generic placeholder image">' +
//                '<span class="badge badge-primary badge-light">' + location.tipo + '</span>' +
//                '<i class="far fa-heart"></i>' +
//                ' </li>' +
//                ' <li class="list-group-item d-flex justify-content-between align-items-center">' +
//                ' <img class="mr-1" style="width: 25px" src="' + base_url + 'assets/images/icons/mapa.svg" alt="Generic placeholder image">' +
//                ' <span class="badge badge-primary badge-light">' + location.cidade + '</span>' +
//                '<span class="badge badge-primary badge-light">' + location.estado + '</span>' +
//                ' </li>' +
//                '<li class="list-group-item d-flex justify-content-between align-items-center">' +
//                '<a href="' + base_url + 'imoveis/' + location.codigo + '" > <button type="button" class="btn btn-outline-primary btn-lg btn-block">Ver detalhes</button> </a> ' +
//                ' </li>' +
//                '</ul>' +
//                '</div>' +
//                '</div>';
//        
//        
//        var infowindow = new google.maps.InfoWindow({
//            content: contentString
//        });
//        var m = new google.maps.Marker({
//            position: location,
//            mapTypeId: 'roadmap',
//            content: contentString,
//            //label: "Novo titulo",
//            title: location.endereco,
//            map: map,
//            // icon: 'http://icon-icons.com/icons2/567/PNG/512/marker_icon-icons.com_54388.png'
//        });
//        m.addListener('click', function () {
//            infowindow.open(map, m);
//        });
////         m.addListener('mouseout', function () {
////            infowindow.close(map, m);
////        });
//
//        return m;
//    });
//    
//    
//    
//    
//    // Add a marker clusterer to manage the markers.
//    var markerCluster = new MarkerClusterer(map, markers, {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
//    //adicionar um evento que dispara quando para de arrastar
//    // map.addListener('idle', function () {
//
//    // });
//
//
////    map.addListener('idle', function () {
////
////
////        $(".lstImoveis").empty();
////        var bounds = map.getBounds();
////        var southWest = bounds.getSouthWest();
////        var northEast = bounds.getNorthEast();
////        var sudoeste_latitude = southWest.lat();
////        var sudoeste_longitude = southWest.lng();
////        var nordeste_latitude = northEast.lat();
////        var nordeste_longitude = northEast.lng();
////        var novoArray = [];
////        
////        
////
////
////        for (var i = novoArray1.length - 1; i >= 0; i--) {
////
////            //SE ESTIVER FORA DAS DIMENÇÕES DA ESQUERDA E DE BAIXO ENTRA NO IF
////            if (novoArray1[i].lat > sudoeste_latitude) {
////
////                if (novoArray1[i].lng > sudoeste_longitude) {
////
////                    if (novoArray1[i].lat < nordeste_latitude) {
////
////                        if (novoArray1[i].lng < nordeste_longitude) {
////
////                            novoArray.push(novoArray1[i]);
////                        }
////
////                    }
////
////                }
////
////            }
////
////        }
////
////        removerDaList(novoArray);
////        novoArray = [];
////    });
//
//
//
//}
//
//
//
//function removerDaList(imovelRemove) {
//    if (imovelRemove.length === 0) {
//
//        imovelRemove = [];
//        AddImoveisNaLista(imovelRemove);
//    } else {
//
//        $.each(imoveis, function (key, value) {
//
//
//
//            $.each(imovelRemove, function (key1, val1) {
//
//                AddImoveisNaLista(imovelRemove);
//
//
//                if (value.latitude == val1.lat && value.longitude == val1.lng) {
//
//                    AddImoveisNaLista(imovelRemove);
//                }
//
//            });
//        });
//    }
//
//}


