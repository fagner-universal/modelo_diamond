<?php

class Imovel {

    private $finalidade = 1;    /* OBRIGATÓRIO - Enviar 1 para ALUGUEL ou 2 para VENDA; */
    private $codigounidade = '';  // OPCIONAL - Enviar código da unidade ou vazio para todas;
    private $codigosimoveis = ''; // OPCIONAL - Enviar os códigos dos imóveis separados por vírgula (,) ou vazio para todos
    private $codigoTipo = '';     //OPCIONAL - Enviar o código do tipo de imóvel selecionado de acordo com a lista existente (RetornarTiposImoveisDisponiveis), para mais de um tipo, separar por vírgula (,) ou vazio para todos
    private $codigocidade = 0;    //OPCIONAL - Enviar o código da cidade selecionada de acordo com a lista existente (RetornarCidadesDisponiveis) ou 0 para todos
    private $codigoregiao = 0;    //OPCIONAL - Enviar o código da região selecionada de acordo com a lista existente (RetornarRegioesDisponiveis) ou 0 para todos
    private $codigosbairros = 0;  // OPCIONAL - Enviar os códigos dos bairros selecionados de acordo com a lista existente (RetornarBairrosDisponiveis) separados por vírgula (,) ou vazio para todos
    private $endereco = '';       //OPCIONAL - Enviar parte do logradouro do endereço ou vazio para todos
    private $numeroquartos = 0; // OPCIONAL - Enviar nº de quartos a partir, 0 para todos
    private $numerovagas = 0; // OPCIONAL - Enviar nº de vagas a partir, 0 para todos
    private $numerobanhos = 0; //OPCIONAL - Enviar nº de banheiros a partir, 0 para todos
    private $numerosuite = 0; //OPCIONAL - Enviar nº de suítes a partir, 0 para todos
    private $numerovaranda = 0; //OPCIONAL - Enviar nº de varandas a partir, 0 para todos
    private $numeroelevador = 0; //OPCIONAL - Enviar nº de elevadores a partir, 0 para todos
    private $valorde = 0; //OPCIONAL - Enviar valor a partir, 0 para todos
    private $valorate = 0; // OPCIONAL - Enviar valor até, 0 para todos
    private $areade = 0; //OPCIONAL - Enviar área a partir, 0 para todos
    private $areaate = 0; // OPCIONAL - Enviar área até, 0 para todos
    private $extras = 0; // OPCIONAL - Enviar código gerado no CRM para o campo extra, separados por vírgula (,) ou vazio para não filtrar
    private $extends = false; //interfone: OPCIONAL - Enviar true ou false
    private $mobiliado = false; //OPCIONAL - Enviar true ou false
    private $dce = false; // OPCIONAL - Enviar true ou false
    private $piscina = false; //OPCIONAL - Enviar true ou false
    private $sauna = false; // OPCIONAL - Enviar true ou false
    private $salaofestas = false; // OPCIONAL - Enviar true ou false
    private $academia = false; //OPCIONAL - Enviar true ou false
    private $boxDespejo = false; // OPCIONAL - Enviar true ou false
    private $portaria24h = false; // OPCIONAL - Enviar true ou false
    private $aceitafinanciamento = false;  // OPCIONAL - Enviar true ou false
    private $arealazer = false; //  OPCIONAL - Enviar true ou false
    private $quartoqtdeexata = false; //  OPCIONAL - Enviar true ou false
    private $vagaqtdexata = false;  //OPCIONAL - Enviar true ou false
    private $destaque = 0; // OPCIONAL - Enviar 1 para simples, 2 para destaque ou 3 para super destaque, 0 para todos
    private $opcaoimovel = 4; //OPCIONAL - Enviar 1 para somente avulsos, 2 para somente lançamentos, 3 para unidades de lançamentos, 4 para avulsos e lançamentos mãe, 0 para todos (avulsos e lançamentos por tipo e m²)
    private $retornomapa = false; // OPCIONAL - Enviar true ou false, usado para exibir os imóveis no mapa (retorno com até 100 registros e JSON reduzido)
    private $retornomapaapp = false; // OPCIONAL - Enviar true ou false, usado para exibir os imóveis no mapa (retorno com até 100 registros e JSON reduzido)
    private $numeropagina = 1; // OBRIGATÓRIO - Usado para paginação, enviar o nº da página atual
    private $numeroregistros = 20; // OBRIGATÓRIO - Usado máximo de imóveis para retorno, máximo 50
    private $ordenacao = ''; //OPCIONAL - Tipo de ordenação, valorasc para valor crescente, valordesc para valor decrescente, ou vazio para assumir destaque decrescente
    private $areaprivativa = false;
    private $areaservico = false;
    private $circuitotv = false;
    private $elevador = false;
    private $varandagourmet = false;
    private $varanda = false;
    
    public function __construct() {
//        $finalidade = 1;    /* OBRIGATÓRIO - Enviar 1 para ALUGUEL ou 2 para VENDA; */
//        $codigounidade = '';  // OPCIONAL - Enviar código da unidade ou vazio para todas;
//        private $codigosimoveis = ''; // OPCIONAL - Enviar os códigos dos imóveis separados por vírgula (,) ou vazio para todos
//        private $codigoTipo = '';     //OPCIONAL - Enviar o código do tipo de imóvel selecionado de acordo com a lista existente (RetornarTiposImoveisDisponiveis), para mais de um tipo, separar por vírgula (,) ou vazio para todos
//        private $codigocidade = 0;    //OPCIONAL - Enviar o código da cidade selecionada de acordo com a lista existente (RetornarCidadesDisponiveis) ou 0 para todos
//        private $codigoregiao = 0;    //OPCIONAL - Enviar o código da região selecionada de acordo com a lista existente (RetornarRegioesDisponiveis) ou 0 para todos
//        private $codigosbairros = 0;  // OPCIONAL - Enviar os códigos dos bairros selecionados de acordo com a lista existente (RetornarBairrosDisponiveis) separados por vírgula (,) ou vazio para todos
//        private $endereco = 0;       //OPCIONAL - Enviar parte do logradouro do endereço ou vazio para todos
//        private $numeroquartos = 0; // OPCIONAL - Enviar nº de quartos a partir, 0 para todos
//        private $numerovagas = 0; // OPCIONAL - Enviar nº de vagas a partir, 0 para todos
//        private $numerobanhos = 0; //OPCIONAL - Enviar nº de banheiros a partir, 0 para todos
//        private $numerosuite = 0; //OPCIONAL - Enviar nº de suítes a partir, 0 para todos
//        private $numerovaranda = 0; //OPCIONAL - Enviar nº de varandas a partir, 0 para todos
//        private $numeroelevador = 0; //OPCIONAL - Enviar nº de elevadores a partir, 0 para todos
//        private $valorde = 0; //OPCIONAL - Enviar valor a partir, 0 para todos
//        private $valorate = 0; // OPCIONAL - Enviar valor até, 0 para todos
//        private $areade = 0; //OPCIONAL - Enviar área a partir, 0 para todos
//        private $areaate = 0; // OPCIONAL - Enviar área até, 0 para todos
//        private $extras = 0; // OPCIONAL - Enviar código gerado no CRM para o campo extra, separados por vírgula (,) ou vazio para não filtrar
//        private $extends = false; //interfone: OPCIONAL - Enviar true ou false
//        private $mobiliado = false; //OPCIONAL - Enviar true ou false
//        private $dce = false; // OPCIONAL - Enviar true ou false
//        private $piscina = false; //OPCIONAL - Enviar true ou false
//        private $sauna = false; // OPCIONAL - Enviar true ou false
//        private $salaofestas = false; // OPCIONAL - Enviar true ou false
//        private $academia = false; //OPCIONAL - Enviar true ou false
//        private $boxDespejo = false; // OPCIONAL - Enviar true ou false
//        private $portaria24h = false; // OPCIONAL - Enviar true ou false
//        private $aceitafinanciamento = false;  // OPCIONAL - Enviar true ou false
//        private $arealazer = false; //  OPCIONAL - Enviar true ou false
//        private $quartoqtdeexata = false; //  OPCIONAL - Enviar true ou false
//        private $vagaqtdexata = false;  //OPCIONAL - Enviar true ou false
//        private $destaque = 0; // OPCIONAL - Enviar 1 para simples, 2 para destaque ou 3 para super destaque, 0 para todos
//        private $opcaoimovel = 4; //OPCIONAL - Enviar 1 para somente avulsos, 2 para somente lançamentos, 3 para unidades de lançamentos, 4 para avulsos e lançamentos mãe, 0 para todos (avulsos e lançamentos por tipo e m²)
//        private $retornomapa = false; // OPCIONAL - Enviar true ou false, usado para exibir os imóveis no mapa (retorno com até 100 registros e JSON reduzido)
//        private $retornomapaapp = false; // OPCIONAL - Enviar true ou false, usado para exibir os imóveis no mapa (retorno com até 100 registros e JSON reduzido)
//        private $numeropagina = 1; // OBRIGATÓRIO - Usado para paginação, enviar o nº da página atual
//        private $numeroregistros = 20; // OBRIGATÓRIO - Usado máximo de imóveis para retorno, máximo 50
//        private $ordenacao = ''; //OPCIONAL - Tipo de ordenação, valorasc para valor crescente, valordesc para valor decrescente, ou vazio para assumir destaque decrescente
//    
//        
    }

    function getCodigoCondominio() {

        return $this->codigocondominio;
    }

    function getElevador() {

        return $this->elevador;
    }

    function getVarandaGourmet() {

        return $this->varandagourmet;
    }

    function getFinalidade() {
        return $this->finalidade;
    }

    function getCodigounidade() {
        return $this->codigounidade;
    }

    function getCodigosimoveis() {
        return $this->codigosimoveis;
    }

    function getCodigoTipo() {
        return $this->codigoTipo;
    }

    function getCodigocidade() {
        return $this->codigocidade;
    }

    function getCodigoregiao() {
        return $this->codigoregiao;
    }

    function getCodigosbairros() {
        return $this->codigosbairros;
    }

    function getEndereco() {
        return $this->endereco;
    }

    function getNumeroquartos() {

        return $this->numeroquartos;
    }

    function getNumerovagas() {
        return $this->numerovagas;
    }

    function getNumerobanhos() {
        return $this->numerobanhos;
    }

    function getNumerosuite() {
        return $this->numerosuite;
    }

    function getNumerovaranda() {
        return $this->numerovaranda;
    }

    function getNumeroelevador() {
        return $this->numeroelevador;
    }

    function getValorde() {
        return $this->valorde;
    }

    function getValorate() {
        return $this->valorate;
    }

    function getAreade() {
        return $this->areade;
    }

    function getAreaate() {
        return $this->areaate;
    }

    function getExtras() {
        return $this->extras;
    }

    function getExtends() {
        return $this->extends;
    }

    function getMobiliado() {
        return $this->mobiliado;
    }

    function getDce() {
        return $this->dce;
    }

    function getPiscina() {
        return $this->piscina;
    }

    function getSauna() {
        return $this->sauna;
    }

    function getSalaofestas() {
        return $this->salaofestas;
    }

    function getAcademia() {
        return $this->academia;
    }

    function getBoxDespejo() {
        return $this->boxDespejo;
    }

    function getPortaria24h() {
        return $this->portaria24h;
    }

    function getAceitafinanciamento() {
        return $this->aceitafinanciamento;
    }

    function getArealazer() {
        return $this->arealazer;
    }

    function getQuartoqtdeexata() {
        return $this->quartoqtdeexata;
    }

    function getVagaqtdexata() {
        return $this->vagaqtdexata;
    }

    function getDestaque() {
        return $this->destaque;
    }

    function getOpcaoimovel() {
        return $this->opcaoimovel;
    }

    function getRetornomapa() {
        return $this->retornomapa;
    }

    function getRetornomapaapp() {
        return $this->retornomapaapp;
    }

    function getNumeropagina() {
        return $this->numeropagina;
    }

    function getNumeroregistros() {
        return $this->numeroregistros;
    }

    function getOrdenacao() {
        return $this->ordenacao;
    }

    function getAreaPrivativa() {
        return $this->areaprivativa;
    }

    function getAreaServico() {
        return $this->areaservico;
    }

    function getCircuitoTv() {
        return $this->circuitotv;
    }
    
    function getVaranda() {
        return $this->varanda;
    }

    /*
     * 
     * METODO DE SET 
     * 
     * 
     */

    function setFinalidade($finalidade) {

        if ($finalidade == 'aluguel' || $finalidade == 1) {
            $finalidade = 1;
        }
        if ($finalidade == 'venda' || $finalidade == 2) {
            $finalidade = 2;
        }


        $this->finalidade = $finalidade;
    }

    function setVaranda($varanda) {

        $this->varanda = $varanda;
    }

    function setElevador($elevador) {

        $this->elevador = $elevador;
    }

    function setVarandaGourmet($varandagourmet) {
        
        if($varandagourmet == 'true'){
            
             $this->varandagourmet = '11';
             
        }else{
            
            $this->varandagourmet = '';
        }
        
    }

    function setCodigoCondominio($codigocondominio){
        $this->codigocondominio = $codigocondominio;
    }

    function setCodigounidade($codigounidade) {
        $this->codigounidade = $codigounidade;
    }

    function setCodigosimoveis($codigosimoveis) {
        $this->codigosimoveis = $codigosimoveis;
    }

    function setCodigoTipo($codigoTipo) {


        if ($codigoTipo == 'imoveis') {
            $codigoTipo = '';
        }

        $this->codigoTipo = $codigoTipo;
    }

    function setCodigocidade($codigocidade) {

        //DEFINE A CIDADE PELA URL QUANDO ESTA MARCADO COMO TODOS
        if ($codigocidade == 'todos-os-bairros') {
            $codigocidade = 0;
        }


        $this->codigocidade = (int) $codigocidade;
    }

    function setCodigoregiao($codigoregiao) {
        $this->codigoregiao = $codigoregiao;
    }

    function setCodigosbairros($codigosbairros) {


        //BAIRRO QUANDO MARCADO TODOS
        if ($codigosbairros == 'todos-os-bairros') {
            $codigosbairros = '';
        }


        $this->codigosbairros = $codigosbairros;
    }

    function setEndereco($endereco) {
        $this->endereco = $endereco;
    }

    function setNumeroquartos($quarto) {


//        //DEFINIR NUMERO DE VAGAS

        if ($quarto == '0-quartos') {
            $quarto = 0;
        }

        if ($quarto == '1-quartos' || $quarto == 1) {
            $quarto = '-1';
        }
        if ($quarto == '2-quartos' || $quarto == 2) {
            $quarto = '-2';
        }
        if ($quarto == '3-quartos' || $quarto == 3) {
            $quarto = '-3';
        }
        if ($quarto == '4-quartos' || $quarto == 4) {
            $quarto = '-4';
        }
        if ($quarto == '5-quartos' || $quarto == 5) {
            $quarto = 5;
        }


        if ($quarto == '0-quarto-ou-mais' || $quarto == '0-quarto') {
            $quarto = 0;
        }


        if ($quarto == '1-quarto-ou-mais' || $quarto == '1-quarto') {
            $quarto = '-1';
        }
        if ($quarto == '2-quartos-ou-mais' || $quarto == '2-quarto') {
            $quarto = '-2';
        }
        if ($quarto == '3-quartos-ou-mais' || $quarto == '3-quarto') {
            $quarto = '-3';
        }
        if ($quarto == '4-quartos-ou-mais' || $quarto == '4-quarto') {
            $quarto = '-4';
        }
        if ($quarto == '5-quartos-ou-mais' || $quarto == '5-quarto') {
            $quarto = 5;
        }

        
        $this->numeroquartos = $quarto;
    }

    function setNumerovagas($numerovagas) {

        //DEFINIR NUMERO DE VAGAS

        
   

        if ($numerovagas == '0-vaga' || $numerovagas == '0') {
            $numerovagas = '0';
        }


        if ($numerovagas == '0-vaga-ou-mais' || $numerovagas == '0') {
            $numerovagas = '0';
        }

        if ($numerovagas == '1-vaga-ou-mais' || $numerovagas == '1') {
            $numerovagas = '-1';
        }
        if ($numerovagas == '2-vaga-ou-mais' || $numerovagas == '2' ) {
            $numerovagas = '-2';
        }
        if ($numerovagas == '3-vaga-ou-mais' || $numerovagas == '3') {
            $numerovagas = '-3';
        }
        if ($numerovagas == '4-vaga-ou-mais' || $numerovagas == '4') {
            $numerovagas = '-4';
        }
        if ($numerovagas == '5-vaga-ou-mais' || $numerovagas == '5') {
            $numerovagas = '5';
        }


        if ($numerovagas == '1-vaga' || $numerovagas == '1-vaga') {
            $numerovagas = '-1';
        }
        if ($numerovagas == '2-vagas' || $numerovagas == '2-vaga') {
            $numerovagas = '-2';
        }
        if ($numerovagas == '3-vagas' || $numerovagas == '3-vaga') {
            $numerovagas = '-3';
        }
        if ($numerovagas == '4-vagas' || $numerovagas == '4-vaga') {
            $numerovagas = '-4';
        }
         if ($numerovagas == '5-vagas' || $numerovagas == '5-vaga') {
            $numerovagas ='5';
        }
        

        if ($numerovagas == '' || $numerovagas == '0') {
            $numerovagas = '0';
        }


        $this->numerovagas = $numerovagas;
    }

    function setNumerobanhos($numerobanhos) {

        //BANHEIROS QUANDO MARCADO TODOS

        if ($numerobanhos == '0-banheiro') {
            $numerobanhos = 0;
        }

        if ($numerobanhos == '0-banheiro-ou-mais') {
            $numerobanhos = '0';
        }

        if ($numerobanhos == '1-banheiro-ou-mais' || $numerobanhos == '1' ) {
            $numerobanhos = '-1';
        }
        if ($numerobanhos == '2-banheiro-ou-mais' || $numerobanhos == '2') {
            $numerobanhos = '-2';
        }
        if ($numerobanhos == '3-banheiro-ou-mais' || $numerobanhos == '3') {
            $numerobanhos = '-3';
        }
        if ($numerobanhos == '4-banheiro-ou-mais' || $numerobanhos == '4') {
            $numerobanhos = '-4';
        }
        
        if ($numerobanhos == '5-banheiro-ou-mais' || $numerobanhos == '5') {
            $numerobanhos = '5';
        }

        //DEFINIR NUMERO DE banheiros
        if ($numerobanhos == '1-banheiro' || $numerobanhos == '1-banheiros') {
            $numerobanhos = '-1';
        }
        if ($numerobanhos == '2-banehiros' || $numerobanhos == '2-banheiros') {
            $numerobanhos = '-2';
        }
        if ($numerobanhos == '3-banheiros' || $numerobanhos == '3-banheiros') {
            $numerobanhos = '-3';
        }
        if ($numerobanhos == '4-banheiros' || $numerobanhos == '4-banheiros') {
            $numerobanhos = '-4';
        }
        if ($numerobanhos == '5-banheiros' || $numerobanhos == '4-banheiros') {
            $numerobanhos = '5';
        }

        if ($numerobanhos == '' || $numerobanhos == '0') {
            $numerobanhos = 0;
        }

        $this->numerobanhos = $numerobanhos;
    }

    function setNumerosuite($numerosuite) {

        if ($numerosuite == '0-suite-ou-mais') {
            $numerosuite = 0;
        }

        if ($numerosuite == '1-suite-ou-mais') {
            $numerosuite = -1;
        }
        if ($numerosuite == '2-suite-ou-mais') {
            $numerosuite = -2;
        }
        if ($numerosuite == '3-suite-ou-mais') {
            $numerosuite = -3;
        }
        if ($numerosuite == '4-suite-ou-mais') {
            $numerosuite = -4;
        }
         if ($numerosuite == '5-suite-ou-mais') {
            $numerosuite = 5;
        }


        //DEFINIR NUMERO DE banheiros
        if ($numerosuite == '1-suite') {
            $numerosuite = -1;
        }
        if ($numerosuite == '2-suites') {
            $numerosuite = -2;
        }
        if ($numerosuite == '3-suites') {
            $numerosuite = -3;
        }
        if ($numerosuite == '4-suites') {
            $numerosuite = -4;
        }
         if ($numerosuite == '4-suites') {
            $numerosuite = 5;
        }

        if ($numerosuite == '' || $numerosuite == '0') {
            $numerosuite = 0;
        }



        $this->numerosuite = $numerosuite;
    }

    function setNumerovaranda($numerovaranda) {
        $this->numerovaranda = $numerovaranda;
    }

    function setNumeroelevador($numeroelevador) {
        $this->numeroelevador = $numeroelevador;
    }

    function setValorde($valorde) {
        $this->valorde = $valorde;
    }

    function setValorate($valorate) {
        $this->valorate = $valorate;
    }

    function setAreade($areade) {
        $this->areade = $areade;
    }

    function setAreaate($areaate) {
        $this->areaate = $areaate;
    }

    function setExtras($extras) {
        $this->extras = $extras;
    }

    function setExtends($extends) {
        $this->extends = $extends;
    }

    function setMobiliado($mobiliado) {
        $this->mobiliado = $mobiliado;
    }

    function setDce($dce) {
        $this->dce = $dce;
    }

    function setPiscina($piscina) {
        $this->piscina = $piscina;
    }

    function setSauna($sauna) {
        $this->sauna = $sauna;
    }

    function setSalaofestas($salaofestas) {
        $this->salaofestas = $salaofestas;
    }

    function setAcademia($academia) {
        $this->academia = $academia;
    }

    function setBoxDespejo($boxDespejo) {
        $this->boxDespejo = $boxDespejo;
    }

    function setPortaria24h($portaria24h) {
        $this->portaria24h = $portaria24h;
    }

    function setAceitafinanciamento($aceitafinanciamento) {
        $this->aceitafinanciamento = $aceitafinanciamento;
    }

    function setArealazer($arealazer) {
        $this->arealazer = $arealazer;
    }

    function setQuartoqtdeexata($quartoqtdeexata) {
        $this->quartoqtdeexata = $quartoqtdeexata;
    }

    function setVagaqtdexata($vagaqtdexata) {
        $this->vagaqtdexata = $vagaqtdexata;
    }

    function setDestaque($destaque) {
        $this->destaque = $destaque;
    }

    function setOpcaoimovel($opcaoimovel) {
        $this->opcaoimovel = $opcaoimovel;
    }

    function setRetornomapa($retornomapa) {
        $this->retornomapa = $retornomapa;
    }

    function setRetornomapaapp($retornomapaapp) {
        $this->retornomapaapp = $retornomapaapp;
    }

    function setNumeropagina($numeropagina) {
        $this->numeropagina = $numeropagina;
    }

    function setNumeroregistros($numeroregistros) {
        $this->numeroregistros = $numeroregistros;
    }

    function setOrdenacao($ordenacao) {
        $this->ordenacao = $ordenacao;
    }

    function setAreaPrivativa($areaprivativa) {
        $this->areaprivativa = $areaprivativa;
    }

    function setAreaServico($areaservico) {
        $this->areaservico = $areaservico;
    }

    function setCircuitoTv($circuitotv) {
        $this->circuitotv = $circuitotv;
    }

}

?>