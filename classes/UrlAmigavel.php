<?php

class UrlAmigavel {

    public $low = array("Á" => "á", "É" => "é", "Í" => "í", "Ó" => "ó", "Ú" => "ú", "Ü" => "ü", "não" => "não", "Ç" => "ç");
    //CLIENTE JOMAR
    public $chave = CHAVE;

    //CHAVE DEMO
    // public $chave = 'RZZuhI4HKPnkrUuToq3j8A==';

    public function converterParaCodigoFinalidade($finalidade) {

        if ($finalidade == "venda") {
            $finalidade = 2;
        } else if ($finalidade == "aluguel") {
            $finalidade = 1;
        } else {
            $finalidade = 0;
        }

        return $finalidade;
    }


    public function gerarTitulo($titulo){


        $titulo = $this->tirarAcentos($titulo);
        $titulo  = str_replace('ç', 'c', mb_strtolower(strtr($titulo, $this->low), 'UTF-8'));
        $titulo = str_replace('(', '', $titulo);
        $titulo = str_replace(')', '', $titulo);
        $titulo = str_replace(' ', '-', $titulo);
        $titulo = str_replace(',', '-', $titulo);
        $titulo = str_replace('/', '-', $titulo);
        $titulo = str_replace('$', '', $titulo);
        $titulo = str_replace('|', '-', $titulo);
        $titulo = str_replace('.', '', $titulo);
        $titulo = str_replace(':', '', $titulo);
        $titulo = str_replace('--', '-', $titulo);
        $titulo = str_replace('²', '-', $titulo);
        $titulo = str_replace('!', '-', $titulo);
        $titulo = str_replace('?', '-', $titulo);
        

     

        return $titulo;

    }
    
    public function urlCidade($resultado) {


//        $resultado = json_decode($resultado);
        $resultado = (array) $resultado;



        //COLOCA TODAS AS LETRAS EM MINUSCULOS
        $array = array();
        foreach ($resultado["lista"] as $key => $value) {
            $array[] = array('nome' => $this->tirarAcentos($value->nome), 'codigo' => $value->codigo);
        }


        //TIRA TODOS OS ACENTOS
        $array2 = array();
        foreach ($array as $key => $value2) {
            $array2[] = array('nome' => str_replace('ç', 'c', mb_strtolower(strtr($value2['nome'], $this->low), 'UTF-8')), 'codigo' => $value2['codigo']);
        }


        //SUBSTITUE OS ESPAÇOES POR "-"
        $array3 = array();
        foreach ($array2 as $key => $value3) {
            $array3[] = array('nome' => str_replace(' ', '-', $value3['nome']), 'codigo' => $value3['codigo']);
        }


        //INSERIR NOME ORIGINAL NO ARRAY
        foreach ($array3 as $key => $value) {
            $array3[$key]['nomeOriginal'] = $resultado["lista"][$key]->nome;
        }


        return $array3;
    }

    public function urlCondominio($cond) {

        $resultado = (array)$cond;
        

        //COLOCA TODAS AS LETRAS EM MINUSCULOS
        $array = array();
        foreach ($resultado["lista"] as $key => $value) {
            $array[] = array('nome' => $this->amigavelURL($value->nome), 'codigo' => $value->codigo);
        }


        //TIRA TODOS OS ACENTOS
        $array2 = array();
        foreach ($array as $key => $value2) {
            $array2[] = array('nome' => str_replace('ç', 'c', mb_strtolower(strtr($value2['nome'], $this->low), 'UTF-8')), 'codigo' => $value2['codigo']);
        }


        //SUBSTITUE OS ESPAÇOES POR "-"
        $array3 = array();
        foreach ($array2 as $key => $value3) {
            $array3[] = array('nome' => str_replace(' ', '-', $value3['nome']), 'codigo' => $value3['codigo']);
        }


        //INSERIR NOME ORIGINAL NO ARRAY
        foreach ($array3 as $key => $value) {
            $array3[$key]['nomeOriginal'] = $resultado["lista"][$key]->nome;
        }
        

        return $array3;
        
    }

    public function urlBairro($resultado) {

        $resultado = (array) $resultado;



        //COLOCA TODAS AS LETRAS EM MINUSCULOS
        $array = array();
        foreach ($resultado["lista"] as $key => $value) {
            $array[] = array(
                'nome' => $this->tirarAcentos($value->nome),
                'nome_cidade' => $this->tirarAcentos($value->cidade),
                'codigo' => $value->codigo);
        }


        //TIRA TODOS OS ACENTOS
        $array2 = array();
        foreach ($array as $key => $value2) {
            $array2[] = array(
                'nome' => str_replace('ç', 'c', mb_strtolower(strtr($value2['nome'], $this->low), 'UTF-8')),
                'nome_cidade' => str_replace('ç', 'c', mb_strtolower(strtr($value2['nome_cidade'], $this->low), 'UTF-8')),
                'codigo' => $value2['codigo']);
        }


        //SUBSTITUE OS ESPAÇOES POR "-"
        $array3 = array();
        foreach ($array2 as $key => $value3) {
            $array3[] = array(
                'nome' => str_replace(' ', '-', $value3['nome']),
                'nome_cidade' => str_replace(' ', '-', $value3['nome_cidade']),
                'codigo' => $value3['codigo']);
        }



        //INSERIR NOME ORIGINAL NO ARRAY
        foreach ($array3 as $key => $value) {
            $array3[$key]['nomeOriginal'] = $this->tirarAcentos($resultado["lista"][$key]->nome);
            $array3[$key]['nomeCidadeOriginal'] = $this->tirarAcentos($resultado["lista"][$key]->cidade);
        }


        return $array3;
    }

    public function urlTipo($resultado) {


        //COLOCA TODAS AS LETRAS EM MINUSCULOS
        $array = array();
        foreach ($resultado->lista as $key => $value) {
            $array[] = array('nome' => $this->tirarAcentos($value->nome), 'codigo' => $value->codigo);
        }


        //TIRA TODOS OS ACENTOS
        $array2 = array();
        foreach ($array as $key => $value2) {
            $array2[] = array('nome' => str_replace('ç', 'c', mb_strtolower(strtr($value2['nome'], $this->low), 'UTF-8')), 'codigo' => $value2['codigo']);
        }


        //SUBSTITUE OS ESPAÇOES POR "-"
        $array3 = array();
        foreach ($array2 as $key => $value3) {
            $array3[] = array('nome' => str_replace(' ', '-', $value3['nome']), 'codigo' => $value3['codigo']);
        }

        //INSERIR NOME ORIGINAL NO ARRAY
        foreach ($array3 as $key => $value) {
            $array3[$key]['nomeOriginal'] = $resultado->lista[$key]->nome;
        }



        return $array3;
    }

    /*
     * 
     * 
     * 
     * CONVERTER PARA CÓDIGO
     * 
     * 
     * 
     */

    public function converterParaCodigoTipo($tipoTratado, $tipo) {


        $dados = array(
            'codigo' => ''
        );

        $parametroDigitado = explode('--', $tipo);



        for ($i = 0; $i < count($parametroDigitado); $i ++) {

            foreach ($tipoTratado as $key => $tTradado) {

                if ($parametroDigitado[$i] == $tTradado['nome']) {

                    $dados['codigo'] .= $tTradado['codigo'] . ',';
                }
            }
        }




        return $dados;
    }

    public function converterParaCodigoCidade($cidades, $cidade) {

        $dados = array();

        $cidadePorExtenso = '';


        if ($cidade != '') {

            foreach ($cidades as $key => $value) {

                if ($cidade == $value['nome']) {

                    $cidadePorExtenso = $value['nome'];
                    $cidade = $value['codigo'];
                }
            }
        }

        if ($cidade == '') {
            $cidade = '';
        }

        $dados['nome'] = $cidadePorExtenso;
        $dados['codigo'] = $cidade;


        return $dados;
    }

    public function converterParaCodigoBairro($bairros, $bairro) {

        $bairroExplode = explode('--', $bairro);


        $arrayBairros = array();
        $novoBairro = '';

        if ($bairro != '') {

            foreach ($bairroExplode as $key => $b) {

                foreach ($bairros as $key2 => $value) {

                    if ($b == $value['nome'] && $b) {

                        $novoBairro .= $value['codigo'] . ',';
                    }
                }
            }
        }


        if ($novoBairro == '') {
            $novoBairro = '';
        } elseif (is_numeric($novoBairro)) {

            $novoBairro = (string) $novoBairro;
        }




        return $novoBairro;
    }

    public function converterParaCodigoQuartos($quartos) {


        if ($quartos == '0-quartos') {
            $quartos = 0;
        } else if ($quartos == '1-quartos') {
            $quartos = 1;
        } else if ($quartos == '2-quartos') {
            $quartos = 2;
        } else if ($quartos == '3-quartos') {
            $quartos = 3;
        } else if ($quartos == '4-quartos') {
            $quartos = 4;
        }


        return $quartos;
    }

    public function converterParaCodigoVagas($vagas) {

        $numeroVaga = 0;

        switch ($vagas) {
            case '1-vaga-ou-mais':
                $numeroVaga = 1;
                break;
            case '2-vagas-ou-mais':
                $numeroVaga = 2;
                break;
            case '3-vagas-ou-mais':
                $numeroVaga = 3;
                break;
            case '4-vagas-ou-mais':
                $numeroVaga = 4;
                break;
            default:
                $numeroVaga = 0;
        }


        return $numeroVaga;
    }

    public function converterParaCodigoBanho($banho) {


        $numerobanho = 0;

        switch ($banho) {
            case '1-banheiro-ou-mais':
                $numerobanho = 1;
                break;
            case '2-banheiros-ou-mais':
                $numerobanho = 2;
                break;
            case '3-banheiros-ou-mais':
                $numerobanho = 3;
                break;
            case '4-banheiros-ou-mais':
                $numerobanho = 4;
                break;
            default:
                $numerobanho = 0;
        }


        return $numerobanho;
    }

    public function tirarAcentos($string) {
        return preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/", "/(ç)/", "/(Ç)/"), explode(" ", "a A e E i I o O u U n N c C"), $string);
    }

    public function amigavelURL($str) {
        return $str = strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), str_replace('/','-',$this->tirarAcentos($str))));
    }

}
