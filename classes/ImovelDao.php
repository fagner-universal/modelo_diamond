<?php

class ImovelDao extends Model {
   
    public function __construct() {
        parent::__construct();
    }
    
    
    public function inserir(Imovel $imovel){
        
        $sql = $this->db->prepare("INSERT INTO imoveis SET titulo = :titulo, descricao = :descricao,"
                . " preco = :preco, qtd = :qtd, tamanho = :tamanho, id_cidade = :id_cidade, img= :img,"
                . " lat = :lat, lng = :lng");
        
        $sql->bindValue(":titulo",$imovel->getTitulo());
        $sql->bindValue(":descricao", $imovel->getDescricao());
        $sql->bindValue(":preco", $imovel->getPerco());
        $sql->bindValue(":qtd",$imovel->getQtd);
        $sql->bindValue(":qtd_banheiro",$imovel->getQtd_banheiro());
        $sql->bindValeu(":qtd_vagas",$imovel->getQtd_vagas());
        $sql->bindValue(":tamanho",$imovel->getTamanho());
        $sql->bindValue(":id_cidade",$imovel->getTitulo());
        $sql->bindValue(":img",$imovel->getImg());
        $sql->bindValue(":lat",$imovel->getLat());
        $sql->bindValue(":lng",$imovel->getLng());
        
        $sql->execute();

    }
}
