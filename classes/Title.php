<?php

class Title {

    public $title = '';
    public $tipo;
    public $cidade;
    public $bairro;
    public $quarto;
    public $numerovagas;
    public $numerobanhos;
    public $valorde;
    public $valorate;
    public $finalidade;

    public function __construct($finalidade = '', $tipo = '', $cidade = '', $bairro = '', $quarto = '', $numerovagas = '', $numerobanhos = '', $valorde = '', $valorate = '') {

        
     
        
        $this->setFinalidade($finalidade);
        $this->tipo = $tipo;
        $this->cidade = $cidade;
        $this->bairro = $bairro;
        $this->quarto = $quarto;
        $this->numerovagas = $numerovagas;
        $this->numerobanhos = $numerobanhos;
        $this->valorde = $valorde;
        $this->valorate = $valorate;
    }

    public function setFinalidade($finalidade) {

        if ($finalidade == 1) {
            $this->finalidade = "Alugar";
        } elseif ($finalidade == 2) {
            $this->finalidade = "Comprar";
        }
    }

    public function getTitle() {

        return 0;
    }

    public function montarTitleListagem() {

        

        if ($this->tipo != '') {
            
            $this->title =    $this->formatarTexto($this->tipo);
        }

        if ($this->finalidade != "") {
            $this->title .= " para " . $this->formatarTexto($this->finalidade);
        }

        if ($this->cidade != "") {

            $this->title .= " em " . $this->formatarTexto($this->cidade);
        }

        if ($this->bairro != "") {

            $this->title .= " em " . $this->formatarTexto($this->bairro);
        }
    
        if ($this->quarto != "") {

            $this->title .= " com " . $this->formatarTexto($this->quarto);
        }
        if ($this->numerovagas != "") {

            $this->title .= " com " . $this->formatarTexto($this->numerovagas);
        }
        if ($this->numerobanhos != "") {

            $this->title .= " com " . $this->formatarTexto($this->numerobanhos);
        }
        if ($this->valorde != "") {

            $this->title .= " com " . $this->formatarTexto($this->valorate);
        }
        

        return $this->title;
    }
    
    public function formatarTexto($params){
        
        return str_replace('-', ' ', $params);
    }

}
