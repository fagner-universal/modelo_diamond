<?php

/**
 * Clase para consumir API Rest
 * Las operaciones soportadas son:
 * 	
 * 	- POST		: Agregar
 * 	- GET		: Consultar
 * 	- DELETE	: Eliminar
 * 	- PUT		: Actualizar
 * 	- PATCH		: Actualizar por parte
 * 
 * Extras
 * 	- autenticaci�n de acceso b�sica (Basic Auth)
 *  	- Conversor JSON
 *
 * @author     	Diego Valladares <dvdeveloper.com>
 * @version 	1.0
 */
function myUrlEncode($string) {

    $entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
    $replacements = array('!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]");
    return str_replace($entities, $replacements, urlencode($string));
}

class API {

    /**
     * autenticaci�n de acceso b�sica (Basic Auth)
     * Ejemplo Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==
     *
     * @param string $URL url para acceder y obtener un token
     * @param string $usuario usuario
     * @param string $password clave
     * @return JSON
     */
    static function Authentication($URL, $usuario, $password) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$usuario:$password");
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /**
     * Enviar par�metros a un servidor a trav�s del protocolo HTTP (POST).
     * Se utiliza para agregar datos en una API REST
     *
     * @param string $URL URL recurso, ejemplo: http://website.com/recurso
     * @param string $TOKEN token de autenticaci�n
     * @param array $ARRAY par�metros a env�ar
     * @return JSON
     */
    static function POST($URL, $TOKEN) {
        $datapost = '';


        $URL = myUrlEncode($URL);



//        $headers 	= array('Authorization: Bearer ' . $TOKEN);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Chave:' . $TOKEN));
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    /**
     * Consultar a un servidor a trav�s del protocolo HTTP (GET).
     * Se utiliza para consultar recursos en una API REST
     *
     * @param string $URL URL recurso, ejemplo: http://website.com/recurso/(id) no obligatorio
     * @param string $TOKEN token de autenticaci�n
     * @return JSON
     */
    static function GET($URL, $TOKEN) {
        //$headers 	= array('Authorization: Bearer ' . $TOKEN);
        //array('Chave:nGmBOUk2FpWLviDbwzs+/8Ddwozev1yfiy5WqLum0bk=')


        try {

            $ch = curl_init();

            if ($ch === false) {
                throw new Exception('Falha na inicialização');
            }

            $ch = curl_init();
            
//            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // allow redirects 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_MAXREDIRS,5);
            
            curl_setopt($ch, CURLOPT_URL, $URL);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Chave:' . $TOKEN));
            $response = curl_exec($ch);
       
//           $info = curl_getinfo($ch);
//           echo "<pre>";
//           print_r($info);
         
            
 
            if ($response === false) {
                throw new Exception(curl_error($ch), curl_errno($ch));
            }

            curl_close($ch);
         
            return $response;
            
        } catch (Exception $e) {

            trigger_error(sprintf('Curl failed with error #%d: %s', $e->getCode(), $e->getMessage()), E_USER_ERROR);
        }
    }

    /**
     * Consultar a un servidor a trav�s del protocolo HTTP (DELETE).
     * Se utiliza para eliminar recursos en una API REST
     *
     * @param string $URL URL recurso, ejemplo: http://website.com/recurso/id
     * @param string $TOKEN token de autenticaci�n
     * @return JSON
     */
    static function DELETE($URL, $TOKEN) {
        $headers = array('Authorization: Bearer ' . $TOKEN);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    /**
     * Enviar par�metros a un servidor a trav�s del protocolo HTTP (PUT).
     * Se utiliza para editar un recurso en una API REST
     *
     * @param string $URL URL recurso, ejemplo: http://website.com/recurso/id
     * @param string $TOKEN token de autenticaci�n
     * @param array $ARRAY par�metros a env�ar
     * @return JSON
     */
    static function PUT($URL, $TOKEN, $ARRAY) {
        $datapost = '';
        foreach ($ARRAY as $key => $value) {
            $datapost .= $key . "=" . $value . "&";
        }
        $headers = array('Authorization: Bearer ' . $TOKEN);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datapost);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    /**
     * Enviar par�metros a un servidor a trav�s del protocolo HTTP (PATCH).
     * Se utiliza para editar un atributo espec�fico (recurso) en una API REST
     *
     * @param string $URL URL recurso, ejemplo: http://website.com/recurso/id
     * @param string $TOKEN token de autenticaci�n
     * @param array $ARRAY parametros par�metros a env�ar
     * @return JSON
     */
    static function PATCH($URL, $TOKEN, $ARRAY) {
        $datapost = '';
        foreach ($ARRAY as $key => $value) {
            $datapost .= $key . "=" . $value . "&";
        }
        $headers = array('Authorization: Bearer ' . $TOKEN);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datapost);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    /**
     * Convertir JSON a un ARRAY
     *
     * @param json $json Formato JSON
     * @return ARRAY
     */
    static function JSON_TO_ARRAY($json) {
        return json_decode($json, true);
    }

}

//chave de teste api site
// nGmBOUk2FpWLviDbwzs+/8Ddwozev1yfiy5WqLum0bk=
// BfHQczLlJtwKiYq9zJy71a4yVJWVkhc31q0g2IOdbIA=
?>