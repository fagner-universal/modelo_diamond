<main id="main">


    <div class="jumbotron jumbotron-fluid" style="background-image: url(../assets/images/banner/call-to-action-venha.jpg);background-position: center">
        <div class="container">
            <div class="section-header">
                <h2 class="text-center" style="color:#FFF">DOCUMENTOS</h2>
                <!--                <h3 style="color:#FFF">Para locatário com carteira assinada os documentos são :</h3>-->
            </div>
        </div>
    </div>

    <section id="about" class="wow fadeInUp">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 content"></div>
                <div class="col-lg-6 content">
                    <!-- <h2>Inquilino</h2> -->
                    <ul>
                        <li><i class="ion-android-checkmark-circle"></i><span>
                                <a target="_blank" href="assets/downloads/documentos_necessarios_li.pdf" class="btn button-sm border-button-theme"><i class="fa fa-download"></i>Documentos Necessários - Locador e imóvel</a>
                            </span></li>
                        <li><i class="ion-android-checkmark-circle"></i><span>
                                <a target="_blank" href="assets/downloads/documentos_necessarios_lf.pdf" class="btn button-sm border-button-theme"><i class="fa fa-download"></i>Documentos Necessários - Locatário e Fiadores</a>
                            </span></li>
                        <li><i class="ion-android-checkmark-circle"></i><span>
                                <a target="_blank" href="assets/downloads/ficha_cadastral_pf.pdf" class="btn button-sm border-button-theme"><i class="fa fa-download"></i>Ficha Cadastral - Pessoa Física</a>
                            </span></li>

                        <li><i class="ion-android-checkmark-circle"></i><span>
                                <a target="_blank" href="assets/downloads/ficha_cadastral_pj.pdf" class="btn button-sm border-button-theme"><i class="fa fa-download"></i>Ficha Cadastral - Pessoa Jurídica</a>
                            </span></li>
                        <li><i class="ion-android-checkmark-circle"></i><span>
                                <a target="_blank" href="assets/downloads/modalidades_garantia.pdf" class="btn button-sm border-button-theme"><i class="fa fa-download"></i>Modalidades de Garantia</a>
                            </span></li>
                        <li><i class="ion-android-checkmark-circle"></i><span>
                                <a target="_blank" href="assets/downloads/guia_desocupacao.pdf" class="btn button-sm border-button-theme"><i class="fa fa-download"></i>Guia da desocupação do Imóvel</a>
                            </span></li>
                    </ul>
                </div>
                <div class="col-lg-3 content"></div>
                <!-- <div class="col-lg-6 content">
                    <h2>Fiador</h2>
                    <ul>
                        <li><i class="ion-android-checkmark-circle"></i><span>
                                <a target="_blank" href="#" class="btn button-sm border-button-theme"><i class="fa fa-download"></i>Ficha Cadastral - Fiador</a>
                            </span></li>

                        <li><i class="ion-android-checkmark-circle"></i><span>
                                <a target="_blank" href="#" class="btn button-sm border-button-theme"><i class="fa fa-download"></i>Documentos Necessários - Pessoa Fisíca</a>
                            </span></li>

                        <li><i class="ion-android-checkmark-circle"></i><span>
                                <a target="_blank" href="#" class="btn button-sm border-button-theme"><i class="fa fa-download"></i>Documentos Necessários - Pessoa Jurídica</a>
                            </span></li>
                    </ul>
                </div> -->

                <!-- <div class="col-lg-6 content">
                    <h2>Seguro Fiança</h2>
                    <ul>
                        <li><i class="ion-android-checkmark-circle"></i><span>
                                <a target="_blank" href="#" class="btn button-sm border-button-theme"><i class="fa fa-download"></i>Documentos Necessários</a>
                            </span></li>

                        <li><i class="ion-android-checkmark-circle"></i><span>
                                <a target="_blank" href="#" class="btn button-sm border-button-theme"><i class="fa fa-download"></i>Ficha Cadastral - Locação Residêncial</a>
                            </span></li>

                        <li><i class="ion-android-checkmark-circle"></i><span>
                                <a target="_blank" href="#" class="btn button-sm border-button-theme"><i class="fa fa-download"></i>Ficha Cadastral - Locação Comercial</a>
                            </span></li>
                    </ul>
                </div> -->
                <!-- <div class="col-lg-6 content">
                    <h2>Título de Capitalização </h2>
                    <ul>
                        <li><i class="ion-android-checkmark-circle"></i><span>
                                <a target="_blank" href="#" class="btn button-sm border-button-theme"><i class="fa fa-download"></i>Ficha Cadastral</a>
                            </span></li>
                    </ul>
                </div> -->

            </div>

        </div>

        </div>
    </section><!-- #about -->

    <!--INCLUDE AREA DE CONTATOS-->
    <?php include 'includes/contatos.php'; ?>

</main>