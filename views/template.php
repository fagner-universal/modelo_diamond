<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">

    <title><?php echo $viewData['titlePagina']; ?></title>

    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta property="og:type" content="website">
    <meta property="og:image" content="<?php echo BASE_URL ?>assets/img/logos/logo.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="672">
    <meta property="og:image:height" content="465">
    <meta property="og:url" content="<?php echo BASE_URL ?>">
    <meta property="og:title" content="<?php echo META_TITLE ?>">
    <meta property="og:site_name" content="<?php echo TITULO_AUXILIAR ?>">
    <meta property="og:description" content="<?php echo DESCRIPTION ?>">

    <meta name="title" content="<?php echo META_TITLE ?>"/>
    <meta name="description" content="<?php echo DESCRIPTION ?>"/>
    <meta name="keywords" content="<?php echo KEYWORDS ?>"/>
    <meta name="robots" content=index />

    
    <!--        <link href="img/favicon.png" rel="icon">
                <link href="img/apple-touch-icon.png" rel="apple-touch-icon">-->

    <link rel="shortcut icon" href="<?php echo BASE_URL ?>assets/images/logo/favicon.ico" type="image/x-icon" />

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet" />

    <!-- Bootstrap CSS File -->
    <link href="<?php echo BASE_URL; ?>assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Libraries CSS Files -->
    <link href="<?php echo BASE_URL; ?>assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_URL; ?>assets/lib/animate/animate.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_URL; ?>assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_URL; ?>assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_URL; ?>assets/lib/magnific-popup/magnific-popup.css" rel="stylesheet" />
    <link href="<?php echo BASE_URL; ?>assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/slick-theme.css" />
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/slick.css" />

    <!--ICONES-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous" />
    <!--<link rel="stylesheet" type="text/css" href="<?php //echo BASE_URL  
                                                        ?>assets/fonts/font-awesome/css/font-awesome.min.css">-->

    <!-- Main Stylesheet File -->
    <link href="<?php echo BASE_URL; ?>assets/css/style.css" rel="stylesheet" />

    <!-- =======================================================
          Theme Name: Reveal
          Theme URL: https://bootstrapmade.com/reveal-bootstrap-corporate-template/
          Author: BootstrapMade.com
          License: https://bootstrapmade.com/license/
        ======================================================= -->

    <!--ICONES GOOGLE-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />

    <!-- import  Clustering de marcadores -->
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>

    <!-- <link href="https://fonts.googleapis.com/css?family=Dancing+Script|Tangerine" rel="stylesheet"> -->

    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/bootstrap/css/bootstrap.min.css" />

    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/style1.css" />

    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/style.css" />
    <?php include('includes/scripts_top.php') ?>

</head>

<body id="body">

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:#00d45e;color:#FFF">
                    <h5 class="modal-title" id="exampleModalLabel"><i class="fab fa-whatsapp"></i> Iniciar conversa no WhatsApp</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mensagem</label>
                            <input id="text-wats" type="text" class="form-control" id="" aria-describedby="emailHelp" placeholder="">
                            <!--<small id="emailHelp" class="form-text text-muted">Mensagem.</small>-->
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" style="background-color:#00d45e;color:#FFF" data-dismiss="modal">Fechar</button>
                    <button id="watsapp" style="background-color:#00d45e;color:#FFF" type="button" class="btn btn-primary">Enviar</button>
                </div>
            </div>
        </div>
    </div>

    <?php include('includes/topo.php') ?>
    <?php $this->loadViewInTemplate($viewName, $viewData); ?>
    <!--==========================
      Top Bar
    ============================-->

    <!--        <section id="topbar" class="d-none d-lg-block ">
            <div class="container clearfix">
                <div class="contact-info float-left">
                    <i class="far fa-envelope-open"></i> <a href="mailto:contact@example.com">contato@jomarimobiliaria.com.br</a>
                    <i class="fa fa-phone"></i>(31) 3226-2132
                </div>
                <div class="social-links float-right">
                    <a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
                    <a href="#" class="facebook"><i class="fab fa-facebook-square"></i></a>
                    <a href="#" class="instagram"><i class="fab fa-twitter"></i></a>
                    <a href="#" class="google-plus"><i class="fab fa-instagram"></i></i></a>
                    <a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a>
                </div>
            </div>
        </section>-->

    <!--==========================
      Header
    ============================-->
    <!--        <header id="header">-->
    <!--            <div class="container">-->
    <!--                <div id="logo" class="pull-left" style="color:#00adef">-->
    <!--                    <a href="--><?php //echo BASE_URL  
                                        ?>
    <!--"><img class="img-logo" src="--><?php //echo BASE_URL  
                                        ?>
    <!--assets/images/logo/logo.png" alt=""></a>-->
    <!--                </div>-->
    <!---->
    <!--                <nav id="nav-menu-container">-->
    <!--                    <ul class="nav-menu">-->
    <!--                        <li class="menu-active"><a href="--><?php //echo BASE_URL  
                                                                    ?>
    <!--">Home</a></li>-->
    <!---->
    <!--                        <li class="menu-has-children"><a href="">Buscar Imóvel</a>-->
    <!--                            <ul>-->
    <!--                                <li><a href="--><?php //echo BASE_URL  
                                                        ?>
    <!--aluguel/imoveis/todas-as-cidades/todos-os-bairros/1-quartos/1-vaga-ou-mais/1-banheiro-ou-mais/?pagina=1&valorminimo=0&valormaximo=0">Buscar Imóveis</a></li>-->
    <!--                                <li><a href="--><?php //echo BASE_URL  
                                                        ?>
    <!--busca">Buscar pelo Mapa</a></li>-->
    <!--                            </ul>-->
    <!--                        </li>-->
    <!---->
    <!--                        <li><a href="--><?php //echo BASE_URL  
                                                ?>
    <!--sobre">Institucional</a></li>-->
    <!--                        <li><a href="--><?php //echo BASE_URL  
                                                ?>
    <!--equipe">Nossa equipe</a></li>-->
    <!--                        <li><a href="http://www.portalunsoft.com.br/area-do-cliente/jomar" target="blank">Área do Cliente</a></li>-->
    <!--                        <li><a href="--><?php //echo BASE_URL  
                                                ?>
    <!--anunciar">Anuncie seu imóvel</a></li>-->
    <!--                        <li><a href="--><?php //echo BASE_URL  
                                                ?>
    <!--contatos/faleConosco">Fale Conosco</a>-->
    <!--                        </li>-->
    <!--                    </ul>-->
    <!--                </nav><!-- #nav-menu-container -->
    <!--            </div>-->
    <!--        </header>-->
    <!-- #header -->

    

    <?php include('includes/whatsapp.php'); ?>
    <?php include('includes/footer.php'); ?>



    <!-----------------------------MODAL PARA EXIBIR BUSCA POR CÓDIGO---------------------------------------- -->
    <div class="modal fade modal-fullscreen force-fullscreen " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="overflow:auto">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center tituloModal">Imóveis encontrados</h4>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <!--                        <button id="fecharModal" type="button" class="btn btn-danger">Fechar</button>-->
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 conteudoModal">
                            </div>
                            <div class="col-md-4 ml-auto">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!--<button id="fecharModal" type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                    <!--                <button id="fecharModal" type="button" class="btn btn-primary">Fechar</button>-->
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!--   ---------------------------MODAL PARA EXIBIR BUSCA POR CÓDIGO---------------------------------------- -->

    <script src="<?php echo BASE_URL; ?>assets/js/base_url.js"></script>


    <!--LINK PARA TOPO DA PAGINA;-->
    <!--<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>-->

    <!-- JavaScript Libraries -->
    <script src="<?php echo BASE_URL; ?>assets/lib/jquery/jquery.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/jquery/jquery-migrate.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/easing/easing.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/superfish/hoverIntent.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/superfish/superfish.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/wow/wow.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/owlcarousel/owl.carousel.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/magnific-popup/magnific-popup.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/sticky/sticky.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/mask/jquery.mask.js"></script>

    <script src="<?php echo BASE_URL; ?>assets/lib/slick-1.8.1/slick/slick.min.js"></script>
    
    <script src="<?php echo BASE_URL; ?>assets/js/home/destaques.js"></script>


    <!-- Contact Form JavaScript File -->
    <script src="<?php echo BASE_URL; ?>assets/js/contactform/contactform.js"></script>

    <!-- importação da api google maps -->
    <!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBrMj9OwhfWmyB1zEfEY2zksiTAtqBkrZg" type="text/javascript"></script> -->

    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->

    <script src="<?php echo BASE_URL; ?>/assets/bootstrap/js/bootstrap.js"></script>


    <!--import filtro por movimentação do mapa-->
    <!--<script src="<?php //echo BASE_URL;               
                        ?>assets/api-maps/filtro-mapa.js"></script>-->

    <!-- Template Main Javascript File -->
    <script src="<?php echo BASE_URL; ?>assets/js/main.js"></script>


    <!--BUSCAR DE IMOVEIS-->
    <!--<script src="<?php //echo BASE_URL;  
                        ?>assets/js/busca/busca.js"></script>-->

    <script src="<?php echo BASE_URL; ?>assets/js/home/busca.js"></script>

    <!--auto complite do input busca por endereço-->
    <!--<script src="<?php //echo BASE_URL;              
                        ?>assets/api-maps/auto-complete-mapa.js"></script>;-->
    <!--<script src="<?php // BASE_URL;               
                        ?>assets/js/imoveis-em-destaque.js"></script>-->


    <!--FORMULARIO CONTATOS-->
    <script src="<?php echo BASE_URL; ?>assets/js/contatos/contato.js"></script>

    <!--FORMULARIO Imoveis-->
    <script src="<?php echo BASE_URL; ?>assets/js/anunciar/anunciar.js"></script>

    <!--FORMULARIO HOME-->
    <script src="<?php echo BASE_URL; ?>assets/js/home/form_busca_home.js"></script>

  
    <!--MODAL DE WATS APP-->
    <script src="<?php echo BASE_URL; ?>assets/js/watsapp/watsapp.js"></script>

    <!--SCRIPT FORMULARIO DE LEAD-->
    <script src="<?php echo BASE_URL; ?>assets/js/formlead/leads.js"></script>

</body>

<!--//SCROOL DETALHES-->
<script src="<?php echo BASE_URL; ?>assets/js/detalhes/detalhes.js"></script>


<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBrMj9OwhfWmyB1zEfEY2zksiTAtqBkrZg&callback=initMap" type="text/javascript"></script>


<script src="<?php echo BASE_URL; ?>assets/js/home/mapa.js"></script>

<script>
    $(document).ready(function() {
        try {

            var url_direct = window.location.href.slice(window.location.href.indexOf('=') + 1);

        } catch (e) {


        }

        if (url_direct == 'aluguel_destaque') {

            $('html, body').animate({
                scrollTop: $('#aluguel_destaque').offset().top
            }, 'slow');


        } else if (url_direct == 'venda_destaque') {

            $('html, body').animate({
                scrollTop: $('#imoveis_venda').offset().top
            }, 'slow');


        } else if (url_direct == 'lancamento_destaque') {

            $('html, body').animate({
                scrollTop: $('#imoveis_lancament').offset().top
            }, 'slow');


        } else {

        }

    });


    //        $('#fecharModal').click(function(){
    //
    //            $('#myModal').toogkke
    //
    //        });


    $('.carousel-item').click(function() {

        setTimeout(function() {
            $('.mfp-arrow-right').trigger('click');
        }, 1000)


    })

    $('#divulgacao').modal('show');
</script>
<script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>
    <script>
        window.cookieconsent.initialise({
            "palette": {
                "popup": {
                    "background": "#eaf7f7",
                    "text": "#5c7291"
                },
                "button": {
                    "background": "var(--cor-primaria)",
                    "text": "#ffffff"
                }
            },
            "theme": "classic",
            "content": {
                "message": "Utilizamos cookies para oferecer melhor experiência, melhorar o desempenho, analisar como você interage em nosso site e personalizar conteúdo. Ao utilizar este site, você concorda com o uso de cookies.",
                "dismiss": "Aceito"
            }
        });
    </script>
    <?php include('includes/scripts_bottom.php') ?>
</html>