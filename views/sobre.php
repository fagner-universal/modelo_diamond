<main id="main">


    <!--      <section id="testimonials" class="wow fadeInUp" style="background-color:#033f6f;">-->
    <!--        <div class="container">-->
    <!--            <div class="row">-->
    <!--                <iframe width="100%" height="700" src="https://www.youtube.com/embed/n96gL4CFfZI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </section>-->

    <!--==========================
      About Section
    ============================-->
    <section id="about" class="wow fadeInUp">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 content" style="text-align: justify">
                    <h3 class="text-sobre-cinza">
                        Ajudamos imobiliárias tradicionais a se transformarem em imobiliárias digitais e se tornarem líderes nas suas regiões
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 content" style="text-align: justify">
                    <h2 class="text-center">Não é o que fazemos, mas como fazemos!!</h2>
                    <p>
                    Pensamento orientado a dados, sem achismo e com automações. Não é simplesmente o que fazemos que gera bons resultados. Tem a ver com a maneira como pensamos e como fazemos as coisas.

                    </p>
                    <p></p>
                </div>
                <div class="col-lg-6 content" style="text-align: justify">
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="assets/images/banner/Semana3.jpg" alt="Imagem Sobre a empresa">
                            </div>
                            <!-- <div class="carousel-item">
                                <img class="d-block w-100" src="assets/images/imoveis/sobre.jpg" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="assets/images/imoveis/sobre.jpg" alt="Third slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="assets/images/imoveis/sobre.jpg" alt="Third slide">
                            </div> -->
                        </div>
                        <!-- <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a> -->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 content">
                    <p style="margin-top:30px;text-align: justify;">
                    is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset 
                    <p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever si</p>
                    <p>E você pensa que acaba por aí?! Claro que não!</p>
                    <h2 style="margin-top:20px;" class="text-center">MISSÃO</h2>
                    <p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever si</p>
                    <h2 class="text-center">VISÃO</h2>
                    <p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever si</p>
                    <h2 class="text-center">VALORES</h2>
                    <ul>
                        <li><i class="ion-android-checkmark-circle"></i><b>Ética:</b>  is simply dummy text of the printing and typesetting industry. Lorem Ipsum has.</li>
                        <li><i class="ion-android-checkmark-circle"></i><b>Satisfação do cliente:</b> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has.</li>
                        <li><i class="ion-android-checkmark-circle"></i><b>Respeito aos colaboradores e parceiros:</b> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has.</li>
                        <li><i class="ion-android-checkmark-circle"></i><b>Responsabilidade social e ambiental:</b> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has.</li>
                    </ul>
                    <!-- <h2 class="text-center">Conheça nossa equipe</h2>
                    <p>A equipe da Forthe é formada por profissionais especializados no mercado imobiliário de Cascavel (PR). Acompanhamos todas as novidades do setor e baseamos nosso trabalho nas demandas e nas necessidades apontadas por nossos clientes. Para a nossa equipe, atender você é a nossa prioridade. Venha alugar, comprar ou vender o seu imóvel em Cascavel com a Forthe - aqui você encontra um novo jeito de olhar para o mercado imobiliário.</p> -->
                </div>
            </div>

        </div>
    </section>



    <!-- <section id="about" class="wow fadeInUp">
        <div class="container">
            <div class="row">

                <div class="col-lg-4 content">
                    <h2>Missão</h2> -->
    <!--<h4>Pessoa Física que deseja alugar um imóvel através de fiador os documentos são :</h4>-->
    <!-- <ul>
                        <li><i class="ion-android-checkmark-circle"></i>Garantir bons negócios aos nossos clientes, através da compra, venda e locação de imóveis de alta qualidade.</li>
                    </ul> -->
    <!--<a href="#contact" class="btn btn-primary btBuscar">Cadastrar imóvel</a>-->

    <!-- </div>
                <div class="col-lg-4 content">
                    <h2>Visão</h2> -->
    <!--<h4>Pessoa Física que deseja alugar um imóvel através de fiador os documentos são :</h4>-->
    <!-- <ul>
                        <li><i class="ion-android-checkmark-circle"></i>Ser referência entre as imobiliárias de Belo Horizonte, principalmente no Bairro Buritis, e ser reconhecida pela excelência dos serviços prestados e atenção dedicada aos nossos clientes.</li>
                    </ul> -->
    <!--<a href="#contact" class="btn btn-primary btBuscar">Cadastrar imóvel</a>-->

    <!-- </div>
                <div class="col-lg-4 content">
                    <h2>Valores</h2> -->
    <!--<h4>Pessoa Física que deseja alugar um imóvel através de fiador os documentos são :</h4>-->
    <!-- <ul>
                        <li><i class="ion-android-checkmark-circle"></i>Ética: transparência, agilidade e integridade nos negócios realizados pela nossa empresa.</li>
                        <li><i class="ion-android-checkmark-circle"></i>Satisfação do cliente: compromisso em atender todas as necessidades de nossos clientes, com cordialidade e eficiência..</li>
                        <li><i class="ion-android-checkmark-circle"></i>Respeito aos colaboradores e parceiros: valorização de todos os nossos colaboradores e parceiros, através de um relacionamento sólido e duradouro.</li>
                        <li><i class="ion-android-checkmark-circle"></i>Responsabilidade social e ambiental: crescer de forma justa, através de uma sustentabilidade social, econômica e ambiental..</li>

                    </ul> -->
    <!--<a href="#contact" class="btn btn-primary btBuscar">Cadastrar imóvel</a>-->

    <!-- </div>
            </div>
        </div>
    </section> -->





    <!--    <section id="about" class="wow fadeInUp">-->
    <!--        <div class="container-fluid">-->
    <!--   -->
    <!--            <div class="row corpo-historia-azul">-->
    <!--                <div class="col-12 col-sm-12 col-md-12 col-lg-6 about-img" >-->
    <!--                    <img class="rounded mx-auto d-block"  src="--><?php //echo BASE_URL        
                                                                            ?>
    <!--assets/images/nossa-historia/historia1.jpg" alt="">-->
    <!--                </div>-->
    <!--                <div class="col-12 col-sm-12 col-md-12 col-lg-6" style="padding: 25px; text-align: justify;" >-->
    <!--                    <p class="text-sobre">-->
    <!--                        Na data de 01/Junho/1985 Iniciou suas atividades a JOMAR IMOBILIÁRIA E LOCADORA DE IMÓVEIS LTDA,empresa pioneira do setor imobiliário da cidade, -->
    <!--                        adotando o slogan de “uma empresa de confiança”. No início o trabalho foi muito difícil, devido ao desconhecimento dos proprietários de imóveis,-->
    <!--                        das pessoas que desejavam alugar, do preenchimento dos formulários, da necessidade de fiadores nos contratos de locações, enfim, -->
    <!--                        foi um árduo trabalho de conscientização da população, para os benefícios e segurança proporcionados por uma empresa imobiliária.-->
    <!--                    </p>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--            <div class="row corpo-historia-branco" >-->
    <!--                <div class="col-12 col-sm-12 col-md-12 col-lg-6" style="padding: 25px; text-align: justify;" >-->
    <!--                    <p class="text-sobre-cinza">-->
    <!--                        No transcorrer dos anos, com o aprimoramento e conhecimentos da população, a JOMAR IMOBILIÁRIA tornou-se uma referência do setor imobiliário,-->
    <!--                        através de uma atuação honesta, respeitosa, cumpridora de suas obrigações, tradicional, zelando pela tranquilidade e segurança de seus clientes.-->
    <!--                        Com a mudança do mercado imobiliário e a evolução tecnológica, José Maria Scaldini Garcia trouxe para a empresa seu filho Vinícius Sousa Scaldini Garcia ,recém-formado e cheio de ideias, buscou o que havia de mais novo no mercado imobiliário para que a empresa familiar continuasse desenvolvendo. -->
    <!--                    </p>-->
    <!--                </div>-->
    <!---->
    <!--                <div class="col-12 col-sm-12 col-md-12 col-lg-6 about-img" >-->
    <!--                    <img class="rounded mx-auto d-block"  src="--><?php //echo BASE_URL        
                                                                            ?>
    <!--assets/images/nossa-historia/historia2.jpg" alt="">-->
    <!--                </div>-->
    <!--            </div>-->
    <!--            <div class="row corpo-historia-azul">-->
    <!--                <div class="col-12 col-sm-12 col-md-12 col-lg-6 about-img" >-->
    <!--                    <img class="rounded mx-auto d-block"  src="--><?php //echo BASE_URL        
                                                                            ?>
    <!--assets/images/nossa-historia/historia3.jpg" alt="">-->
    <!--                </div>-->
    <!---->
    <!--                <div class="col-12 col-sm-12 col-md-6 col-lg-6" style="padding: 25px; text-align: justify;">-->
    <!--                    <p class="text-sobre" >-->
    <!--                        Sempre inovando e saindo na frente, hoje a JOMAR IMOBILIÁRIA conta com três processos de locação para melhor atender seus clientes, sendo, -->
    <!--                        através de fiadores, seguro fiança ou título de capitalização. A empresa atua em diversas redes sociais, sendo elas,<span class="text-span"> Facebook, Instagram, Skype e WhatsApp.</span>-->
    <!--                        No ano de 2018, adotou um software de sistema de última geração, visando melhorar seus resultados, gerando mais satisfação de seus clientes e possivelmente concluindo mais negócios.   </p>-->
    <!---->
    <!---->
    <!--                </div>-->
    <!--            </div>-->
    <!---->
    <!---->
    <!--            <div class="row corpo-historia-branco">-->
    <!---->
    <!--                <div class="col-12 col-sm-12 col-md-6 col-lg-6" style="padding: 25px; text-align: justify;">-->
    <!--                    <p class="text-sobre-cinza">-->
    <!--                        E no final deste mesmo ano, a JOMAR IMOBILIÁRIA lançou um dos melhores e maiores sites do mercado imobiliário da região,-->
    <!--                        onde seus clientes conseguem acessar qualquer informação sem sair de casa, gerando mais conforto e comodidade para seus clientes nessa nova era digital.-->
    <!--                    </p>-->
    <!---->
    <!--                </div>-->
    <!--                <div class="col-12 col-sm-12 col-md-12 col-lg-6 about-img" >-->
    <!--                    <img class="rounded mx-auto d-block"  src="--><?php //echo BASE_URL        
                                                                            ?>
    <!--assets/images/nossa-historia/historia4.jpg" alt="">-->
    <!--                </div>-->
    <!---->
    <!--            </div>-->
    <!--        </div>-->
    <!--        </div>-->
    <!--    </section>-->

    <!-- ==========================
          Our Portfolio Section
        ============================-->
    <!--    <section id="portfolio" class="wow fadeInUp">-->
    <!--        <div class="container">-->
    <!--            <div class="section-header">-->
    <!--                <h2 class="text-center">Veja nossa estrutura</h2>-->
    <!---->
    <!--            </div>-->
    <!--        </div>-->
    <!---->
    <!--        <div class="container-fluid">-->
    <!--            <div class="row no-gutters">-->
    <!---->
    <!--                <div class="col-lg-3 col-md-4">-->
    <!--                    <div class="portfolio-item wow fadeInUp">-->
    <!--                        <a href="--><?php //echo BASE_URL        
                                            ?>
    <!--assets/images/estrutura/1.jpg" class="portfolio-popup">-->
    <!--                            <img src="--><?php //echo BASE_URL        
                                                    ?>
    <!--assets/images/estrutura/1.jpg" alt="">-->
    <!--                            <div class="portfolio-overlay">-->
    <!--                                <!--<div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Item 1</h2></div>-->
    <!--                            </div>-->
    <!--                        </a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!---->
    <!--                <div class="col-lg-3 col-md-4">-->
    <!--                    <div class="portfolio-item wow fadeInUp">-->
    <!--                        <a href="--><?php //echo BASE_URL        
                                            ?>
    <!--assets/images/estrutura/2.jpg" class="portfolio-popup">-->
    <!--                            <img src="--><?php //echo BASE_URL        
                                                    ?>
    <!--assets/images/estrutura/2.jpg" alt="">-->
    <!--                            <div class="portfolio-overlay">-->
    <!--                                <!--<div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Item 2</h2></div>-->
    <!--                            </div>-->
    <!--                        </a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!---->
    <!--                <div class="col-lg-3 col-md-4">-->
    <!--                    <div class="portfolio-item wow fadeInUp">-->
    <!--                        <a href="--><?php //echo BASE_URL        
                                            ?>
    <!--assets/images/estrutura/3.jpg" class="portfolio-popup">-->
    <!--                            <img src="--><?php //echo BASE_URL        
                                                    ?>
    <!--assets/images/estrutura/3.jpg" alt="">-->
    <!--                            <div class="portfolio-overlay">-->
    <!--                                <!--<div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Item 3</h2></div>-->
    <!--                            </div>-->
    <!--                        </a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!---->
    <!--                <div class="col-lg-3 col-md-4">-->
    <!--                    <div class="portfolio-item wow fadeInUp">-->
    <!--                        <a href="--><?php //echo BASE_URL        
                                            ?>
    <!--assets/images/estrutura/4.jpg" class="portfolio-popup">-->
    <!--                            <img src="--><?php //echo BASE_URL        
                                                    ?>
    <!--assets/images/estrutura/4.jpg" alt="">-->
    <!--                            <div class="portfolio-overlay">-->
    <!--                                <!--<div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Item 4</h2></div>-->
    <!--                            </div>-->
    <!--                        </a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!---->
    <!--                <div class="col-lg-3 col-md-4">-->
    <!--                    <div class="portfolio-item wow fadeInUp">-->
    <!--                        <a href="--><?php //echo BASE_URL        
                                            ?>
    <!--assets/images/estrutura/5.jpg" class="portfolio-popup">-->
    <!--                            <img src="--><?php //echo BASE_URL        
                                                    ?>
    <!--assets/images/estrutura/5.jpg" alt="">-->
    <!--                            <div class="portfolio-overlay">-->
    <!--                                <!--<div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Item 5</h2></div>-->
    <!--                            </div>-->
    <!--                        </a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="col-lg-3 col-md-4">-->
    <!--                    <div class="portfolio-item wow fadeInUp">-->
    <!--                        <a href="--><?php //echo BASE_URL        
                                            ?>
    <!--assets/images/estrutura/6.jpg" class="portfolio-popup">-->
    <!--                            <img src="--><?php //echo BASE_URL        
                                                    ?>
    <!--assets/images/estrutura/6.jpg" alt="">-->
    <!--                            <div class="portfolio-overlay">-->
    <!--                                <!--<div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Item 6</h2></div>-->
    <!--                            </div>-->
    <!--                        </a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="col-lg-3 col-md-4">-->
    <!--                    <div class="portfolio-item wow fadeInUp">-->
    <!--                        <a href="--><?php //echo BASE_URL        
                                            ?>
    <!--assets/images/estrutura/7.jpg" class="portfolio-popup">-->
    <!--                            <img src="--><?php //echo BASE_URL        
                                                    ?>
    <!--assets/images/estrutura/7.jpg" alt="">-->
    <!--                            <div class="portfolio-overlay">-->
    <!--                                <!--<div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Item 7</h2></div>;-->
    <!--                            </div>-->
    <!--                        </a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="col-lg-3 col-md-4">-->
    <!--                    <div class="portfolio-item wow fadeInUp">-->
    <!--                        <a href="--><?php //echo BASE_URL        
                                            ?>
    <!--aassets/images/estrutura/8.jpg" class="portfolio-popup">-->
    <!--                            <img src="--><?php //echo BASE_URL        
                                                    ?>
    <!--assets/images/estrutura/8.jpg" alt="">-->
    <!--                            <div class="portfolio-overlay">-->
    <!--                                <!--<div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Item 8</h2></div>-->
    <!--                            </div>-->
    <!--                        </a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="col-lg-3 col-md-4">-->
    <!--                    <div class="portfolio-item wow fadeInUp">-->
    <!--                        <a href="--><?php //echo BASE_URL        
                                            ?>
    <!--aassets/images/estrutura/9.jpg" class="portfolio-popup">-->
    <!--                            <img src="--><?php //echo BASE_URL        
                                                    ?>
    <!--assets/images/estrutura/9.jpg" alt="">-->
    <!--                            <div class="portfolio-overlay">-->
    <!--                                <!--<div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Item 8</h2></div>-->
    <!--                            </div>-->
    <!--                        </a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="col-lg-3 col-md-4">-->
    <!--                    <div class="portfolio-item wow fadeInUp">-->
    <!--                        <a href="--><?php //echo BASE_URL        
                                            ?>
    <!--aassets/images/estrutura/10.jpg" class="portfolio-popup">-->
    <!--                            <img src="--><?php //echo BASE_URL        
                                                    ?>
    <!--assets/images/estrutura/10.jpg" alt="">-->
    <!--                            <div class="portfolio-overlay">-->
    <!--                                <!--<div class="portfolio-info"><h2 class="wow fadeInUp">Portfolio Item 8</h2></div>-->
    <!--                            </div>-->
    <!--                        </a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </section>-->
    <!---->




    <!--==========================
      PORQUE A JOMAR IMOVEIS
    ============================-->
    <!--    <section id="testimonials" class="wow fadeInUp">-->
    <!--        <div class="container">-->
    <!--            <div class="section-header">-->
    <!--                <h2 class="text-center">POR QUE A --><?php //echo NOME_IMOBILIARIA        
                                                                ?>
    <!-- ?</h2>-->
    <!--            </div>-->
    <!--            <div class="owl-carousel testimonials-carousel case-suesso">-->
    <!---->
    <!--                --><?php //foreach ($depoimentos as $key => $depoimento) :        
                            ?>
    <!---->
    <!--                    <div class="testimonial-item">-->
    <!--                        <img src="--><?php //echo BASE_URL        
                                                ?>
    <!--assets/images/depoimentos/--><?php //echo $depoimento['img']        
                                        ?>
    <!--" class="testimonial-img" alt="">-->
    <!--                        <h3>--><?php //echo $depoimento['nome']        
                                        ?>
    <!--</h3>-->
    <!--                        <p>-->
    <!--                            <img src="--><?php //echo BASE_URL        
                                                    ?>
    <!--assets/images/quote-sign-left.png" class="quote-sign-left" alt="">-->
    <!--                            --><?php //echo $depoimento['texto']        
                                        ?>
    <!--                            <img src="--><?php //echo BASE_URL        
                                                    ?>
    <!--assets/images/quote-sign-right.png" class="quote-sign-right" alt="">-->
    <!--                        </p>-->
    <!---->
    <!--                    </div>-->
    <!--                --><?php //endforeach;        
                            ?>
    <!--            </div>-->
    <!--        </div>-->
    <!--    </section>-->

    <?php include 'includes/contatos.php'; ?>
</main>