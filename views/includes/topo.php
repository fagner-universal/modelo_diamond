<!--==========================
      Top Bar
    ============================-->

<section id="topbar" class="d-none d-lg-block ">
    <div class="container clearfix">
        <div class="contact-info float-left">
            <i class="far fa-envelope-open"></i> <a href="mailto:<?php echo EMAIL ?>" target="_blank"><?php echo EMAIL ?></a>
            <i class="fa fa-phone"></i><a href="tel:<?php echo TELEFONE_FIXO_SCRIPT ?>" target="_blank"><?php echo TELEFONE_FIXO ?></a>
            <i class="fab fa-whatsapp"></i><a href="https://api.whatsapp.com/send?phone=55<?php echo CELULAR_SCRIPT ?>&text=Olá, Gostaria de informações sobre um imóvel... " target="_blank"><?php echo CELULAR ?></a>
            <a style="border-left: 1px solid #e9e9e9;padding-left: 20px;margin-left: 20px;" href="">CRECI: <?php echo CRECI ?></a>
        </div>
        <div class="social-links float-right">
            <?php if (TWITTER != "" ) { ?>
            <a href="<?php echo TWITTER  ?>" class="twitter"><i class="fab fa-twitter"></i></a>
            <?php } ?>
            <?php if (FACEBOOK != "" ) { ?>
            <a href="<?php echo FACEBOOK ?>" target="_blank" class="facebook"><i class="fab fa-facebook-square"></i></a>
            <?php } ?>
            <?php if (INSTAGRAM != "" ) { ?>
            <a href="<?php echo INSTAGRAM ?>" target="_blank" class="google-plus"><i class="fab fa-instagram"></i></a>
            <?php } ?>
            <?php if (YOUTUBE != "" ) { ?>
            <a href="<?php echo YOUTUBE ?>" target="_blank" class="google-plus"><i class="fab fa-youtube"></i></a>
            <?php } ?>
            <?php if (LINKEDIN != "" ) { ?>
            <a href="<?php echo LINKEDIN ?>" target="_blank" class="linkedin"><i class="fab fa-linkedin"></i></a>
            <?php } ?>
            <!--<a href="<?php //echo LINKEDIN ?>" target="_blank" class="linkedin"><i class="fab fa-linkedin-in"></i></a>-->
        </div>
    </div>
</section>
<header id="header">
    <div class="container">
        <div id="logo" class="pull-left" style="color:#00adef">
            <a href="<?php echo BASE_URL ?>" class="logo"><img  src="<?php echo BASE_URL ?>assets/images/logo/logo.png" alt=""></a>
        </div>
        <nav id="nav-menu-container">
            <ul class="nav-menu">
                <li class="menu-active"><a href="<?php echo BASE_URL ?>">Home</a></li>

                <li class="menu-has-children"><a href="#">Buscar Imóvel</a>
                    <ul>
                        <li><a href="<?php echo BASE_URL ?>aluguel/imoveis/todas-as-cidades/todos-os-bairros/0-quartos/0-suite-ou-mais/0-vaga-ou-mais/0-banheiro-ou-mais/sem-portaria-24horas/sem-area-lazer/sem-dce/sem-mobilia/sem-area-privativa/sem-area-servico/sem-box-despejo/sem-circuito-tv/?valorminimo=0&valormaximo=0&pagina=1">Buscar Imóveis</a></li>
                        <li><a href="<?php echo BASE_URL ?>busca">Buscar pelo Mapa</a></li>
                    </ul>
                </li>
                <li class="menu-has-children"><a href="#">Sobre</a>
                    <ul>
                        <li><a href="<?php echo BASE_URL ?>sobre">Institucional</a></li>
                        <li><a href="<?php echo BASE_URL ?>documentos">Documentos</a></li>
                    </ul>
                </li>

                <!-- <li><a href="<?php echo BASE_URL ?>sobre">Institucional</a></li> -->
                <?php if (AREA_CLIENTE != "" ) { ?>
                <li><a target="_blank" href="<?php echo AREA_CLIENTE ?>">Área do Cliente</a></li>
                <?php } ?>
                <!--                <li><a href="--><?php //echo BASE_URL  ?><!--equipe">Nossa equipe</a></li>-->
<!--                <li><a href="--><?php //echo AREA_CLIENTE ?><!--" target="blank">Área do Cliente</a></li>-->

                <!-- <li class="menu-has-children"><a href="#">Área do Cliente</a>
                    <ul>
                        <li><a href="https://adm056709.superlogica.net/clients/areadofornecedor" target="_blank">Locador / Proprietário </a></li>
                        <li><a href="https://adm056709.superlogica.net/clients/areadocliente" target="_blank">Locatario/ Inquilino </a></li>

                    </ul>
                </li> -->
                <li><a href="<?php echo BASE_URL ?>anunciar">Anuncie seu imóvel</a></li>
                <li><a href="<?php echo BASE_URL ?>contatos/faleConosco">Fale Conosco</a>
                </li>
            </ul>
        </nav><!-- #nav-menu-container -->
    </div>
</header>