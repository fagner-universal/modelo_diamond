<section id="contact" class="wow fadeInUp">
    <div class="container">
        <div class="section-header">
            <h2 class="text-center">Redes Sociais</h2>

        </div>

        <div class="row contact-info">
            <?php if (FACEBOOK != "" ) {?>
            <div class="col-md-3 col-lg-3">
                <div class="contact-phone">
                    <a href="<?php echo FACEBOOK ?>" target="_blank" class="facebook">
                        <i class="fab fa-facebook-square"></i>
                        <h3>Facebook</h3>
                    </a>
                </div>
            </div>
            <?php }?>
            <?php if (INSTAGRAM != "" ) {?>
            <div class="col-md-3 col-lg-3">
                <div class="contact-phone">
                    <a href="<?php echo INSTAGRAM ?>" target="_blank" class="google-plus">
                        <i class="fab fa-instagram"></i>
                        <h3>Instagram</h3>
                    </a>
                </div>
            </div>
            <?php }?>
            <?php if (YOUTUBE != "" ) {?>
            <div class="col-md-3 col-lg-3">
                <div class="contact-phone">
                    <a href="<?php echo YOUTUBE ?>" target="_blank" class="google-plus">
                        <i class="fab fa-youtube"></i>
                        <h3>Youtube</h3>
                    </a>
                </div>
            </div>
            <?php }?>
            <?php if (CELULAR_SCRIPT != "" ) {?>
            <div class="col-md-3 col-lg-3">
                <div class="contact-phone">
                    <a class="watsapp" href="https://api.whatsapp.com/send?phone=55<?php echo CELULAR_SCRIPT ?>&text=" target="_blank">
                        <i class="fab fa-whatsapp"></i>
                        <h3>WhatsApp</h3>
                    </a>
                </div>
            </div>
            <?php }?>
        </div>

    </div>
</section>