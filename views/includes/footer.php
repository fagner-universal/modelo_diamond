<!--==========================
    Footer
  ============================-->
<footer id="footer" style="background-color: var(--cor-primaria);padding-bottom: 0px">
    <div class="container-fluid" style="padding-left: 25px" >
        <div class="row" style="padding-top: 30px">
            <div  class="col-sm-12 col-md-3 col-lg-3 col-xl-3" style="border-right: solid 1px #FFF">
                <h4 style="font-weight: bold;" class="text-footer-color">Buscar Imóvel</h4>
                <a href="<?php echo BASE_URL ?>aluguel/imoveis/todas-as-cidades/todos-os-bairros/1-quartos/1-vaga-ou-mais/1-banheiro-ou-mais/?pagina=1&valorminimo=0&valormaximo=0" class="link-text-footer" >
                <!--<a href="<?php //echo BASE_URL?> #aluguel_destaque" class="link-text-footer" >-->
                    <h6 class="text-footer-color">Alugar</h6>
                </a>
                <a href="<?php echo BASE_URL ?>venda/imoveis/todas-as-cidades/todos-os-bairros/1-quartos/1-vaga-ou-mais/1-banheiro-ou-mais/?pagina=1&valorminimo=0&valormaximo=0" class="link-text-footer" >
                  <!--<a href="#imoveis_venda" class="link-text-footer" >-->
                    <h6 class="text-footer-color">Comprar</h6>
                </a>
<!--                <a href="--><?php //echo BASE_URL ?><!--?red=lancamento_destaque" class="link-text-footer" >-->
<!--                  <!--<a href="#imoveis_lancament" class="link-text-footer" >-->
<!--                    <h6 class="text-footer-color">Lançamentos</h6>-->
<!--                </a>-->
                <a href="<?php echo BASE_URL ?>aluguel/imoveis/todas-as-cidades/todos-os-bairros/1-quartos/1-vaga-ou-mais/1-banheiro-ou-mais/?pagina=1&valorminimo=0&valormaximo=0" class="link-text-footer" >
                    <h6 class="text-footer-color">Buscar Imóveis</h6>
                </a>
                 <a href="<?php echo BASE_URL ?>busca" class="link-text-footer" >
                    <h6 class="text-footer-color">Buscar pelo Mapa</h6>
                </a>
            </div>
            <div  class="col-sm-12 col-md-3 col-lg-3 col-xl-3" style="border-right: solid 1px #FFF">
                <h3 style="font-weight: bold;" class="text-footer-color">Institucional</h3>
                <a href="<?php echo BASE_URL ?>sobre" class="link-text-footer">
                    <h6 class="text-footer-color">Sobre nós</h6>
                </a>
<!--                <a href="--><?php //echo BASE_URL ?><!--equipe" class="link-text-footer"> -->
<!--                    <h6 class="text-footer-color">Nossa Equipe</h6>-->
<!--                </a>-->
                <?php if (AREA_CLIENTE != "" ) { ?>
                <a href="<?php echo AREA_CLIENTE ?>" target="_blank" class="link-text-footer">
                    <h6 class="text-footer-color">Área do Cliente</h6>
                </a>
                <?php } ?>
                <a href="<?php echo BASE_URL ?>contatos/faleConosco" class="link-text-footer">
                    <h6 class="text-footer-color">Fale Conosco</h6>
                </a>
                  <a href="<?php echo BASE_URL ?>anunciar" class="link-text-footer">
                    <h6 class="text-footer-color">Anuncie seu Imóvel</h6>
                </a>
               

            </div>
            <div  class="col-sm-12 col-md-6 col-lg-6 col-xl-6" >
            <div class="row">
                <div  class="col-12" >
                    <h4 class="text-center text-footer-color" style="font-weight: bold;border-bottom: 1px solid #ffffff40;">Contato</h4>
                </div>
            </div>
            <div class="row">
                <div  class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style="border-right: solid 1px #FFF;overflow:auto;">
                    <!--<h5 class="text-footer-color">Nova Serrana</h5>-->
                    <h2 class="float:left">
                        <?php if (ENDERECO != "" ) { ?>
                        <a href="<?php echo LINK_ENDERECO ?>" target="_blank" class="link-text-footer">
                            <h6 class="text-footer-color"><i class="fa fa-map-marker"></i> <?php echo ENDERECO ?></h6>
                        </a>
                        <?php } ?>
                        <?php if (EMAIL != "" ) { ?>
                        <a href="mail:<?php echo EMAIL ?>"  class="link-text-footer">
                            <h6 class="text-footer-color"><i class="fa fa-envelope"></i> <?php echo EMAIL ?></h6>
                        </a>
                        <?php } ?>
                        <?php if (TELEFONE_FIXO_SCRIPT != "" ) { ?>
                        <a href="tel:<?php echo TELEFONE_FIXO_SCRIPT ?>"  class="link-text-footer">
                            <h6 class="text-footer-color"><i class="fa fa-phone"></i> <?php echo TELEFONE_FIXO ?></h6>
                        </a>
                        <?php } ?>
                        <?php if (CELULAR != "" ) { ?>
                        <a href="https://api.whatsapp.com/send?phone=55<?php echo CELULAR_SCRIPT ?>&text=Olá, Gostaria de informações sobre um imóvel... "  class="link-text-footer">
                            <h6 class="text-footer-color"><i class="fab fa-whatsapp"></i> <?php echo CELULAR ?></h6>
                        </a>
                        <?php } ?>
                    </h2>
                </div>
            </div>
        </div>

        </div>
    </div>
<hr>
    <div  class="col-12" style="border-bottom: 1px solid #ffffff40 ;background-color: var(--cor-primaria);padding-bottom: 33px;">
<!--        <div class="copyright text-footer-color">-->
<!--            &copy; Copyright <strong class="text-footer-color">Jomar Imobiliária</strong>-->
<!--        </div>-->
        <div class="credits text-footer-color">
            Desenvolvido por <a class="text-footer-color" href="https://www.universalsoftware.com.br/">Universal Software</a>
        </div>
    </div>

</footer>