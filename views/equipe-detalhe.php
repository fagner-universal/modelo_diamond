
<main id="main">

    <div class="jumbotron jumbotron-fluid"  >
        <div class="container" >
            <div class="section-header" >
                <h2 style="color:#black"><?php echo $funcionario['nome_funcioario'] ?></h2>
               <!--<h3 style="color:#6c757d"><?php //echo $funcionario['nome_cargo']  ?></h3>-->

            </div>
        </div>
    </div>

    <!--==========================
      About Section
    ============================-->


    <section class=" banner-equipe-detalhe" id="about" class="wow fadeInUp" style="margin-top: -40px;padding-bottom: 100px; height: auto">
        <div class="container-fluid">
            <div class="row" style="padding:  10px">
                <div class="col-lg-6 about-img">
                    
                    
                    <a href="" >
                        <img src="<?php echo BASE_URL ?>assets/images/equipe/<?php echo $funcionario['foto'] ?>" alt="">
                    </a>
                </div>
                <div class="col-lg-6 content" style="word-wrap: break-word;">

                    <p class="text-sobre p-branco" style="text-align: justify;">
                        <?php echo $funcionario['descricao'] ?> 
                    </p>
                </div>
            </div>

        </div>
    </section>

    <!--AREA DE CONTATOS-->
<?php include 'includes/contatos.php'; ?>

</main>
