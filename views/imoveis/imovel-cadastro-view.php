
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class="text-center">Cadastre o seu Imóvel</h1>
        </div>

    </div>

    <form>


     <div class="form-row">
        <div class="form-group col-md-6">
            <label for="address">Rua</label>
            <input type="text" class="form-control" id="address" placeholder="Digite o endereço">
            <ul class="list-group lst-endereco">
                <!-- conteudo dinamico -->
            </ul>
        </div>
        <div class="form-group col-md-2">
            <label for="inputAddress2">Numero</label>
            <input type="text" class="form-control" id="inputAddress2" placeholder="">
        </div>
        <div class="form-group col-md-4">
            <label for="inputAddress2">Bairro</label>
            <input type="text" class="form-control" id="inputAddress2" placeholder="">
        </div>
        
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
             <label for="cidade">Vidade</label>
            <select id="cidade" class="form-control">
                <option value="Belo Horizonte" class="cit" >Belo Horizonte</option>
                <option value="São Paulo" class="cit">São Paulo</option>
                <option value="Ibirite" class="cit" >Ibirite</option>
                <option value="Santa Luzia" class="cit">Santa Luzia</option>
            </select>
        </div>
        <div class="form-group col-md-3">
            <label for="inputState">Estado</label>
            <select id="inputState" class="form-control">
                <option selected>Choose...</option>
                <option>...</option>
            </select>
        </div>
        <div class="form-group col-md-3">
            <label for="cep">CEP</label>
            <input type="text" class="form-control" id="cep">
        </div>
    </div>
    <div class="form-group">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" id="gridCheck">
            <label class="form-check-label" for="gridCheck">
                Check me out
            </label>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Sign in</button>
</form>
</div>

