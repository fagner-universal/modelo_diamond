<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title><?php echo $viewData['titlePagina'];?></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">


        <link rel="shortcut icon"  href="<?php echo BASE_URL ?>assets/images/logo/favicon.ico" type="image/x-icon" />

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">

        <!-- Bootstrap CSS File -->
        <link href="<?php echo BASE_URL; ?>assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Libraries CSS Files -->
        <link href="<?php echo BASE_URL; ?>assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo BASE_URL; ?>assets/lib/animate/animate.min.css" rel="stylesheet">
        <!--<link href="<?php //echo BASE_URL; ?>assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">-->
        <link href="<?php echo BASE_URL; ?>assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="<?php echo BASE_URL; ?>assets/lib/magnific-popup/magnific-popup.css" rel="stylesheet">
        <link href="<?php echo BASE_URL; ?>assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">

        <!-- Main Stylesheet File -->
        <link href="<?php echo BASE_URL; ?>assets/css/style.css" rel="stylesheet">

        <!-- =======================================================
          Theme Name: Reveal
          Theme URL: https://bootstrapmade.com/reveal-bootstrap-corporate-template/
          Author: BootstrapMade.com
          License: https://bootstrapmade.com/license/
        ======================================================= -->

        <!--ICONES GOOGLE-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
        
        
         <!--ICONES-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous"/>


        <!-- import  Clustering de marcadores -->
        <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>

        <!-- <link href="https://fonts.googleapis.com/css?family=Dancing+Script|Tangerine" rel="stylesheet"> -->

        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/bootstrap/css/bootstrap.min.css">

        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/style1.css">


        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/style.css">
        <?php include('includes/scripts_top.php') ?>
    </head>
    <?php include ('includes/topo.php') ?>
    <body>

        <!--==========================
          Top Bar
        ============================-->

<!--        <section id="topbar" class="d-none d-lg-block ">
            <div class="container clearfix">
                <div class="contact-info float-left">
                    <i class="fa fa-envelope-o"></i> <a href="mailto:contact@example.com">contact@example.com</a>
                    <i class="fa fa-phone"></i> +1 5589 55488 55
                </div>
                <div class="social-links float-right">
                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                    <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                    <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                </div>
            </div>
        </section>-->

        <!--==========================
          Header
        ============================-->
<!--         <header id="header">-->
<!--            <div class="container">-->
<!--                <div id="logo" class="pull-left" style="color:#00adef">-->
<!--                    <a href="--><?php //echo BASE_URL ?><!--"><img class="img-logo" src="--><?php //echo BASE_URL ?><!--assets/images/logo/logo.png" alt=""></a>-->
<!--                </div>-->
<!--                <nav id="nav-menu-container">-->
<!--                    <ul class="nav-menu">-->
<!--                        <li class="menu-active"><a href="--><?php //echo BASE_URL ?><!--">Home</a></li>-->
<!---->
<!--                        <li class="menu-has-children"><a href="">Buscar Imóvel</a>-->
<!--                            <ul>-->
<!--                                <li><a href="--><?php //echo BASE_URL ?><!--aluguel/apartamento/nova-serrana/">Buscar Imoveis</a></li>-->
<!--                                <li><a href="--><?php //echo BASE_URL ?><!--busca">Buscar pelo Mapa</a></li>-->
<!--                            </ul>-->
<!--                        </li>-->
<!---->
<!--                        <li><a href="--><?php //echo BASE_URL ?><!--sobre">Institucional</a></li>-->
<!--                        <li><a href="--><?php //echo BASE_URL ?><!--equipe">Nossa equipe</a></li>-->
<!--                          <li><a href="http://www.portalunsoft.com.br/area-do-cliente/jomar" target="blank">Area do Cliente</a></li>-->
<!--                        <li><a href="--><?php //echo BASE_URL ?><!--anunciar">Anuncie seu imóvel</a></li>-->
<!--                        <li><a href="--><?php //echo BASE_URL ?><!--contatos/faleConosco">Fale Conosco</a>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </nav><!-- #nav-menu-container -->
<!--            </div>-->
<!--        </header>-->

        <div class="container-fluid">
            <div class="col-sm-12" style="padding: 20px; background-color: rgba(255,255,255,0.8)">
                <div class="d-flex flex-row">
                    <form class="form-inline d-flex justify-content-center" style="margin: auto">
                         <select id="inputMapaCidade" class="custom-select my-1 mr-sm-4" id="inlineFormCustomSelectPref">
                        </select>
                        <select id="inputMapaFinalidade" class="custom-select my-1 mr-sm-4" id="inlineFormCustomSelectPref">

                            <option value="1">Aluguel</option>
                            <option value="2">Venda</option>

                        </select>

                         <select id="inputMapaTipo" class="custom-select my-1 mr-sm-4" id="inlineFormCustomSelectPref">


                        </select>

                    </form>
                </div>
            </div>
            <div class="row">
                <!--col-sm-12 col-md-5 col-lg-5-->
                <div class="col-sm-12" style="margin-bottom: 20px">
                    <div>
                        <div id="map" style="height: 700px"></div>
                    </div>
                </div>

<!--                
                <div id="scroll" class="col-sm-12"  style="height:800px;overflow:auto">

                    <div class="row lstImoveis">

                        <section id="team" class="wow fadeInUp" style="margin-top:100px">
                            <div class="container-fluid">
                                <div class="section-header text-center" >
                                    <h2>ALUGUÉIS EM DESTAQUE</h2>
                                </div>

                                <div class="row">
                                    <div class="owl-carousel testimonials-carousel imoveis-destaque">

                                        <?php //foreach (array('s' => 2, 's' => 2, 's' => 2, 's' => 2) as $key => $value) : ?>

                                            <a  href="<?php //echo BASE_URL ?>imoveis/<?php echo $value->codigo ?>">

                                                <div class="testimonial-item corpo-card">
                                                    <div class="member">
                                                        <div class="pic">
                                                            <img src="<?php //echo $value->fotos[0]->url ?>" alt="">
                                                        </div>

                                                        <div class="valor-card-destaque">
                                                            <h5 class="valor-texto"><?php //echo $value->valor ?></h5>
                                                        </div>

                                                        <div class="details">
                                                            <div class="social">
                                                                <ul class="list-group list-group-flush corpoListDetalhe">
                                                                    <li class="list-group-item d-flex  justify-content-between align-items-center">
                                                                        <span class="badge badge-primary badge-light"></span>
                                                                        <span class="badge badge-primary badge-light"><?php //echo $value->tipo ?></span>
                                                                        <span class="badge badge-primary badge-light"></span>
                                                                    </li>

                                                                    <li class="list-group-item d-flex  justify-content-between align-items-center">
                                                                        <span class="badge badge-primary badge-light"></span>
                                                                        <span class="badge badge-primary badge-light"><?php //echo $value->bairro ?> | <?php echo $value->cidade ?> </span>
                                                                        <span class="badge badge-primary badge-light"></span>
                                                                    </li>

                                                                    <li class="list-group-item d-flex justify-content-between align-items-center">

                                                                        <div class="row">
                                                                            <div class="col-4">
                                                                                <div class="d-flex justify-content-between align-items-center" >
                                                                                    <img class="mr-1 iconeListaCardListagem" src="<?php //echo BASE_URL ?>assets/images/icons/quartos.svg" alt="Generic placeholder image">
                                                                                    <div class="col txt-card-itens">
                                                                                        <span class="badge badge-primary badge-light"><?php //echo $value->numeroquartos ?></span>
                                                                                        <span class="badge badge-primary badge-light text-cards">Quartos</span> 
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="d-flex justify-content-between align-items-center">
                                                                                    <img class="mr-1 iconeListaCardListagem"  src="<?php //echo BASE_URL ?>assets/images/icons/banheiro1.png" alt="Generic placeholder image">
                                                                                    <div class="col txt-card-itens">
                                                                                        <span class="badge badge-primary badge-light"><?php //echo $value->numerobanhos ?></span>  
                                                                                        <span class="badge badge-primary badge-light text-cards">Banheiros</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <div class="d-flex justify-content-between align-items-center">
                                                                                    <img class="mr-1 iconeListaCardListagem"  src="<?php //echo BASE_URL ?>assets/images/icons/car-solid.svg" alt="Generic placeholder image">
                                                                                    <div class="col txt-card-itens">
                                                                                        <span class="badge badge-primary badge-light"><?php //echo $value->numerovagas ?></span>
                                                                                        <span class="badge badge-primary badge-light text-cards">Vagas</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                                                                        <img class="mr-1 iconeListaCardListagem"  src="<?php //echo BASE_URL                      ?>assets/images/icons/preco.svg" alt="Generic placeholder image">
                                                                    
                                                                                                                        <span class="badge badge-primary badge-light" style="font-size:20px"><?php //echo $value->valor                      ?></span>
                                                                                                                        <span class="badge badge-primary badge-light "></span>
                                                                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </a>

                                        <?php //endforeach; ?>

                                    </div>
                                </div>

                            </div>
                        </section>


                                                <div id="spinner"  style="margin: auto; margin-top: 50px">
                        
                                                    <img class="rounded mx-auto d-block" style="width: 200px;height: 150px" src="<?php //echo BASE_URL     ?>/assets/images/load.gif" />
                                                    <h1 class="text-center">...Aguarde o carregamentos dos imoveis</h1> 
                        
                                                </div>


                    </div>
                </div>
-->

            </div>

        </div>
    </div>
</div>


<?php include 'includes/contatos.php'; ?>


<?php include 'includes/footer.php'; ?>


<!--==========================
  Footer
============================-->
<!--<footer id="footer">
    <div class="container">
        <div class="copyright">
            &copy; Copyright <strong>Jomar Imoveis</strong>.
        </div>
        <div class="credits">
            Designed by <a href="#">Universal Software</a>
        </div>
    </div>
</footer> #footer 

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>-->




<script src="<?php echo BASE_URL; ?>assets/js/base_url.js"></script>



<!-- JavaScript Libraries -->
<script src="<?php echo BASE_URL; ?>assets/lib/jquery/jquery.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/lib/jquery/jquery-migrate.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/lib/easing/easing.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/lib/superfish/hoverIntent.js"></script>
<script src="<?php echo BASE_URL; ?>assets/lib/superfish/superfish.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/lib/wow/wow.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/lib/magnific-popup/magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL; ?>assets/lib/sticky/sticky.js"></script>

<!-- Contact Form JavaScript File -->
<script src="<?php echo BASE_URL; ?>assets/js/contactform/contactform.js"></script>

<!-- importação da api google maps -->          


        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->

   <!-- <script src="<?php //echo BASE_URL;                        ?>/assets/js/jquery.min.js"></script> -->

<script src="<?php echo BASE_URL; ?>/assets/bootstrap/js/bootstrap.js"></script> 
<!--  <script src="<?php // echo BASE_URL;                        ?>/assets/js/script.js"></script> --> 


<!--import filtro por movimentação do mapa-->
<script src="<?php echo BASE_URL; ?>assets/api-maps/filtro-mapa.js"></script>

<!--auto complite do input busca por endereço-->
<script src="<?php echo BASE_URL; ?>assets/api-maps/auto-complete-mapa.js"></script>

<!-- Template Main Javascript File -->
<script src="<?php echo BASE_URL; ?>assets/js/main.js"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBrMj9OwhfWmyB1zEfEY2zksiTAtqBkrZg&callback=initMap" type="text/javascript"></script>

<script>
    
    
   
    
// PREENCHER FORMULARIO TIPO
function carregarTipo() {

    $.ajax({
        url: '' + base_url + 'tipoAjax',
        type: 'GET',
        data: {},
        success: function (tipos) {

            $('#inputMapaTipo').append('<option  value="imoveis">Todos</option>');

            //                    var totalImoveis = 0;

            $.each(tipos, function (todostipo, tipo) {

                $('#inputMapaTipo').append('<option  value="' + tipo.codigo + '">' + tipo.nomeOriginal + '</option>');

            });

        }
    }).then(function () {
       
    });

}
carregarTipo();


    var todosBairroseCidades = [];

    //PREENCHER FORMULARIO BAIRRO
    function carregarBairrosMapa(inputMapaCidade) {

        $.ajax({
            url: '' + base_url + 'bairroAjaxOriginal',
            type: 'GET',
            data: {},
            success: function (resultBairros) {

                let resultadoBairros = jQuery.parseJSON(resultBairros);

                let bairroSelecionado = $('#inputMapaCidade').val();

                $('#inputMapaBairro').empty();

                var totalImoveis = 0;

                $.each(resultadoBairros.lista, function (todos, bairr) {

                    if (bairroSelecionado == resultadoBairros.lista[todos].cidade) {

                        $('#inputMapaBairro').append('<option  value="' + bairr.codigo + '">' + bairr.nome + '</option>');

                    }

                });

            }
        });

    }




//PREENCHER FORMULARIO CIDADE
    function carregarCidadeMapa() {

        $.ajax({
            url: '' + base_url + 'cidadeAjaxOriginal',
            type: 'GET',
            data: {},
            success: function (Rcidades) {

                let cidades = jQuery.parseJSON(Rcidades);
                todosBairroseCidades = cidades;

                $('#inputMapaCidade').empty();

                 $('#inputMapaCidade').append('<option selected value="0">todas as cidades</option>');
                $.each(cidades.lista, function (todos, cidade) {
                    $('#inputMapaCidade').append('<option value="' + cidade.codigo + '">' + cidade.nome + '</option>');
                });


            }
        });
    }


    carregarCidadeMapa();

    $('#inputMapaFinalidade').on('change', function () {
        let inputMapaFinalidade = $(this).val();
        $('.lstImoveis').empty();

        initMap();
        parametros.finalidade = inputMapaFinalidade;
        carregarImoveisAPI();

    });


    $('#inputMapaCidade').on('change', function () {
        let inputMapaCidade = $(this).val();


//        $('#inputMapaCidade').append('<option value="">Todos</option>');
        //pegar o codigo do bairro   



//        $.each(todosBairroseCidades.lista, function (t, c) {
//            if (inputMapaCidade == c.nome) {
//                
//     
//                $('#inputMapaCidade').append('<option value="' + c.codigo + '">' + c.nome + '</option>');
//            }
//        });

        $('.lstImoveis').empty();

        initMap();
        parametros.codigocidade = inputMapaCidade;
        carregarImoveisAPI();

        //carregarBairrosMapa(inputMapaCidade);


    });

    $('#inputMapaBairro').on('change', function () {

        let inputMapaBaairro = $(this).val();

        $('.lstImoveis').empty();

        initMap();
        parametros.codigosbairros = inputMapaBaairro;
        carregarImoveisAPI();


    });
    
    
    
     $('#inputMapaTipo').on('change', function () {

        let inputCodigoTipo = $(this).val();

        $('.lstImoveis').empty();



        initMap();
        parametros.codigoTipo = inputCodigoTipo;
        carregarImoveisAPI();


    });
    
    
    



</script>

<?php include('includes/scripts_bottom.php') ?>
</body>
</html>






