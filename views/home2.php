<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">

    <title><?php echo $viewData['titlePagina']; ?></title>

    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <meta property="og:type" content="website">
    <meta property="og:image" content="<?php echo BASE_URL ?>assets/img/logos/logo.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="672">
    <meta property="og:image:height" content="465">
    <meta property="og:url" content="<?php echo BASE_URL ?>">
    <meta property="og:title" content="<?php echo META_TITLE ?>">
    <meta property="og:site_name" content="<?php echo TITULO_AUXILIAR ?>">
    <meta property="og:description" content="<?php echo DESCRIPTION ?>">

    <meta name="title" content="<?php echo META_TITLE ?>"/>
    <meta name="description" content="<?php echo DESCRIPTION ?>"/>
    <meta name="keywords" content="<?php echo KEYWORDS ?>"/>
    <meta name="robots" content=index />
    <!-- Favicons -->
    <!--        <link href="img/favicon.png" rel="icon">
                <link href="img/apple-touch-icon.png" rel="apple-touch-icon">-->

    <link rel="shortcut icon" href="<?php echo BASE_URL ?>assets/images/logo/favicon.ico" type="image/x-icon" />

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet" />

    <!-- Bootstrap CSS File -->
    <link href="<?php echo BASE_URL; ?>assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/lib/slick-1.8.1/slick/slick-theme.css" />
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/lib/slick-1.8.1/slick/slick.css" />
    <!-- Libraries CSS Files -->
    <link href="<?php echo BASE_URL; ?>assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_URL; ?>assets/lib/animate/animate.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_URL; ?>assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_URL; ?>assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_URL; ?>assets/lib/magnific-popup/magnific-popup.css" rel="stylesheet" />
    <link href="<?php echo BASE_URL; ?>assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />
   

    <!--ICONES-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous" />

    <!-- Main Stylesheet File -->
    <link href="<?php echo BASE_URL; ?>assets/css/style.css" rel="stylesheet" />



    <!--ICONES GOOGLE-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />

    <!-- import  Clustering de marcadores -->
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>

    <!-- <link href="https://fonts.googleapis.com/css?family=Dancing+Script|Tangerine" rel="stylesheet"> -->

    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/bootstrap/css/bootstrap.min.css" />

    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/style1.css" />

    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/style.css" />
    
    <?php include('includes/scripts_top.php') ?>

</head>

<body id="body">
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:#00d45e;color:#FFF">
                    <h5 class="modal-title" id="exampleModalLabel"><i class="fab fa-whatsapp"></i> Iniciar conversa no WhatsApp</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mensagem</label>
                            <input id="text-wats" type="text" class="form-control" id="" aria-describedby="emailHelp" placeholder="">
                            <!--<small id="emailHelp" class="form-text text-muted">Mensagem.</small>-->
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" style="background-color:#00d45e;color:#FFF" data-dismiss="modal">Fechar</button>
                    <button id="watsapp" style="background-color:#00d45e;color:#FFF" type="button" class="btn btn-primary">Enviar</button>
                </div>
            </div>
        </div>
    </div>

    <?php include('includes/topo.php') ?>


    <style>
    .btn-secondary {
        background-color: rgba(204, 204, 204, 0.7);
        border-color: rgba(204, 204, 204, 0.7);
    }

    .btn-secondary:not(:disabled):not(.disabled).active,
    .btn-secondary:not(:disabled):not(.disabled):active,
    .show>.btn-secondary.dropdown-toggle {
        color: #fff;
        background-color: var(--cor-primaria);
        border-color: var(--cor-primaria);
        width: 90px;
    }
</style>

<main id="main">
    <div class="corpo-banner-busca">
        <section id="intro">
            <div class="intro-content">
                <div class="container boxBusca">
                    <h3 class="input-style">Encontrar imóveis</h3>
                    <form class="needs-validation" novalidate>
                        <div class="form-row">
                            <div class="col-md-3 corpo-input-busca-inicial">
                                <label class="label-home 	d-none d-sm-block" for="validationCustomUsername">Finalidade</label>
                                <div id="finalidade" class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label class="btn btn-secondary active finalidade_home" id="aluguel">
                                        <input class="finalidade_home" type="radio" value="1" name="options" autocomplete="off" checked>Alugar</label>
                                    </label>
                                    <label class="btn btn-secondary finalidade_home" id="venda">
                                        <input class="finalidade_home" type="radio" value="2" name="options" autocomplete="off">Comprar</label>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3 corpo-input-busca-inicial">
                                <label class="label-home d-none d-sm-block" for="validationCustomUsername">Tipo</label>
                                <select class="form-control tipoImoveis tipoImoveis_home" id="exampleFormControlSelect1">
                                    <option selected value="">Tipo do imóvel</option>
                                </select>
                            </div>
                            <div class="col-md-6 corpo-input-busca-inicial">
                                <label class="label-home d-none d-sm-block" for="validationCustomUsername">Cidade</label>
                                <div class="input-group">
                                    <select class="form-control cidade cidade_home" >
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 corpo-input-busca-inicial" style="padding-top: 7px;">
                                <div class="input-group">
                                    <select  class="form-control bairro bairro_home">
                                        <option selected value="todas-os-bairros">Todos os bairros</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6" style="padding-top: 7px;">
                                <div class="input-group">
                                    <input id="codigoImovel" type="text" class="form-control" placeholder="Digite o Código" aria-label="Recipient's username" aria-describedby="button-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="button" id="button-addon2" style="color: #fff;background-color: var(--cor-primaria);border-color: var(--cor-primaria);width: 100%;">
                                            Buscar código
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6" style="padding-top: 7px;margin: auto;">
                                <button class="btn btn-primary btBuscar btBuscar_home" type="button">Buscar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

    </div>

    </section><!-- #intro -->

    <main id="main">

        <!--==========================
      About Section
    ============================-->
    <?php if (EXIBIR_MAPA == "S") { ?>
        <section>
            <div id="map" class="container-fluid" style="height: 500px">
                <!--<div id="map" style="height: 500px"></div>-->
            </div>
            <div class="buttom-map" style="margin-top: -74px;margin-left: 10%;">
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    <label class="btn btn-secondary active" id="aluguel-mapa">
                        <input type="radio" name="aluguel" autocomplete="off" checked> Alugar
                    </label>

                    <label class="btn btn-secondary " id="venda-mapa">
                        <input type="radio" name="venda" autocomplete="off"> Comprar
                    </label>

                </div>
            </div>
        </section><!-- #about -->
    <?php } ?>

        <!--==========================
            IMOVEIS VENDA EM DESTAQUES
        ============================-->


        <section id="team" class="wow fadeInUp">
            <div class="container-fluid">
                <div class="section-header text-center">
                    <h2 id="imoveis_venda">VENDAS EM DESTAQUE</h2>
                </div>

                <div id="destaqueVenda">
                    <p class="text-center">Carregando...</p>
                </div>

            </div>
        </section>

        <!--==========================
            IMOVEIS ALUGUEL EM DESTAQUES
        ============================-->
        <section id="team" class="wow fadeInUp" >
            <div class="container-fluid">
                <div class="section-header text-center">
                    <h2 id="aluguel_destaque">ALUGUÉIS EM DESTAQUE</h2>
                </div>

                <div id="destaqueAluguel">
                    <p class="text-center">Carregando...</p>
                </div>
            </div>
        </section>


        <!--==========================
            LANÇAMENTOS EM DESTAQUES
        ============================-->
        <section id="team" class="wow fadeInUp" >
            <div class="container-fluid">
                <div class="section-header text-center">
                    <h2 id="lancamento_destaque">LANÇAMENTOS EM DESTAQUE</h2>
                </div>

                <div id="lancamentos">
                    <p class="text-center">Carregando...</p>
                </div>
            </div>
        </section>
        

        <!--            <div class="area-call-to-action-alugar ">-->
        <section class="area-call-to-action-alugar wow fadeInUp">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12" style="margin-botto: 100px">
                        <h1 class="text-center h3-branco" style="margin-bottom: 100px">Como alugar um imóvel na <?php echo NOME_IMOBILIARIA ?> ?</h1>
                    </div>
                    <div class="col-md-6 content">
                        <a href="<?php echo BASE_URL ?>garantias">
                            <img class="rounded mx-auto d-block rotate-scale-up " style="width: 150px" src="<?php echo BASE_URL ?>assets/images/icons/security-circulo.png" />
                            <h3 class="text-center h3-branco">Garantias Locatícias</h3>
                        </a>
                    </div>
                    <div class="col-md-6 content">
                        <a href="<?php echo BASE_URL ?>documentos">
                            <img class="rounded mx-auto d-block rotate-scale-up " style="width: 150px;color:#fff" src="<?php echo BASE_URL ?>assets/images/icons/personal-circulo.png" />
                            <h3 class="text-center h3-branco">Documentos</h3>
                        </a>
                    </div>
                    <!-- <div class="col-md-4">
                        <div class="contact-phone">
                            <a href="<?php echo BASE_URL ?>captalizacao">
                                <img class="rounded mx-auto d-block  rotate-scale-up " style="width: 150px" src="<?php echo BASE_URL ?>assets/images/icons/house-circulo.png" />
                                <h3 class="text-center h3-branco">Título de captalização</h3>
                            </a>
                        </div>
                    </div> -->


                </div>
            </div>

            </div>
        </section>
        <!--        </div>-->
        <?php include 'includes/contatos.php'; ?>

        <?php if (AREA_CLIENTE != "") { ?>
        <section class="area-call-to-action wow fadeInUp">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12" style="margin-bottom: 67px">
                        <h1 style="margin-bottom:30px" class="text-center h3-branco">Área do Cliente</h1>
                    </div>
                    <div class="col-md-6 content">
                        <a href="<?php echo AREA_CLIENTE ?>" target="blank">
                            <img class="rounded mx-auto d-block rotate-scale-up " style="width: 150px" src="<?php echo BASE_URL ?>assets/images/icons/locador.png" />
                            <h3 class="text-center h3-branco">Locador / Proprietário </h3>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <div class="contact-phone">
                            <a href="<?php echo AREA_CLIENTE ?>" target="blank">
                                <img class="rounded mx-auto d-block  rotate-scale-up " style="width: 150px" src="<?php echo BASE_URL ?>assets/images/icons/locatario.png" />
                                <h3 class="text-center h3-branco">Locatario/ Inquilino</h3>
                            </a>
                        </div>
                    </div>
                    <!--                <div class="col-md-4">-->
                    <!--                    <div class="contact-phone">-->
                    <!--                        <a href='--><?php //echo AREA_CLIENTE 
                                                            ?>
                    <!--' target="blank">-->
                    <!--                            <img class="rounded mx-auto d-block  rotate-scale-up " style="width: 150px" src="--><?php //echo BASE_URL 
                                                                                                                                        ?>
                    <!--assets/images/icons/manutencao.png" />-->
                    <!--                            <h3  class="text-center h3-branco">Abrir Ticket de manutenção</h3>-->
                    <!--                        </a>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                </div>
            </div>

            </div>
        </section>
        <?php } ?>

    </main>

</main>

<div class="modal fade" id="divulgacao" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="z-index: 999999999;">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <!--                                        <h4 class="modal-title" id="gridSystemModalLabel">Horário de Plantão</h4>-->
            </div>
            <div class="modal-body text-center ">
                <!--                    <iframe width="560" height="315"  src="https://www.youtube.com/embed/n08fM4hQ7a0"  allowfullscreen=""></iframe>-->
                <img src="<?php echo BASE_URL ?>assets/images/imagem-popup.jpeg" style="width: 100% ; height: auto;">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
    
    <?php if(WHATSAPP != "") { ?>
    <?php include('includes/whatsapp.php'); ?>
    <?php } ?>
    <?php include('includes/footer.php'); ?>



    <!-----------------------------MODAL PARA EXIBIR BUSCA POR CÓDIGO---------------------------------------- -->
    <div class="modal fade modal-fullscreen force-fullscreen " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="overflow:auto">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center tituloModal">Imóveis encontrados</h4>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <!--                        <button id="fecharModal" type="button" class="btn btn-danger">Fechar</button>-->
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 conteudoModal">
                            </div>
                            <div class="col-md-4 ml-auto">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!--<button id="fecharModal" type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                    <!--                <button id="fecharModal" type="button" class="btn btn-primary">Fechar</button>-->
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!--   ---------------------------MODAL PARA EXIBIR BUSCA POR CÓDIGO---------------------------------------- -->

    <script src="<?php echo BASE_URL; ?>assets/js/base_url.js"></script>


    <!-- JavaScript Libraries -->
    <script src="<?php echo BASE_URL; ?>assets/lib/jquery/jquery.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/jquery/jquery-migrate.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/slick-1.8.1/slick/slick.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/js/home/destaques.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/easing/easing.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/superfish/hoverIntent.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/superfish/superfish.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/wow/wow.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/owlcarousel/owl.carousel.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/magnific-popup/magnific-popup.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/sticky/sticky.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/mask/jquery.mask.js"></script>

    


    <script src="<?php echo BASE_URL; ?>/assets/bootstrap/js/bootstrap.js"></script>


    <!-- Template Main Javascript File -->
    <script src="<?php echo BASE_URL; ?>assets/js/main.js"></script>

    <!--FORMULARIO HOME-->
    <script src="<?php echo BASE_URL; ?>assets/js/home/form_busca_home.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/js/home/busca.js"></script>

  
    <!--MODAL DE WATS APP-->
    <script src="<?php echo BASE_URL; ?>assets/js/watsapp/watsapp.js"></script>

    <!--SCRIPT FORMULARIO DE LEAD-->

</body>

<!--//SCROOL DETALHES-->


<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBrMj9OwhfWmyB1zEfEY2zksiTAtqBkrZg&callback=initMap" type="text/javascript"></script>

<?php if (EXIBIR_MAPA == "S") { ?>
<script src="<?php echo BASE_URL; ?>assets/js/home/mapa.js"></script>
<?php } ?>
<script>
    $(document).ready(function() {
        try {

            var url_direct = window.location.href.slice(window.location.href.indexOf('=') + 1);

        } catch (e) {


        }

        if (url_direct == 'aluguel_destaque') {

            $('html, body').animate({
                scrollTop: $('#aluguel_destaque').offset().top
            }, 'slow');


        } else if (url_direct == 'venda_destaque') {

            $('html, body').animate({
                scrollTop: $('#imoveis_venda').offset().top
            }, 'slow');


        } else if (url_direct == 'lancamento_destaque') {

            $('html, body').animate({
                scrollTop: $('#imoveis_lancament').offset().top
            }, 'slow');


        } else {

        }

    });


    //        $('#fecharModal').click(function(){
    //
    //            $('#myModal').toogkke
    //
    //        });


    $('.carousel-item').click(function() {

        setTimeout(function() {
            $('.mfp-arrow-right').trigger('click');
        }, 1000)


    })

    //Ativar Modal
    // $('#divulgacao').modal('show');


</script>
    <script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>
    <script>
        window.cookieconsent.initialise({
            "palette": {
                "popup": {
                    "background": "#eaf7f7",
                    "text": "#5c7291"
                },
                "button": {
                    "background": "var(--cor-primaria)",
                    "text": "#ffffff"
                }
            },
            "theme": "classic",
            "content": {
                "message": "Utilizamos cookies para oferecer melhor experiência, melhorar o desempenho, analisar como você interage em nosso site e personalizar conteúdo. Ao utilizar este site, você concorda com o uso de cookies.",
                "dismiss": "Aceito"
            }
        });
    </script>
    <?php include('includes/scripts_bottom.php') ?>
</html>

