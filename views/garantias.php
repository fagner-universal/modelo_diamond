<main id="main">
    <div class="jumbotron jumbotron-fluid" style="background-image: url(../assets/images/banner/call-to-action-venha.jpg);background-position: center">
        <div class="container">
            <div class="section-header">
                <h2 class="text-center" style="color:#FFF">Garantias Locatícias</h2>
                <!--<h3 class="text-center" style="color:#FFF">PESSOA FÍSICA</h3>-->

            </div>
        </div>
    </div>


    <section id="about" class="wow fadeInUp">
        <div class="container">
            <div class="row">

                <div class="col-lg-6 content">

                    <h2>Fiador</h2>
                    <!--<h4>Pessoa Física que deseja alugar um imóvel através de fiador os documentos são :</h4>-->
                    <ul>
                        <li style="text-align: justify;"><i class="ion-android-checkmark-circle"></i>Neste caso, são solicitados 02 (dois) fiadores, pessoa física (PF), residentes no
                            Espírito Santo. Ambos devem apresentar renda liquida de no mínimo três vezes o
                            valor locatício (que é a soma dos gastos mensais do imóvel), ter até 65 anos, e
                            possuir ao menos um imóvel quitado, localizado no estado do ES, apresentar cópia
                            da escritura do imóvel ou Certidão Atualizada do registro de Imoveis a serem
                            oferecidos como garantia (Máximo 30 dias) e IPTU do ano.</li>
                        <!-- <li><i class="ion-android-checkmark-circle"></i> <b>Fiador de renda</b> - Apresentar renda de 3 vezes o valor do aluguel mais os encargos ( condomínio e IPTU ), RG, CPF, Comprovante de residência, estado civil;</li> -->

                        <p>
                            Favor enviar a documentação para o email: administrativo@liviamachado.com.
                        </p>
                    </ul>


                </div>

                <div class="col-lg-6 content">
                    <h2>Seguro Fiança</h2>
                    <!--<h4>Pessoa Física que deseja alugar um imóvel através de fiador os documentos são :</h4>-->
                    <ul>
                        <li style="text-align: justify;"><i class="ion-android-checkmark-circle"></i>O seguro fiança é uma forma rápida de se alugar um imóvel. O processo é
                            intermediado pela Livia Machado imoveis com a corretora. Enviamos a
                            documentação e a ficha cadastral (favor solicitar ao atendente) para análise, que é
                            feita em até 48 horas. Após aprovação, recebemos o orçamento, e o pagamento
                            pode ser feito em até 4x sem juros no carnê, débito em conta ou em até 8x sem
                            juros no cartão de crédito. O seguro deverá ser renovado a cada 12 meses.
                        </li>

                        <p>
                            Favor enviar a documentação para o email: administrativo@liviamachado.com.
                        </p>

                        <!-- <li><i class="ion-android-checkmark-circle"></i> Nesta modalidade a seguradora será sua fiadora, e este seguro será renovado a cada 12 meses.
                            (Valor não será resgatado ao fim da locação).<br/> Gentileza entrar em contato com a xxxxx seguros TEL : xxxx-xxxx.</li> -->

                    </ul>

                </div>

                <div class="col-lg-6 content">

                    <h2>CredPago</h2>
                    <p style="text-align: justify;">A CredPago é a forma mais rápida e prática de alugar o seu imóvel. Sem
                        burocracia, utilizando apenas seu cartão de crédito!
                        Para análise o locatário deverá apresentar:</p>
                    <!--<h4>Pessoa Física que deseja alugar um imóvel através de fiador os documentos são :</h4>-->
                    <ul>
                        <li><i class="ion-android-checkmark-circle"></i> <b>Documentos pessoais:</b> RG e CPF ou CNH;</li>
                        <li style="text-align: justify;"><i class="ion-android-checkmark-circle"></i> <b>Dados do cartão de crédito:</b> Precisará ter um limite à vista superior a 4 vezes o valor do aluguel, sendo possível a soma de cartões de crédito de até 2 pessoas em um mesmo contrato;</li>
                        <li><i class="ion-android-checkmark-circle"></i> Email e telefone;</li>
                        <li><i class="ion-android-checkmark-circle"></i> A análise cadastral é feita e o resultado sai na hora! Pronto, viu como é fácil?</li>
                        <p>A CredPago deverá ser renovada a cada 12 meses.</p>
                        <p>
                            Favor enviar a documentação para o email: administrativo@liviamachado.com.
                        </p>

                    </ul>

                </div>

                <!-- <div class="col-lg-6 content">

                    <h2>Documentos Locatário</h2> -->
                <!--<h4>Pessoa Física que deseja alugar um imóvel através de fiador os documentos são :</h4>-->
                <!-- <ul>
                        <li><i class="ion-android-checkmark-circle"></i> <b>Locatário</b> - Apresentar RG, CPF, Comprovante de residência, estado civil, profissão, e-mail e contato;</li>
                        <p>
                            Recolher as documentações solicitadas enviar para análise no e-mail: xxxxxxxxxxxx@xxxx.xxx Contato: (xx) xxxxx-xxxx.
                        </p>

                    </ul>

                </div> -->

            </div>

        </div>

        </div>
    </section><!-- #about -->


    <!--INCLUDE AREA DE CONTATOS-->
    <?php include 'includes/contatos.php'; ?>


</main>