<!--
<section id="intro">
    <div class="intro-content">
        <div>
            <div class="container" style="background-color: rgba(0,0,0,0.8);padding: 10px">
                <h3 class="input-style">Titulo do imovel</h3>
            </div>
        </div>
    </div>

    <div id="intro-carousel" class="owl-carousel" >
        <div class="item" style="background-image: url('<?php echo BASE_URL ?>assets/images/banner/1.jpg');"></div>
        <div class="item" style="background-image: url('<?php echo BASE_URL ?>assets/images/banner/2.jpg');"></div>
        <div class="item" style="background-image: url(<?php echo BASE_URL ?>assets/images/banner/3.jpg);"></div>
        <div class="item" style="background-image: url(<?php echo BASE_URL ?>assets/images/banner/.jpg);"></div>
        <div class="item" style="background-image: url(<?php echo BASE_URL ?>assets/images/banner/3.jpg);"></div>
    </div>

</section> #intro -->
<!-- Button trigger modal -->



<main id="main">



    <section class="area-call-to-action-equipe">
        <div class="container-fluid">
            <div class="row">
                <!--                <div class="col-lg-6 content">
                                    <h3  class="text-center h3-branco">Venha protagonizar essa história de sucesso com a gente.</h3>
                                </div>-->
                <div class="col-md-12">
                    <div class="contact-phone">
                        <!--                        <h2 class="text-center h3-branco">Click para fazer contato</h2>-->
                        <!--                        <div class="text-center ">
                                                    <a  class="btn-get-started scrollto" href="#team">
                                                        <button  class="btn btn-outline-dark btn-call" 
                                                                 style="background-color: #ff6633ba;
                                                                 border-color: transparent;
                                                                 color: #FFF;
                                                                 font-size: 20px;" type="submit">
                                                            
                                                            Conheça nosso time
                                                        </button>
                                                        <b>Conheça nosso time</b>
                                                    </a>
                                                </div>-->
                    </div>
                </div>
            </div>
        </div>

        </div>
    </section>




    <!--     ==========================
          Our Team Section
        ============================-->
    <section id="team" class="wow fadeInUp">
        <div class="container" data-target="#exampleModalLong">
            <div class="section-header">
                <!-- <h2>Diretoria</h2> -->
            </div>
            <div class="row">

                <?php foreach ($funcionarios as $key => $value) : ?>

                    <?php if ($value['id_cargo'] == 1) : ?>

                        <div class="col-md-3 col-lg-4">
                            <div class="member" style="box-shadow: 10px 10px 10px 0px #8888884f;">
                                <a href="<?php echo BASE_URL ?>equipe-detalhe/<?php echo $value['id']; ?>/<?php echo $value['id_cargo']; ?>">
                                    <div class="pic"><img src="<?php echo BASE_URL ?>assets/images/equipe/<?php echo $value['foto']; ?>" alt=""></div>
                                </a>
                                <div class="details nosso-time">
                                    <h4><?php echo $value['nome_funcioario']; ?></h4>
                                    <span><?php echo $value['nome_cargo']; ?></span>
                                    <?php if ($value['wats'] == 's') : ?>
                                        <a class='watsapp' class='link-email' href="#" data-toggle="modal" valores="<?php echo $value['telefone']; ?>" class="twitter"><?php echo $value['telefone']; ?> <i class="fab fa-whatsapp"></i></a>
                                    <?php endif ?>

                                    <?php if ($value['wats'] == 'n') : ?>
                                        <a class='link-email' href="#" data-toggle="modal" class="twitter"><?php echo $value['telefone']; ?></a>
                                    <?php endif ?>
                                    <a class='link-email' href="mailto:<?php echo $value['email']; ?>"><span><i class="fa-envelope-opena"></i><?php echo $value['email']; ?></span></a>
                                </div>
                            </div>
                            </a>
                        </div>

                    <?php endif; ?>

                <?php endforeach; ?>

            </div>
        </div>
    </section>

    <section id="team" class="wow fadeInUp">
        <div class="container" data-target="#exampleModalLong">
            <div class="section-header">
                <!-- <h2>Locação - Nova Serrana</h2> -->
            </div>
            <div class="row">
                <?php foreach ($atendimentoNovaSerrana as $key => $value) : ?>

                    <?php if ($value['id_cargo'] == 2) : ?>

                        <div class="col-md-3 col-lg-4">
                            <div class="member" style="box-shadow: 10px 10px 10px 0px #8888884f;">
                                <a href="<?php echo BASE_URL ?>equipe-detalhe/<?php echo $value['id']; ?>/<?php echo $value['id_cargo']; ?>">
                                    <div class="pic"><img src="<?php echo BASE_URL ?>assets/images/equipe/<?php echo $value['foto']; ?>" alt=""></div>
                                </a>
                                <div class="details nosso-time">
                                    <h4><?php echo $value['nome_funcioario']; ?></h4>
                                    <span><?php echo $value['nome_cargo']; ?></span>
                                    <?php if ($value['wats'] == 's') : ?>
                                        <a class='watsapp' class='link-email' href="#" data-toggle="modal" valores="<?php echo $value['telefone']; ?>" class="twitter"><?php echo $value['telefone']; ?> <i class="fab fa-whatsapp"></i></a>
                                    <?php endif ?>

                                    <?php if ($value['wats'] == 'n') : ?>
                                        <a class='link-email' href="#" data-toggle="modal" class="twitter"><?php echo $value['telefone']; ?></a>
                                    <?php endif ?>
                                    <a class='link-email' href="mailto:<?php echo $value['email']; ?>"><span><i class="fa-envelope-opena"></i><?php echo $value['email']; ?></span></a>
                                </div>
                            </div>
                            </a>
                        </div>

                    <?php endif; ?>

                <?php endforeach; ?>

            </div>
        </div>
    </section>

    <section id="team" class="wow fadeInUp">
        <div class="container" data-target="#exampleModalLong">
            <div class="section-header">
                <!-- <h2>Locação - Perdigão</h2> -->
            </div>
            <div class="row">

                <?php //echo "<pre>"; var_dump($atendimentoNovaSerrana); 
                ?>

                <?php foreach ($atendimentoNovaPerdigao as $key => $value) : ?>

                    <?php if ($value['id_cargo'] == 3) : ?>

                        <div class="col-md-3 col-lg-4">
                            <div class="member" style="box-shadow: 10px 10px 10px 0px #8888884f;">
                                <a href="<?php echo BASE_URL ?>equipe-detalhe/<?php echo $value['id']; ?>/<?php echo $value['id_cargo']; ?>">
                                    <div class="pic"><img src="<?php echo BASE_URL ?>assets/images/equipe/<?php echo $value['foto']; ?>" alt=""></div>
                                </a>
                                <div class="details nosso-time">
                                    <h4><?php echo $value['nome_funcioario']; ?></h4>
                                    <span><?php echo $value['nome_cargo']; ?></span>
                                    <!--<i class="fab fa-whatsapp"></i>-->
                                    <!--data-target="#exampleModal"-->
                                    <?php if ($value['wats'] == 's') : ?>
                                        <a class='watsapp' class='link-email' href="#" data-toggle="modal" valores="<?php echo $value['telefone']; ?>" class="twitter"><?php echo $value['telefone']; ?> <i class="fab fa-whatsapp"></i></a>
                                    <?php endif ?>

                                    <?php if ($value['wats'] == 'n') : ?>
                                        <a class='link-email' href="#" data-toggle="modal" class="twitter"><?php echo $value['telefone']; ?></a>
                                    <?php endif ?>
                                    <a class='link-email' href="mailto:<?php echo $value['email']; ?>"><span><i class="fa-envelope-opena"></i><?php echo $value['email']; ?></span></a>
                                </div>
                            </div>
                            </a>
                        </div>

                    <?php endif; ?>

                <?php endforeach; ?>

            </div>
        </div>
    </section>

    <section id="team" class="wow fadeInUp">
        <div class="container" data-target="#exampleModalLong">
            <div class="section-header">
                <!-- <h2>Vendas</h2> -->
            </div>
            <div class="row">


                <?php foreach ($corretores as $key => $value) : ?>

                    <?php if ($value['id_cargo'] == 4) : ?>

                        <div class="col-md-3 col-lg-4">
                            <div class="member" style="box-shadow: 10px 10px 10px 0px #8888884f;">
                                <a href="<?php echo BASE_URL ?>equipe-detalhe/<?php echo $value['id']; ?>/<?php echo $value['id_cargo']; ?>">
                                    <div class="pic"><img src="<?php echo BASE_URL ?>assets/images/equipe/<?php echo $value['foto']; ?>" alt=""></div>
                                </a>
                                <div class="details nosso-time">
                                    <h4><?php echo $value['nome_funcioario']; ?></h4>
                                    <span><?php echo $value['nome_cargo']; ?></span>
                                    <!--<i class="fab fa-whatsapp"></i>-->
                                    <!--data-target="#exampleModal"-->
                                    <?php if ($value['wats'] == 's') : ?>
                                        <a class='watsapp' class='link-email' href="#" data-toggle="modal" valores="<?php echo $value['telefone']; ?>" class="twitter"><?php echo $value['telefone']; ?> <i class="fab fa-whatsapp"></i></a>
                                    <?php endif ?>

                                    <?php if ($value['wats'] == 'n') : ?>
                                        <a class='link-email' href="#" data-toggle="modal" class="twitter"><?php echo $value['telefone']; ?></a>
                                    <?php endif ?>
                                    <a class='link-email' href="mailto:<?php echo $value['email']; ?>"><span><i class="fa-envelope-opena"></i><?php echo $value['email']; ?></span></a>
                                </div>
                            </div>
                            </a>
                        </div>

                    <?php endif; ?>

                <?php endforeach; ?>

            </div>
        </div>
    </section>



    <section id="team" class="wow fadeInUp">
        <div class="container" data-target="#exampleModalLong">
            <div class="section-header">
                <!-- <h2>Financeiro</h2> -->
            </div>
            <div class="row">

                <?php foreach ($financeiro as $key => $value) : ?>



                    <?php if ($value['id_cargo'] == 7) : ?>

                        <div class="col-md-3 col-lg-4">
                            <div class="member" style="box-shadow: 10px 10px 10px 0px #8888884f;">
                                <a href="<?php echo BASE_URL ?>equipe-detalhe/<?php echo $value['id']; ?>/<?php echo $value['id_cargo']; ?>">
                                    <div class="pic"><img src="<?php echo BASE_URL ?>assets/images/equipe/<?php echo $value['foto']; ?>" alt=""></div>
                                </a>
                                <div class="details nosso-time">
                                    <h4><?php echo $value['nome_funcioario']; ?></h4>
                                    <span><?php echo $value['nome_cargo']; ?></span>
                                    <!--<i class="fab fa-whatsapp"></i>-->
                                    <!--data-target="#exampleModal"-->
                                    <?php if ($value['wats'] == 's') : ?>
                                        <a class='watsapp' class='link-email' href="#" data-toggle="modal" valores="<?php echo $value['telefone']; ?>" class="twitter"><?php echo $value['telefone']; ?> <i class="fab fa-whatsapp"></i></a>
                                    <?php endif ?>

                                    <?php if ($value['wats'] == 'n') : ?>
                                        <a class='link-email' href="#" data-toggle="modal" class="twitter"><?php echo $value['telefone']; ?></a>
                                    <?php endif ?>
                                    <!--<a class='watsapp' class='link-email' href="#" data-toggle="modal"  valores="<?php //echo $value['telefone'];    
                                                                                                                        ?>" class="twitter"><?php //echo $value['telefone'];    
                                                                                                                                                                                ?> <i class="fab fa-whatsapp"></i></a>-->

                                    <a class='link-email' href="mailto:<?php echo $value['email']; ?>"><span><i class="fa-envelope-opena"></i><?php echo $value['email']; ?></span></a>
                                </div>
                            </div>
                            </a>
                        </div>

                    <?php endif; ?>

                <?php endforeach; ?>

            </div>
        </div>
    </section>


    <section id="team" class="wow fadeInUp">
        <div class="container" data-target="#exampleModalLong">
            <div class="section-header">
                <!-- <h2>Administrativo</h2> -->
            </div>
            <div class="row">


                <?php foreach ($atendimento as $key => $value) : ?>

                    <?php if ($value['id_cargo'] == 8) : ?>

                        <div class="col-md-3 col-lg-4">
                            <div class="member" style="box-shadow: 10px 10px 10px 0px #8888884f;">
                                <a href="<?php echo BASE_URL ?>equipe-detalhe/<?php echo $value['id']; ?>/<?php echo $value['id_cargo']; ?>">
                                    <div class="pic"><img src="<?php echo BASE_URL ?>assets/images/equipe/<?php echo $value['foto']; ?>" alt=""></div>
                                </a>
                                <div class="details nosso-time">
                                    <h4><?php echo $value['nome_funcioario']; ?></h4>
                                    <span><?php echo $value['nome_cargo']; ?></span>
                                    <!--<i class="fab fa-whatsapp"></i>-->
                                    <!--data-target="#exampleModal"-->
                                    <?php if ($value['wats'] == 's') : ?>
                                        <a class='watsapp' class='link-email' href="#" data-toggle="modal" valores="<?php echo $value['telefone']; ?>" class="twitter"><?php echo $value['telefone']; ?> <i class="fab fa-whatsapp"></i></a>
                                    <?php endif ?>

                                    <?php if ($value['wats'] == 'n') : ?>
                                        <a class='link-email' href="#" data-toggle="modal" class="twitter"><?php echo $value['telefone']; ?></a>
                                    <?php endif ?>
                                    <!--<a class='watsapp' class='link-email' href="#" data-toggle="modal"  valores="<?php //echo $value['telefone'];    
                                                                                                                        ?>" class="twitter"><?php //echo $value['telefone'];    
                                                                                                                                                                                ?> <i class="fab fa-whatsapp"></i></a>-->

                                    <a class='link-email' href="mailto:<?php echo $value['email']; ?>"><span><i class="fa-envelope-opena"></i><?php echo $value['email']; ?></span></a>
                                </div>
                            </div>
                            </a>
                        </div>

                    <?php endif; ?>

                <?php endforeach; ?>

            </div>
        </div>
    </section>


    <section id="team" class="wow fadeInUp">
        <div class="container" data-target="#exampleModalLong">
            <div class="section-header">
                <!-- <h2>Vistoria</h2> -->
            </div>
            <div class="row">

                <?php foreach ($vistoriadores as $key => $value) : ?>

                    <?php if ($value['id_cargo'] == 5) : ?>

                        <div class="col-md-3 col-lg-4">
                            <div class="member" style="box-shadow: 10px 10px 10px 0px #8888884f;">
                                <a href="<?php echo BASE_URL ?>equipe-detalhe/<?php echo $value['id']; ?>/<?php echo $value['id_cargo']; ?>">
                                    <div class="pic"><img src="<?php echo BASE_URL ?>assets/images/equipe/<?php echo $value['foto']; ?>" alt=""></div>
                                </a>
                                <div class="details nosso-time">
                                    <h4><?php echo $value['nome_funcioario']; ?></h4>
                                    <span><?php echo $value['nome_cargo']; ?></span>
                                    <!--<i class="fab fa-whatsapp"></i>-->
                                    <!--data-target="#exampleModal"-->
                                    <?php if ($value['wats'] == 's') : ?>
                                        <a class='watsapp' class='link-email' href="#" data-toggle="modal" valores="<?php echo $value['telefone']; ?>" class="twitter"><?php echo $value['telefone']; ?> <i class="fab fa-whatsapp"></i></a>
                                    <?php endif ?>

                                    <?php if ($value['wats'] == 'n') : ?>
                                        <a class='link-email' href="#" data-toggle="modal" class="twitter"><?php echo $value['telefone']; ?></a>
                                    <?php endif ?>
                                    <a class='link-email' href="mailto:<?php echo $value['email']; ?>"><span><i class="fa-envelope-opena"></i><?php echo $value['email']; ?></span></a>
                                </div>
                            </div>
                            </a>
                        </div>

                    <?php endif; ?>

                <?php endforeach; ?>

            </div>
        </div>
    </section>

    <section id="team" class="wow fadeInUp">
        <div class="container" data-target="#exampleModalLong">
            <div class="section-header">
                <!-- <h2>Reclamação</h2> -->
            </div>
            <div class="row">



                <?php foreach ($reclamacao as $key => $value) : ?>

                    <?php if ($value['id_cargo'] == 6) : ?>

                        <div class="col-md-3 col-lg-4">
                            <div class="member" style="box-shadow: 10px 10px 10px 0px #8888884f;">
                                <a href="<?php echo BASE_URL ?>equipe-detalhe/<?php echo $value['id']; ?>/<?php echo $value['id_cargo']; ?>">
                                    <div class="pic"><img src="<?php echo BASE_URL ?>assets/images/equipe/<?php echo $value['foto']; ?>" alt=""></div>
                                </a>
                                <div class="details nosso-time">
                                    <h4><?php echo $value['nome_funcioario']; ?></h4>
                                    <span><?php echo $value['nome_cargo']; ?></span>
                                    <!--<i class="fab fa-whatsapp"></i>-->
                                    <!--data-target="#exampleModal"-->
                                    <?php if ($value['wats'] == 's') : ?>
                                        <a class='watsapp' class='link-email' href="#" data-toggle="modal" valores="<?php echo $value['telefone']; ?>" class="twitter"><?php echo $value['telefone']; ?> <i class="fab fa-whatsapp"></i></a>
                                    <?php endif ?>

                                    <?php if ($value['wats'] == 'n') : ?>
                                        <a class='link-email' href="#" data-toggle="modal" class="twitter"><?php echo $value['telefone']; ?></a>
                                    <?php endif ?>
                                    <a class='link-email' href="mailto:<?php echo $value['email']; ?>"><span><i class="fa-envelope-opena"></i><?php echo $value['email']; ?></span></a>
                                </div>
                            </div>
                            </a>
                        </div>

                    <?php endif; ?>

                <?php endforeach; ?>

            </div>
        </div>
    </section>






    <section id="team" class="wow fadeInUp">
        <div class="container" data-target="#exampleModalLong">
            <div class="section-header">
                <!-- <h2>Suporte TI</h2> -->
            </div>
            <div class="row">

                <?php foreach ($ti as $key => $value) : ?>

                    <?php if ($value['id_cargo'] == 9) : ?>

                        <div class="col-md-3 col-lg-4">
                            <div class="member" style="box-shadow: 10px 10px 10px 0px #8888884f;">
                                <a href="<?php echo BASE_URL ?>equipe-detalhe/<?php echo $value['id']; ?>/<?php echo $value['id_cargo']; ?>">
                                    <div class="pic"><img src="<?php echo BASE_URL ?>assets/images/equipe/<?php echo $value['foto']; ?>" alt=""></div>
                                </a>
                                <div class="details nosso-time">
                                    <h4><?php echo $value['nome_funcioario']; ?></h4>
                                    <span><?php echo $value['nome_cargo']; ?></span>
                                    <!--<i class="fab fa-whatsapp"></i>-->
                                    <!--data-target="#exampleModal"-->
                                    <?php if ($value['wats'] == 's') : ?>
                                        <a class='watsapp' class='link-email' href="#" data-toggle="modal" valores="<?php echo $value['telefone']; ?>" class="twitter"><?php echo $value['telefone']; ?> <i class="fab fa-whatsapp"></i></a>
                                    <?php endif ?>

                                    <?php if ($value['wats'] == 'n') : ?>
                                        <a class='link-email' href="#" data-toggle="modal" class="twitter"><?php echo $value['telefone']; ?></a>
                                    <?php endif ?>
                                    <a class='link-email' href="mailto:<?php echo $value['email']; ?>"><span><i class="fa-envelope-opena"></i><?php echo $value['email']; ?></span></a>
                                </div>
                            </div>
                            </a>
                        </div>

                    <?php endif; ?>

                <?php endforeach; ?>

            </div>
        </div>
    </section>





    <!-- ==========================
          Contact Section
        ============================-->
    <section id="contact" class="wow fadeInUp">

        <?php include 'includes/contatos.php'; ?>                                
        <!-- <div class="container">
            <div class="section-header">
                <h2 class="text-center">Contatos</h2> -->
                <!--<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI,</p>-->
            <!-- </div>

            <div class="row contact-info"> -->

                <!-- <div class="col-md-3"> -->
                <!--<a target="blanks" href="https://www.google.com.br/maps/place/Jomar+Imobili%C3%A1ria+e+Locadora+de+Im%C3%B3veis/@-19.8771542,-44.9899873,17z/data=!3m1!4b1!4m5!3m4!1s0xa7560972ff50a5:0x20feab5cb8482f60!8m2!3d-19.8771542!4d-44.9877986">-->
                <!-- <div class="contact-phone">
                        <a href="https://pt-br.facebook.com/jomarimobiliaria/"  target="_blank"  class="facebook"><i class="fab fa-facebook-square"></i></a>
                        <h3>Facebook</h3> -->
                <!--<address>R. Rodrigues Alves, 185 - Frei Paulo, Nova Serrana - MG, 35519-000 </address>-->
                <!-- </div>
                    </a>
                </div> -->

                <!-- <div class="col-md-6">
                    <div class="contact-phone">
                        <a href="https://www.instagram.com/jomarimobiliaria/" target="_blank" class="google-plus"><i class="fab fa-instagram"></i></i></a>
                        <h3>Instagram</h3> -->
                        <!--<p><a href="tel:(37) 3226-2132">(37) 3226-2132</a></p>-->
                    <!-- </div>
                </div> -->


                <!-- <div class="col-md-3">
                    <div class="contact-phone">
                        <a href="tel:(37)3226-2132" class="google-plus"><i class="fab fa-skype"></i></i></a>

                        <h3>Skype</h3> -->
                <!--<p><a href="tel:(37) 3226-2132">(37) 3226-2132</a></p>-->
                <!-- </div>
                </div> -->

                <!-- <div class="col-md-6">
                    <a class='watsapp' class='link-email' href="#" data-toggle="modal" valores="(37)3226-2132" class="twitter">
                        <div class="contact-phone">
                            <i class="fab fa-whatsapp"></i>
                    </a>

                    <h3>WhatsApp</h3> -->
                    <!--<p><a href="mailto:info@example.com">atendimento@jomarimobiliaria.com.br</a></p>-->
                <!-- </div>
                </a>
            </div>

        </div> -->



        <!--<div id="google-map" data-latitude="-19.9361076" data-longitude="-43.924648"></div>-->


        <div class="container">

            <div class="section-header">
                <h2 class="text-center">Fale Conosco</h2>
                <div class="msg-contato">

                </div>
                <div class="form">
                    <div id="sendmessage">Preencha o formulario, que faremos contato com você!</div>
                    <div id="errormessage"></div>
                    <form action="" method="post" role="form" class="contactForm">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="text" name="name" class="form-control" id="name_contato" placeholder="Seu nome" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                <div class="validation"></div>
                            </div>
                            <div class="form-group col-md-12">
                                <input type="email" class="form-control" name="email" id="email_contato" placeholder="Email" data-rule="email" data-msg="Please enter a valid email" />
                                <div class="validation"></div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <!--//data-msg="Please enter at least 4 chars"-->
                                <input type="text" name="tel_fixo" class="form-control" id="tel_fixo" placeholder="Telefone Fixo" data-msg="" />
                                <div class="validation"></div>
                            </div>
                            <div class="form-group col-md-6">
                                <!--Please enter a valid email;-->
                                <input type="text" class="form-control" name="tel_celular" id="tel_celular" placeholder="Telefone Celular" data-msg="" />
                                <div class="validation"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <textarea id="msg_contato" class="form-control" name="mensagem" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Mensagem"></textarea>
                            <div class="validation"></div>
                        </div>
                        <div class="text-center"><button id="button_forms" type="button">Enviar Mensagem</button></div>
                    </form>
                </div>
            </div>

        </div>

    </section>

</main>