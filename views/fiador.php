<main id="main">
    <div class="jumbotron jumbotron-fluid" style="background-image: url(../assets/images/banner/call-to-action-venha.jpg);background-position: center">
        <div class="container">
            <div class="section-header">
                <h2 class="text-center" style="color:#FFF">Garantias Locatícias</h2>
                <!--<h3 class="text-center" style="color:#FFF">PESSOA FÍSICA</h3>-->

            </div>
        </div>
    </div>


    <section id="about" class="wow fadeInUp">
        <div class="container">
            <div class="row">

                <div class="col-lg-6 content">

                    <h2>Fiador</h2>
                    <!--<h4>Pessoa Física que deseja alugar um imóvel através de fiador os documentos são :</h4>-->
                    <ul>
                        <li><i class="ion-android-checkmark-circle"></i> <b>Locatário</b> - Apresentar renda de 3 vezes o valor do aluguel mais encargos ( condomínio e IPTU ).</li>
                        <li><i class="ion-android-checkmark-circle"></i> <b>Fiador de renda</b> - Apresentar renda de 3 vezes o valor do aluguel mais os encargos ( condomínio e IPTU ), RG, CPF, Comprovante de residência, estado civil;</li>

                        <p>
                            Recolher as documentações solicitadas enviar para análise no e-mail: secretarialamoradaimoveis@gmail.com Contato: (92) 99285-9695.
                        </p>
                    </ul>


                </div>

                <div class="col-lg-6 content">
                    <h2>Caução</h2>
                    <!--<h4>Pessoa Física que deseja alugar um imóvel através de fiador os documentos são :</h4>-->
                    <ul>
                        <li><i class="ion-android-checkmark-circle"></i>Será calculado aproximadamente de 2 vezes e meia a 3 vezes o valor de aluguel mais encargos (condomínio
                            e IPTU).</li>

                        <p>
                            Recolher as documentações solicitadas enviar para análise no e-mail: secretarialamoradaimoveis@gmail.com Contato: (92) 99285-9695.
                        </p>

                        <!-- <li><i class="ion-android-checkmark-circle"></i> Nesta modalidade a seguradora será sua fiadora, e este seguro será renovado a cada 12 meses.
                            (Valor não será resgatado ao fim da locação).<br/> Gentileza entrar em contato com a xxxxx seguros TEL : xxxx-xxxx.</li> -->

                    </ul>

                </div>

                <div class="col-lg-6 content">

                    <h2>Documentos Locador</h2>
                    <!--<h4>Pessoa Física que deseja alugar um imóvel através de fiador os documentos são :</h4>-->
                    <ul>
                        <li><i class="ion-android-checkmark-circle"></i> <b>Locador</b> - Apresentar RG, CPF, Comprovante de residência, estado civil, profissão, e-mail e contato.</li>
                        <p>
                            Recolher as documentações solicitadas enviar para análise no e-mail: secretarialamoradaimoveis@gmail.com Contato: (92) 99285-9695.
                        </p>

                    </ul>

                </div>

                <div class="col-lg-6 content">

                    <h2>Documentos Locatário</h2>
                    <!--<h4>Pessoa Física que deseja alugar um imóvel através de fiador os documentos são :</h4>-->
                    <ul>
                        <li><i class="ion-android-checkmark-circle"></i> <b>Locatário</b> - Apresentar RG, CPF, Comprovante de residência, estado civil, profissão, e-mail e contato;</li>
                        <p>
                            Recolher as documentações solicitadas enviar para análise no e-mail: secretarialamoradaimoveis@gmail.com Contato: (92) 99285-9695.
                        </p>

                    </ul>

                </div>

            </div>

        </div>

        </div>
    </section><!-- #about -->


    <!--INCLUDE AREA DE CONTATOS-->
    <?php include 'includes/contatos.php'; ?>


</main>