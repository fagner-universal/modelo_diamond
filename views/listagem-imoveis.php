<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo TITULO_AUXILIAR ?> - Busca de imóveis</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">

        <!-- Favicons -->
        <link href="img/favicon.png" rel="icon">
        <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

        <link rel="shortcut icon"  href="<?php echo BASE_URL ?>assets/images/logo/favicon.ico" type="image/x-icon" />

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet"/>


        <!-- Bootstrap CSS File -->
        <link href="<?php echo BASE_URL; ?>assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>

        <!-- Libraries CSS Files -->
        <link href="<?php echo BASE_URL; ?>assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
        <link href="<?php echo BASE_URL; ?>assets/lib/animate/animate.min.css" rel="stylesheet"/>
        <link href="<?php echo BASE_URL; ?>assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet"/>
        <link href="<?php echo BASE_URL; ?>assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet"/>
        <link href="<?php echo BASE_URL; ?>assets/lib/magnific-popup/magnific-popup.css" rel="stylesheet"/>
        <link href="<?php echo BASE_URL; ?>assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/lib/fonts/flaticon/font/flaticon.css">

        <!--ICONES-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous"/>
        <!--<link rel="stylesheet" type="text/css" href="<?php //echo BASE_URL     ?>assets/fonts/font-awesome/css/font-awesome.min.css">-->

        <!-- Main Stylesheet File -->
        <link href="<?php echo BASE_URL; ?>assets/css/style.css" rel="stylesheet"/>

        <!--ICONES GOOGLE-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>

        <!-- import  Clustering de marcadores -->
        <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>

        <!-- <link href="https://fonts.googleapis.com/css?family=Dancing+Script|Tangerine" rel="stylesheet"> -->

        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/bootstrap/css/bootstrap.min.css"/>

        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/style1.css"/>

        <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/style.css" />

        <link href="<?php echo BASE_URL ?>assets/css/busca/estilo.css" rel="stylesheet">

        <style>
            .btn-danger {
                color: var(--cor-primaria);
                background-color: rgba(220, 53, 69, 0);
                border-color: var(--cor-primaria);
            }
        </style>

        <?php include('includes/scripts_top.php') ?>

    </head>

    <body>
        <!--==========================
           Header
        ============================-->


        <?php include 'includes/topo.php'; ?>
        <!--==========================
                  Intro Section
                ============================-->

        <main id="main" style="padding-top: 50px; background-color: #f6f6f6;">
            <div class="container">
                <div class="row" style="padding-top: 50px">
                    <h4  class="section-title text-center" style="display: none"><span id="total-imoveis">0</span> Imoveis encontrados</h4>
                </div>
                <div class="row" style="background-color: #cccccc1a">
                    <a href="#" class="back-to-top menu-busca-mobile" style="display: inline;padding-top: 12px;
                       color: #fff;">
                        <i class="fa fa-sort-amount-asc" aria-hidden="true"></i>

                    </a>
                </div>

                <div class="row">
                    <div id="corpo-filtro" class="col-12 col-sm-12 col-md-4 col-md-3 col-xl-3"
                         style="background-color:#f6f6f6;
                         padding-top: 10px;
                         border-radius: 10px;
                         padding-bottom: 10px;
                         max-height: 1500px;">

                        <div class="row" style="background-color:var(--cor-primaria);
                             padding-top: 10px;
                             border-radius: 10px;
                             padding-bottom: 10px;
                             max-height: 1500px;">

                            <div class="col-12">
                                <form>
                                    <div class="form-group" style="display:none">
                                        <label for="tipo">Finalidade</label>
                                        <select  class="custom-select"  id="finalidade-input-busca" >
                                            <option value="aluguel" >Alugar</option>
                                            <option  value="venda">Comprar</option>
                                        </select>
                                    </div>
                                    <div class="form-group" style="display:none">
                                        <label for="tipo">Código</label>
                                        <input id="codigo-input" class="form-control" type="text" placeholder="Código">
                                    </div>
                                    <div class="form-group">
                                        <label for="tipo">Cidades</label>
                                        <select class="custom-select" id="cidade-input-busca">
                                            <option selected>Apartamento</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tipo">Bairros</label>
                                        <select class="custom-select" id="bairro-input-busca">
                                            <option value="todo-os-bairro">Todos</option>
                                        </select>

                                        <!--<label for="tipo">Tipos selecionados</label>-->

                                        <div id="list_bairros" class="col-12">
                                            <!--
                                                                                <span class="badge badge-secondary marcador-de-busca">Centro x </span>
                                                                                <span class="badge badge-secondary marcador-de-busca">Barreiro x </span>
                                                                                <span class="badge badge-secondary marcador-de-busca">Savassi x </span>-->
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="tipo">Tipo de Imóvel</label>
                                        <select class="custom-select" id="tipo-input-busca">
                                            <option value="todos-os-tipos">Todos</option>
                                        </select>

                                        <div  id="list_tipos" class="col-12">
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="tipo">Condomínio</label>
                                        <select class="custom-select corpo-cond-busca" id="corpo-cond-busca">
                                            <option selected>Todos condomínios</option>
                                        </select>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-12 col-sm-12 col-md-6 col-md-6 col-xl-6">
                                            <label for="tipo">Valor Mínimo</label>
                                            <input id="valor-minimo" class="form-control" type="text" placeholder="R$ 0,00">
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-6 col-md-6 col-xl-6">
                                            <label for="tipo">Valor Máximo</label>
                                            <input id="valor-maximo" class="form-control" type="text" placeholder="R$ 0,00">
                                        </div>
                                    </div>

                                    <div class="form-row" style="padding-top:10px ">
                                        <div class="col-12 col-sm-12 col-md-6 col-md-6 col-xl-6">
                                            <label for="tipo">Área Mínima</label>
                                            <input id="area-minimo" class="form-control" type="text" placeholder="M²">
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-6 col-md-6 col-xl-6">
                                            <label for="tipo">Área Máxima</label>
                                            <input id="area-maximo" class="form-control" type="text" placeholder="M²" >
                                        </div>
                                    </div>

                                    <div class="form-row" style="padding: 20px">
                                        <div class="col-12 ">
                                            <label for="tipo">Quartos</label>
                                        </div>
                                        <div id="quartos_check" class="btn-group" role="group" aria-label="quartos">
                                            <button id="quarto_check_1" type="button" class="btn btn-secondary botao-quarto">1</button>
                                            <button id="quarto_check_2"  type="button" class="btn btn-secondary botao-quarto">2</button>
                                            <button id="quarto_check_3"  type="button" class="btn btn-secondary botao-quarto">3</button>
                                            <button id="quarto_check_4"  type="button" class="btn btn-secondary botao-quarto">4</button>
                                            <button id="quarto_check_5"  type="button" class="btn btn-secondary botao-quarto">5+</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-8 col-xl-9">
                        <div class="container-fluid">
                           
                            <div class="row">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item">
                                        <a id="aba-finalidade-aluguel" class="nav-link aba-finalidade active">Alugar</a>
                                    </li>
                                    <li class="nav-item">
                                        <a id="aba-finalidade-venda" class="nav-link aba-finalidade" >Comprar</a>
                                    </li>


                                </ul>
                               
                                <div class="container-fluid corpo-imoveis-busca">
                                    <!--EXEMPLO DE CARD-->


                                </div>
                                <div class="load-gif-imoveis text-center">
                                    <img id="load-gif"  style="display:none" class="rounded mx-auto" src="<?php echo BASE_URL ?>assets/images/preloader_1.gif" />
                                    <p class="text-center">Carregando Imóveis...</p>
                                </div>
                                <div class="container-fluid">
                                    <nav  id="paginacao"  aria-label="Page navigation example">
                                        <ul class="pagination justify-content-center">


                                        </ul>
                                    </nav>
                                </div>

                            </div>           
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </main>

    <!--==========================
        Footer
   ============================-->
    <?php include 'includes/footer.php'; ?>


    <!--     Botão para acionar modal 
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalExemplo">
            Abrir modal de demonstração
        </button>-->

    <!-- Modal -->
    <div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Imóveis encontrados</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="load-gif-imoveis">
                        <img id="load-gif"  style="display:block" class="rounded mx-auto" src="<?php echo BASE_URL ?>assets/img/preloader.gif" />
                        <p class="text-center">Carregando Imóveis...</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>


    <a href="#" class="back-to-top">
        <i class="fa fa-chevron-up"></i>
    </a>
    <div id="preloader"></div>


    <!--URL BASE DO SITE-->
    <script src="<?php echo BASE_URL ?>assets/js/base_url.js"></script>

    <!--
         JavaScript Libraries 
        <script src="<?php //echo BASE_URL     ?>assets/lib/jquery/jquery.min.js"></script>
        <script src="<?php //echo BASE_URL     ?>assets/lib/jquery/jquery-migrate.min.js"></script>-->


    <!-- JavaScript Libraries -->
    <script src="<?php echo BASE_URL; ?>assets/lib/jquery/jquery.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/jquery/jquery-migrate.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/easing/easing.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/superfish/hoverIntent.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/superfish/superfish.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/wow/wow.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/owlcarousel/owl.carousel.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/magnific-popup/magnific-popup.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/sticky/sticky.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/mask/jquery.mask.js"></script>


    <script src="<?php echo BASE_URL; ?>assets/js/main.js"></script>
    <!-- OBJETO IMOVEL-->
    <script src="<?php echo BASE_URL ?>assets/js/obj/obj_imovel.js"></script>

    <!--JAVA SCRIPT FOMULARIO DO HEADER-->
    <script src="<?php echo BASE_URL ?>assets/js/busca/busca.js"></script>

    <!--JAVA SCRIPT FOMULARIO DO HEADER-->
    <script src="<?php echo BASE_URL ?>assets/js/busca/gerarUrl.js"></script>


    <?php include('includes/scripts_bottom.php') ?>




</body>
</html>