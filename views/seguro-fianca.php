
<main id="main">


    <div class="jumbotron jumbotron-fluid" style="background-image: url(../assets/images/banner/call-to-action-venha.jpg);background-position: center"  >
        <div class="container" >
            <div class="section-header" >
                <h2 class="text-center" style="color:#FFF">COMO ALUGAR UM IMÓVEL ATRÁVES DO SEGURO FIANÇA</h2>
<!--                <h3 style="color:#FFF">Para locatário com carteira assinada os documentos são :</h3>-->
            </div>
        </div>
    </div>

    <section id="about" class="wow fadeInUp">
        <div class="container">
            <div class="row">

                <div class="col-lg-6 content">

                    <h2>Locatário Pessoa Física</h2>
                    <!--<h4>Pessoa Física que deseja alugar um imóvel através de fiador os documentos são :</h4>-->  
                    <ul>
                       <li><i class="ion-android-checkmark-circle"></i> Cópia RG e CPF</li>
                        <li><i class="ion-android-checkmark-circle"></i> Se casado, documentos do cônjuge</li>
                        <li><i class="ion-android-checkmark-circle"></i> Cópia do comprovante de residência</li>
                        <li><i class="ion-android-checkmark-circle"></i> Se pagar aluguel atualmente, apresentar os três últimos recibos pagos</li>
                        <li><i class="ion-android-checkmark-circle"></i> Carteira assinada </li>
                        <li><i class="ion-android-checkmark-circle"></i> Cópia da carteira de trabalho (página da foto, frente e verso, registro de trabalho e última atualização salarial)</li>
                        <li><i class="ion-android-checkmark-circle"></i> Cópia dos últimos três contracheques </li>
                        <li><i class="ion-android-checkmark-circle"></i> Caso faça declaração de imposto de renda, apresentar cópia do último exercício e o comprovante de entrega</li>
                        <li><i class="ion-android-checkmark-circle"></i> Preencher ficha</li>
                    </ul>
                    
                    <!--<a href="#contact" class="btn btn-primary btBuscar">Cadastrar imóvel</a>-->

                </div>

                <div class="col-lg-6 content">

                    <!--<h2>Locatário autônomo</h2>-->
                    <h2>Locatário Autônomo, Empresário ou Profissional Liberal</h2>  

                    <ul>
                        <li><i class="ion-android-checkmark-circle"></i> Cópia RG e CPF</li>
                        <li><i class="ion-android-checkmark-circle"></i> Se casado, documentos do cônjuge</li>
                        <li><i class="ion-android-checkmark-circle"></i> Cópia do comprovante de residência</li>
                        <li><i class="ion-android-checkmark-circle"></i> Se pagar aluguel atualmente, apresentar os três últimos recibos pagos</li>
                        <li><i class="ion-android-checkmark-circle"></i> Extrato bancários dos últimos 06 meses</li>
                        <li><i class="ion-android-checkmark-circle"></i> Caso seja empresário, apresentar cópia do contrato social</li>
                        <li><i class="ion-android-checkmark-circle"></i> Caso faça declaração de imposto de renda, apresentar cópia do último exercício e o comprovante de entrega</li>
                        <li><i class="ion-android-checkmark-circle"></i> Preencher ficha</li>
                    </ul>
                    <!--<a href="#contact" class="btn btn-primary btBuscar">Cadastrar imóvel</a>-->

                </div>
                
                
                <div class="col-lg-6 content">

                    <h2>Locatário Pessoa Jurídica</h2>
                    <h4>DOCUMENTAÇÃO DE TODOS OS SÓCIOS QUE CONSTAREM NO CONTRATO SOCIAL:</h4>  
                    <ul>
                       <li><i class="ion-android-checkmark-circle"></i> Cópia RG e CPF</li>
                        <li><i class="ion-android-checkmark-circle"></i> Cópia do comprovante de residência</li>
                        <li><i class="ion-android-checkmark-circle"></i> Cópia da declaração de imposto de renda do último exercício completa mais o recibo de entrega</li>
                     
                    </ul>
                    
                    <!--<a href="#contact" class="btn btn-primary btBuscar">Cadastrar imóvel</a>-->

                </div>
                
                  <div class="col-lg-6 content">

                    <h2>Empresa Optante Simples Nacional / Simei</h2>
                    <!--<h4>DOCUMENTAÇÃO DE TODOS OS SÓCIOS QUE CONSTAREM NO CONTRATO SOCIAL:</h4>-->  
                    <ul>
                       <li><i class="ion-android-checkmark-circle"></i> Contrato social e alterações</li>
                        <li><i class="ion-android-checkmark-circle"></i> Cartão CNPJ</li>
                        <li><i class="ion-android-checkmark-circle"></i> 06 últimos DAS do SIMPLES com os respectivos comprovantes de pagamento</li>
                        <li><i class="ion-android-checkmark-circle"></i> Declaração anual do simples DASN na íntegra, mais recibo de entrega</li>
                        <li><i class="ion-android-checkmark-circle"></i> Extrato simplificado completo extraído do sistema de cálculo do simples, referente ao último mês</li>
                        <li><i class="ion-android-checkmark-circle"></i> Preencher ficha</li>
                     
                    </ul>
                    
                    <!--<a href="#contact" class="btn btn-primary btBuscar">Cadastrar imóvel</a>-->

                </div>
                
                  <div class="col-lg-6 content">

                    <h2>Empresas Optantes Pelo Lucro Presumido</h2>
                    <!--<h4>DOCUMENTAÇÃO DE TODOS OS SÓCIOS QUE CONSTAREM NO CONTRATO SOCIAL:</h4>-->  
                    <ul>
                       <li><i class="ion-android-checkmark-circle"></i> Contrato social e alterações</li>
                        <li><i class="ion-android-checkmark-circle"></i> Cartão CNPJ</li>
                        <li><i class="ion-android-checkmark-circle"></i> 06 últimos DAS do SIMPLES com os respectivos comprovantes de pagamento</li>
                        <li><i class="ion-android-checkmark-circle"></i> Declaração anual do simples DASN na íntegra, mais recibo de entrega</li>
                        <li><i class="ion-android-checkmark-circle"></i> Extrato simplificado completo extraído do sistema de cálculo do simples, referente ao último mês</li>
                        <li><i class="ion-android-checkmark-circle"></i> Preencher ficha</li>
                     
                    </ul>
                   
                    <!--<a href="#contact" class="btn btn-primary btBuscar">Cadastrar imóvel</a>-->
                </div>
                 <div class="col-lg-6 content">

                    <h2>Empresas Sem Fins Lucrativos</h2>
                    <!--<h4>DOCUMENTAÇÃO DE TODOS OS SÓCIOS QUE CONSTAREM NO CONTRATO SOCIAL:</h4>-->  
                    <ul>
                       <li><i class="ion-android-checkmark-circle"></i> Contrato social e alterações</li>
                        <li><i class="ion-android-checkmark-circle"></i> Cartão CNPJ</li>
                        <li><i class="ion-android-checkmark-circle"></i> ECF (Escrituração Contábil Fiscal) dos dois últimos exercícios, na íntegra</li>
                        <li><i class="ion-android-checkmark-circle"></i> Estatuto social e ata da última eleição de diretoria</li>
                        <li><i class="ion-android-checkmark-circle"></i> Balanço completo do último exercício com ativo, passivo e DRE (Demonstração Do Resultado do Exercício), acumulado, conciliado e assinado pelo Contador</li>
                        <li><i class="ion-android-checkmark-circle"></i> Balanço completo do último exercício extraído da ECD (Escrituração Contábil Digital), com a página de protocolo (Ativo, Passivo e Demonstração do Resultado do exercício)</li>
                        <li><i class="ion-android-checkmark-circle"></i> Balancete completo do exercício vigente com ativo, passivo e DRE (Demonstração do Resultado do Exercício), acumulado, conciliado e Assinado pelo contador</li>
                     <li><i class="ion-android-checkmark-circle"></i> Preencher ficha</li>
                    </ul>
                   
                    <!--<a href="#contact" class="btn btn-primary btBuscar">Cadastrar imóvel</a>-->
                </div>
                
                
                
                
                <div class="col-lg-12 content">

                    <h2 style="color: green">Observações</h2>
                    <!--<h4>Pessoa Física que deseja alugar um imóvel através de fiador os documentos são :</h4>-->  
                    <ul>
                        <li><i class="ion-android-checkmark-circle"></i> É cobrado uma porcentagem sobre o valor do aluguel pela seguradora.</li>
                    </ul>
                    <!--<a href="#contact" class="btn btn-primary btBuscar">Cadastrar imóvel</a>-->
                </div>

            </div>

        </div>
    </section><!-- #about -->

    <!--INCLUDE AREA DE CONTATOS-->
    <?php include 'includes/contatos.php'; ?>

</main>

