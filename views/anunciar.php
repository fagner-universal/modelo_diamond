

<main id="main">

    <!--==========================
      About Section
    ============================-->
    <!-- <section id="about" class="wow fadeInUp">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 about-img"> -->
                    <!--<img src="<?php echo BASE_URL ?>assets/images/about-img.jpg" alt="">-->
                    <!-- <a href="www.google.com">
                        <img src="<?php echo BASE_URL ?>assets/images/imoveis/anuncie-img.jpg" alt="">
                    </a>
                </div>

                <div class="col-lg-6 content" style="text-align: justify">

                    <h2>Anuncie seu Imóvel</h2> -->
                    
                    
                    <!--//justificar o texto-->
                    <!-- <p>Na Aceti Imóveis todos os clientes são atendidos de forma diferenciada, sabemos da importância de estar sempre a postos para agir com prontidão. Por isso, colocamos à sua disposição uma equipe de consultoria qualificada que avalia seu imóvel, cuida da divulgação do mesmo e fomenta negócios conforme as particularidades dos clientes. 
                        Venha tomar um café conosco ou agende uma visita no seu imóvel com esse time de 23 anos de mercado no Buritis!</p>   -->
                    <!--<h3>Aqui seu imóvel ganha visibilidade na mídia com estratégias diferenciadas para atingir o público ideal, além de uma consultoria exclusiva do início ao fim da negociação..</h3>-->

<!--                    <ul>
                        <li><i class="ion-android-checkmark-circle"></i> Aluguel garantido</li>
                        <li><i class="ion-android-checkmark-circle"></i> Preservação do imóvel</li>
                        <li><i class="ion-android-checkmark-circle"></i> Acessoria Jurídica</li>
                        <li><i class="ion-android-checkmark-circle"></i> Setor de cobrança</li>
                        <li><i class="ion-android-checkmark-circle"></i> Divulgação do imóvel</li>
                        <li><i class="ion-android-checkmark-circle"></i> E muito mais...</li>
                    </ul>-->
                    <!--<a href="#contact" class="btn btn-primary btBuscar">Cadastrar imóvel</a>-->

                <!-- </div>
                
            </div>
          

        </div>
    </section>#about -->

    <!--    ==========================
          Contact Section
        ============================-->
    <section id="contact" class="wow fadeInUp" style="padding-top: 0px">
        <div class="container">
            <div class="section-header">
                <h2>Cadastrar Imóvel</h2>
                <h4>Preencha o formulário com os dados do seu imóvel.</h4>

                

                <div class="form">
                    <div id="sendmessage">Your message has been sent. Thank you!</div>
                    <div id="errormessage"></div>
                    <form action="" method="post" role="form" class="contactForm">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="validationTooltip03">Nome:</label>
                                <input type="text" name="name" class="form-control" id="name_anunciar" placeholder="Seu nome" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                <div class="validation"></div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="validationTooltip03">Email: </label>
                                <input type="email" class="form-control" name="email" id="email_anuciar" placeholder="Email" data-rule="email" data-msg="Please enter a valid email" />
                                <div class="validation"></div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="validationTooltip03">Telefone Fixo: </label>
                                <input type="text" name="tel_fixo" class="form-control" id="fixo_anuciar" placeholder="Telefone Fixo" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                <div class="validation"></div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="validationTooltip03">Telefone Celular: </label>
                                <input type="text" class="form-control" name="tel_celular" id="celular_anuciar" placeholder="Telefone Celular" data-rule="email" data-msg="Please enter a valid email" />
                                <div class="validation"></div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="validationTooltip03">Endereço: </label>
                                <input type="text" name="endereco" class="form-control" id="endereco_anunciar" placeholder="Endereço do imóvel" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                <div class="validation"></div>
                            </div>
                            <div class="form-group col-md-1">
                                <label for="validationTooltip03">N°: </label>
                                <input type="text" name="numero" class="form-control" id="n_anunciar" placeholder="N°" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                <div class="validation"></div>
                            </div>
                            <div class="form-group col-md-5">
                                <label for="validationTooltip03">Bairro: </label>
                                <input type="text" name="bairro" class="form-control" id="bairro_anunciar" placeholder="Bairro" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                <div class="validation"></div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="validationTooltip03">Cidade: </label>
                                <input type="text" name="cidade" class="form-control" id="cidade_anunciar" placeholder="Cidade" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                <div class="validation"></div>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="validationTooltip03">Estado: </label>
                                <input type="text" name="estado" class="form-control" id="estado_anunciar" placeholder="ex: MG" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                <div class="validation"></div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="validationTooltip03">CEP: </label>
                                <input type="text" name="cep" class="form-control" id="cep_anunciar" placeholder="0000-0000" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                <div class="validation"></div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="validationTooltip03">Tipo de Imóvel:</label>
                                <select id="tipo_imovel_anunciar" class="custom-select" required>
                                    <option value="">Tipo de Imóvel</option>


                                </select>
                                <div class="invalid-feedback">Example invalid custom select feedback</div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="validationTooltip03">Finalidade: </label>
                                <select id="tipo_imovel_finalidade" class="custom-select" required>
                                    <option value="">Selecione a Finalidade</option>
                                    <option value="Aluguel">Aluguel</option>
                                    <option value="Venda">Venda</option>

                                </select>
                                <div class="invalid-feedback">Example invalid custom select feedback</div>
                            </div>
                            <!--                            <div class="form-group col-md-4">
                                                            <select class="custom-select" required>
                                                                <option value="">Open this select menu</option>
                                                                <option value="1">One</option>
                                                                <option value="2">Two</option>
                                                                <option value="3">Three</option>
                                                            </select>
                                                            <div class="invalid-feedback">Example invalid custom select feedback</div>
                                                        </div>-->
                        </div>
                        <div class="form-group">

                        </div>

                        <div class="form-group">
                            <label for="validationTooltip03">Mensagem: </label>
                            <textarea class="form-control" name="mensagem" id="mensagem_anunciar" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Mensagem"></textarea>
                            <div class="validation"></div>
                        </div>
                        <div class="msg-contato">

                        </div>
                        <div class="text-center "><button id="button_forms_anunciar"  type="button" onclick="return gtag_report_conversion('tel:3133786722')" >Enviar</button></div>
                    </form>
                </div>
            </div>
            <!--            <div class="row contact-info">
            
                            <div class="col-md-3">
                                <a target="blanks" href="https://www.google.com.br/maps/place/Jomar+Imobili%C3%A1ria+e+Locadora+de+Im%C3%B3veis/@-19.8771542,-44.9899873,17z/data=!3m1!4b1!4m5!3m4!1s0xa7560972ff50a5:0x20feab5cb8482f60!8m2!3d-19.8771542!4d-44.9877986">
                                <div class="contact-phone">
                                    <a href="#" class="facebook"><i class="fab fa-facebook-square"></i></a>
                                    <h3>Facebook</h3>
                                    <address>R. Rodrigues Alves, 185 - Frei Paulo, Nova Serrana - MG, 35519-000 </address>
                                </div>
                                </a>
                            </div>
            
                            <div class="col-md-3">
                                <div class="contact-phone">
                                    <a href="#" class="google-plus"><i class="fab fa-instagram"></i></i></a>
                                    <h3>Instagram</h3>
                                    <p><a href="tel:(37) 3226-2132">(37) 3226-2132</a></p>
                                </div>
                            </div>
            
            
                            <div class="col-md-3">
                                <div class="contact-phone">
                                    <a href="#" class="google-plus"><i class="fab fa-skype"></i></i></a>
                                    <h3>Skype</h3>
                                    <p><a href="tel:(37) 3226-2132">(37) 3226-2132</a></p>
                                </div>
                            </div>
            
                            <div class="col-md-3">
                                <div class="contact-phone">
                                    <i class="fab fa-whatsapp"></i></a>
                                    <h3>WhatsApp</h3>
                                    <p><a href="mailto:info@example.com">atendimento@jomarimobiliaria.com.br</a></p>
                                </div>
                            </div>
            
                        </div>-->
        </div>

    </section> 
    <!--#contact--> 

</main>
