<main id="main">
    <div class="jumbotron jumbotron-fluid" style="background-image: url(../assets/images/banner/call-to-action-venha.jpg);background-position: center"  >
        <div class="container" >
            <div class="section-header" >
                <h2 class="text-center" style="color:#FFF">COMO ALUGAR UM IMÓVEL ATRAVÉS DO TÍTULO DE CAPITALIZAÇÃO</h2>
                <!--<h3 style="color:#6c757d"></h3>-->

            </div>
        </div>
    </div>
    <section id="about" class="wow fadeInUp">
        <div class="container">
            <div class="row">

                <div class="col-sm-12 col-md-6 col-lg-6 content">
                    <div class="col-12 content">
                        <h2>Locatário Pessoa Física</h2>
                    </div>
                    <div class="col-lg-12 content">
                        <!--<h4>Pessoa Física</h4>-->
                        <!--<h4>Pessoa Física que deseja alugar um imóvel através de fiador os documentos são :</h4>-->  
                        <ul>
                            <li><i class="ion-android-checkmark-circle"></i> Cópia do RG e CPF</li>
                            <li><i class="ion-android-checkmark-circle"></i> Preencher ficha</li>
                            <!--<li><i class="ion-android-checkmark-circle"></i> Tudo com segurança, tranparencia</li>-->
                        </ul>
                        <!--<a href="#contact" class="btn btn-primary btBuscar">Cadastrar imóvel</a>-->
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 content">
                    <div class="col-12 content">
                        <h2>Locatário Pessoa Jurídica</h2>
                    </div>
                    <div class="col-lg-12 content">
                        <!--<h4>ocatário Pessoa Jurídica</h4>-->
                        <!--<h4>Pessoa Física que deseja alugar um imóvel através de fiador os documentos são :</h4>-->  
                        <ul>
    <!--                        <li><i class="ion-android-checkmark-circle"></i> Cópia do RG e CPF</li>
                            <li><i class="ion-android-checkmark-circle"></i> Preencher ficha</li>-->
                            <li><i class="ion-android-checkmark-circle"></i> Contrato social e última alteração contratual</li>
                            <li><i class="ion-android-checkmark-circle"></i> Cópia do RG e CPF dos sócios da empresa</li>
                            <li><i class="ion-android-checkmark-circle"></i> Preencher ficha</li>
                            <!--<li><i class="ion-android-checkmark-circle"></i> Tudo com segurança, tranparencia</li>-->
                        </ul>
                        <!--<a href="#contact" class="btn btn-primary btBuscar">Cadastrar imóvel</a>-->
                    </div>

                </div>

                <div class="col-lg-12 content">

                    <h2 style="color: green">Observações:</h2>
                    <!--<h4>Pessoa Física que deseja alugar um imóvel através de fiador os documentos são :</h4>-->  
                    <ul>

                        <li><i class="ion-android-checkmark-circle"></i>O Título será calculado com base no valor do aluguel</li>
                        <li><i class="ion-android-checkmark-circle"></i>Ao sair do imóvel o título poderá ser resgatado</li>

                    </ul>
                    <!--<a href="#contact" class="btn btn-primary btBuscar">Cadastrar imóvel</a>-->
                </div>
            </div>
        </div>
    </section><!-- #about -->


    <!--INCLUDE AREA DE CONTATOS-->
    <?php include 'includes/contatos.php'; ?>


</main>

