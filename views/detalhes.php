<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo $viewData['titlePagina']; ?></title>

    <meta property="og:title" content="<?php echo $imovel->tipo . ' | ' . $imovel->cidade . ' | ' . $imovel->bairro ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="<?php echo $imovel->titulo ?>" />
    <meta property="og:url" content="<?php echo BASE_URL . 'detalhe-imovel/' .  $imovel->codigo ?>" />
    <meta property="og:image" content="<?php echo $imovel->urlfotoprincipal ?>" />

    <meta content="<?php echo $imovel->titulo ?> " name="description">

    <!-- Favicons -->
    <!--        <link href="img/favicon.png" rel="icon">
                <link href="img/apple-touch-icon.png" rel="apple-touch-icon">-->

    <link rel="shortcut icon" href="<?php echo BASE_URL ?>assets/images/logo/favicon.ico" type="image/x-icon" />

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet" />

    <!-- Bootstrap CSS File -->
    <link href="<?php echo BASE_URL; ?>assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Libraries CSS Files -->
    <link href="<?php echo BASE_URL; ?>assets/lib/font-awesome/css/font-awesome.min.css?teste=sdsdsdsdsd" rel="stylesheet" />
    <link href="<?php echo BASE_URL; ?>assets/lib/animate/animate.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_URL; ?>assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_URL; ?>assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_URL; ?>assets/lib/magnific-popup/magnific-popup.css" rel="stylesheet" />
    <link href="<?php echo BASE_URL; ?>assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet" />


    <!--ICONES-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous" />
    <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>assets/fonts/font-awesome/css/font-awesome.min.css">

    <!-- Main Stylesheet File -->
    <link href="<?php echo BASE_URL; ?>assets/css/style.css" rel="stylesheet" />

    <!-- =======================================================
          Theme Name: Reveal
          Theme URL: https://bootstrapmade.com/reveal-bootstrap-corporate-template/
          Author: BootstrapMade.com
          License: https://bootstrapmade.com/license/
        ======================================================= -->

    <!--ICONES GOOGLE-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />

    <!-- import  Clustering de marcadores -->
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>

    <!-- <link href="https://fonts.googleapis.com/css?family=Dancing+Script|Tangerine" rel="stylesheet"> -->

    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/bootstrap/css/bootstrap.min.css" />

    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/style1.css" />

    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/style.css" />

    
    <style>
        .icon-compartilhar {
            font-size: 20px;
            color: var(--cor-primaria);
            font-size: 42px;

        }

        .breadcrumb-item+.breadcrumb-item::before {
            display: inline-block;
            padding-right: .5rem;
            color: #FFF;
            content: "/";
        }
    </style>
    <?php include('includes/scripts_top.php') ?>

</head>

<body id="body">

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:#00d45e;color:#FFF">
                    <h5 class="modal-title" id="exampleModalLabel"><i class="fab fa-whatsapp"></i> Iniciar conversa no WhatsApp</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mensagem</label>
                            <input id="text-wats" type="text" class="form-control" id="" aria-describedby="emailHelp" placeholder="">
                            <!--<small id="emailHelp" class="form-text text-muted">Mensagem.</small>-->
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" style="background-color:#00d45e;color:#FFF" data-dismiss="modal">Fechar</button>
                    <button id="watsapp" style="background-color:#00d45e;color:#FFF" type="button" class="btn btn-primary">Enviar</button>
                </div>
            </div>
        </div>
    </div>

    <?php include('includes/topo.php') ?>

    <span id="codigo-text" style="display: none;"><?php echo $imovel->codigo ?></span>

    <main id="main" style="padding:20px;">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="background-color:var(--cor-primaria)">
                <li class="breadcrumb-item"><a style="color:#FFF" href="<?php echo BASE_URL ?>"><?php echo TITULO_AUXILIAR ?></a></li>
                <li class="breadcrumb-item"><a style="color:#FFF" href="<?php echo BASE_URL ?><?php echo ($imovel->finalidade == 'Aluguel' ? 'aluguel' : 'venda') ?>/<?php echo str_replace(' ', '-', mb_strtolower($imovel->tipo)) ?>/todas-as-cidades/todos-os-bairros/1-quartos/0-suite-ou-mais/0-vaga-ou-mais/0-banheiro-ou-mais/sem-portaria-24horas/sem-area-lazer/sem-dce/sem-mobilia/sem-area-privativa/sem-area-servico/sem-box-despejo/sem-circuito-tv/?valorminimo=0&valormaximo=0&pagina=1"><?php echo $imovel->finalidade ?></a></li>
                <li class="breadcrumb-item"><a style="color:#FFF" href="<?php echo BASE_URL ?><?php echo ($imovel->finalidade == 'Aluguel' ? 'aluguel' : 'venda') ?>/<?php echo str_replace(' ', '-', mb_strtolower($imovel->tipo)) ?>/<?php echo str_replace(' ', '-', mb_strtolower($imovel->cidade)) ?>/todos-os-bairros/1-quartos/0-suite-ou-mais/0-vaga-ou-mais/0-banheiro-ou-mais/sem-portaria-24horas/sem-area-lazer/sem-dce/sem-mobilia/sem-area-privativa/sem-area-servico/sem-box-despejo/sem-circuito-tv/?valorminimo=0&valormaximo=0&pagina=1"><?php echo $imovel->tipo ?></a></li>
                <li class="breadcrumb-item active" aria-current="page"> <span style="color:#FFF"><?php echo $imovel->titulo ?></span></li>
            </ol>

            <a href="javascript:history.back()" style="color:#000"><i class="fa fa-arrow-left"></i> Voltar para busca</a>

        </nav>



        <section id="services">
            <div id="detalhes-foco" class="container-fluid">
                <a href="javascript:history.back()">Voltar</a>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <div class="section-header">
                            <h2 style="margin-top:0px;padding-bottom:0px;">Fotos do Imóvel</h2>
                        </div>
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <a href="<?php echo $value->url; ?>" class="portfolio-popup">

                                    <?php foreach ($imovel->fotos as $key => $value) : ?>

                                        <div class="carousel-item <?php echo $key == 0 ? "active" : "" ?>">
                                            <img class="d-block w-100" src="<?php echo $value->url ?>" alt="First slide">

                                        </div>

                                    <?php endforeach; ?>
                                </a>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-3">

                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <!--<div class="icon"> <i class="fas fa-dollar-sign"></i></div>-->
                                <span class="badge badge-primary badge-light">Código</span>
                                <span class="badge badge-primary badge-light" style="font-size:20px"><?php echo $imovel->codigo; ?></span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <div class="icon"> <i class="fas fa-dollar-sign"></i></div>
                                <span class="badge badge-primary badge-light">Valor</span>
                                <span class="badge badge-primary badge-light" style="font-size:20px"><?php echo $imovel->valor; ?></span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <div class="icon"> <i class="fas fa-dollar-sign"></i></div>
                                <span class="badge badge-primary badge-light">Valor m2</span>
                                <span class="badge badge-primary badge-light" style="font-size:20px">R$ <?php echo $imovel->valorm2; ?></span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <img class="mr-1" style="width: 25px" src="<?php echo BASE_URL ?>assets/images/icons/preco.svg" alt="Generic placeholder image">
                                <span class="badge badge-primary badge-light">Valor Condomínio</span>
                                <span class="badge badge-primary badge-light"><?php echo $imovel->valorcondominio; ?></span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <img class="mr-1" style="width: 25px" src="<?php echo BASE_URL ?>assets/images/icons/preco.svg" alt="Generic placeholder image">
                                <span class="badge badge-primary badge-light">Tipo</span>
                                <span class="badge badge-primary badge-light"><?php echo $imovel->tipo; ?></span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <img class="mr-1 iconeListaCardListagem" src="<?php echo BASE_URL ?>assets/images/icons/quartos.svg" alt="Generic placeholder image">
                                <span class="badge badge-primary badge-light">Quartos</span>
                                <span class="badge badge-primary badge-light"><?php echo $imovel->numeroquartos; ?></span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <img class="mr-1 iconeListaCardListagem" src="<?php echo BASE_URL ?>assets/images/icons/banheiro1.png" alt="Generic placeholder image">
                                <span class="badge badge-primary badge-light">Banheiros</span>
                                <span class="badge badge-primary badge-light"><?php echo $imovel->numerobanhos; ?></span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <i class="fas fa-shower"></i>
                                <span class="badge badge-primary badge-light">Suites</span>
                                <span class="badge badge-primary badge-light"><?php echo $imovel->numerosuites; ?></span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <img class="mr-1 iconeListaCardListagem" src="<?php echo BASE_URL ?>assets/images/icons/car-solid.svg" alt="Generic placeholder image">
                                <span class="badge badge-primary badge-light">Vagas</span>
                                <span class="badge badge-primary badge-light"><?php echo $imovel->numerovagas; ?></span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <i class="fas fa-drafting-compass"></i>
                                <!--<img class="mr-1 iconeListaCardListagem" src="<?php echo BASE_URL ?>assets/images/icons/car-solid.svg" alt="Generic placeholder image">-->
                                <span class="badge badge-primary badge-light">Área Principal</span>
                                <span class="badge badge-primary badge-light"><?php echo $imovel->areaprincipal; ?></span>
                            </li>
                        </ul>
                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-3">
                       
                            <div class="container-fluid" style="background-color:var(--cor-primaria)  ;padding-top: 33px ; padding-bottom: 10px; border-radius: 10px; color: white; ">
                                <div class="form">
                                    <div id="sendmessage">
                                        <h4 class='text-center'>Fale agora com a <?php echo TITULO_AUXILIAR ?></h4>
                                    </div>
                                    <div id="errormessage"></div>
                                    <form action="#" method="post" role="form" class="contactForm">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <input id="name-lead" type="text" name="name" class="form-control" placeholder="Nome" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                                <div class="validation"></div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <input id="email-lead" type="email" class="form-control" name="email" placeholder="Email" data-rule="email" data-msg="Please enter a valid email" />
                                                <div class="validation"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input id="tel-lead" type="text" class="form-control" name="subject" placeholder="Telefone" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                                            <div class="validation"></div>
                                        </div>
                                        <div class="form-group">
                                            <textarea id="text-lead" class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Mensagem"><?php echo "Olá, gostaria de ter mais informações sobre o imóvel de código " . $imovel->codigo . ". Aguardo retorno, obrigado." ?> </textarea>
                                            <div class="validation"></div>
                                        </div>

                                        <div class="form-group">
                                            <div id="alert-leed" style="display:none" class="alert alert-success" role="alert">
                                                Informações enviados com sucesso
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <div id="alert-leed-danger" style="display:none" class="alert alert-danger" role="alert">
                                                Preencha todos os campos do formulário
                                            </div>

                                        </div>

                                        <div class="form-check">


                                            <input id="check-lead" class="form-check-input" type="checkbox" value="ss">
                                            <label class="form-check-label" for="defaultCheck1">
                                                Já sou cliente
                                            </label>
                                            </br>
                                        </div>
                                        <div class="text-center"><button id="button-lead" type="button" class="btn btn-primary  btn-block" style="    color: #fff;
                                                                        background-color: var(--cor-primaria);
                                                                        border-color: #ffffff;
                                                                        margin-top: 10px;
                                                                        ">Enviar Mensagem</button></div>
                                        <p style="margin-top: 10px;">Ao contatar o anunciante, você concorda com os Termos de Uso e Política de Privacidade .</p>
                                    </form>
                                </div>

                            </div>

                        <div class="row" style="padding:20px">
                            <div class="col-12">
                                <h4 style="color:var(--cor-primaria)">Compartilhar</h4>
                            </div>
                            <div class="col-4" style="padding-top: 13px;">
                                <div class="fb-share-button" data-href="<?php echo BASE_URL . $_SERVER["REQUEST_URI"]   ?>" data-layout="box_count"></div>
                            </div>
                            <div class="col-4">
                                <a href="https://api.whatsapp.com/send?text=<?php echo BASE_URL . $_SERVER["REQUEST_URI"]   ?>">
                                    <p class="icon-compartilhar"><i class="fab fa-whatsapp"></i></p>
                                </a>
                            </div>

                        </div>


                    </div>

                </div>
            </div>
        </section>


        <!--     ==========================
             IMAGENS DA GALERIS EXPANDIDA
           ============================-->


        <section id="portfolio" class="wow fadeInUp">

            <div class="container-fluid">
                <div class="row">
                    <!--href="https://www.google.com/maps/place/<?php //echo $imovel->latitude   
                                                                ?>,<?php echo $imovel->longitude ?>;-->

                    <h3><img class="mr-1 iconeListaCardListagem" src="<?php echo BASE_URL ?>assets/images/icons/mark-baixa.png" alt="Generic placeholder image">
                        <?php echo $imovel->bairro ?> - <span><?php echo $imovel->cidade ?> - <?php echo $imovel->estado ?></span></h3>
                </div>
                <div class="row no-gutters">

                    <!--GALERIA DE FOTOS EXPANDIDA-->
                    <?php foreach ($imovel->fotos as $key => $value) : ?>
                        <div class="col-lg-3 col-md-4">
                            <div class="portfolio-item wow fadeInUp" style="display:none">
                                <a href="<?php echo $value->url; ?>" class="portfolio-popup">
                                    <img src="<?php echo $value->url ?>" alt="">
                                    <div class="portfolio-overlay">
                                        <div class="portfolio-info">
                                            <h2 class="wow fadeInUp"><?php echo $value->descricao; ?></h2>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>


        <!--  ==========================
         ======= FIM IMAGENS DA GALERIS EXPANDIDA =======
              ============================      -->


        <!--==========================
         PORQUE A IMOVEIS
       ============================-->
        <section id="testimonials" class="wow fadeInUp">
            <div class="container-fluid">
                <div class="row">
                    <?php if ($imovel->urlvideo != "") : ?>
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <h2 style="color: var(--cor-primaria) ;"><b>VÍDEO DO IMÓVEL</b></h2>
                            <?php if ($imovel->urlvideo != "") : ?>
                                <iframe src="<?php echo ($imovel->urlvideo != "") ? $imovel->urlvideo : 'https://www.youtube.com/embed/5e0LxrLSzok' ?>" width="100%" height="324" autoplay="0" frameborder="0" scrolling="no" allowfullscreen></iframe>
                            <?php endif ?>

                        </div>
                    <?php endif ?>
                    <?php if ($imovel->urlpublica != "") : ?>
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <h2 style="color: var(--cor-primaria) ;"><b>TOUR VIRTUAL</b></h2>
                            <?php if ($imovel->urlpublica != "") : ?>
                                <iframe src="<?php echo ($imovel->urlpublica != "") ? $imovel->urlpublica : 'https://www.youtube.com/embed/5e0LxrLSzok' ?>" width="100%" height="324" autoplay="0" frameborder="0" scrolling="no" allowfullscreen></iframe>
                            <?php endif ?>

                        </div>
                    <?php endif ?>
                </div>
            </div>
        </section>
        <section id="testimonials" class="wow fadeInUp">
            <div class="container-fluid">
                <div class="row">
                    <!--                                    <div class="col-lg-8">-->
                    <div class="col-lg-6">
                        <div class="section-header">
                            <h2 style="margin-top:0px">Detalhes do imóvel</h2>
                            <p> <?php
                                echo nl2br($imovel->descricao);
                                ?>
                            </p>
                        </div>
                        <?php
                        if (
                            $imovel->arcondicionado != 0 || $imovel->areaservico != 0 || $imovel->closet != 0 || $imovel->mobiliado != 0 || $imovel->churrasqueira != 0 || $imovel->hidromassagem != 0 || $imovel->piscina != 0 || $imovel->quadraesportiva != 0 || $imovel->interfone != 0 || $imovel->portaria24horas != 0 || $imovel->playground != 0 || $imovel->areaprivativa != 0 || $imovel->areaprivativa != 0
                        ) :
                        ?>

                        <?php endif ?>
                    </div>

                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section-header">
                                    <h2 style="color:var(--cor-primaria);margin-top: 0px;">CARACTERÍSTICAS</h2>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item d-flex justify-content-between align-items-center">

                                        <img class="mr-1 iconeListaCardListagem" src="<?php echo BASE_URL ?>assets/images/icons/lazer.svg" alt="Generic placeholder image">
                                        <span class="badge badge-primary badge-light">Area de Lazer</span>
                                        <span class="badge badge-primary badge-light"><?php echo ($imovel->areaprivativa == true) || ($imovel->piscina == true) || ($imovel->playground == true) || ($imovel->quadraesportiva == true) ? "Sim" : "Não"; ?></span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        <img class="mr-1 iconeListaCardListagem" src="<?php echo BASE_URL ?>assets/images/icons/portaria.svg" alt="Generic placeholder image">
                                        <span class="badge badge-primary badge-light">Portaria 24 horas</span>
                                        <span class="badge badge-primary badge-light"><?php echo $imovel->portaria24horas == true ? "Sim" : "Não"; ?></span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        <img class="mr-1 iconeListaCardListagem" src="<?php echo BASE_URL ?>assets/images/icons/elevador.svg" alt="Generic placeholder image">
                                        <span class="badge badge-primary badge-light">Numero Elevador</span>
                                        <span class="badge badge-primary badge-light"><?php echo $imovel->numeroelevador; ?></span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        <img class="mr-1 iconeListaCardListagem" src="<?php echo BASE_URL ?>assets/images/icons/document.svg" alt="Generic placeholder image">
                                        <span class="badge badge-primary badge-light">Valor IPTU</span>
                                        <span class="badge badge-primary badge-light"><?php echo $imovel->valoriptu; ?></span>
                                    </li>
                            </div>
                        </div>
                    </div>


                    <div class="col-12">
                        <div class="properties-amenities" style="margin-top: 40px ">
                            <div class="section-header">
                                <h2>Comodidades</h2>
                            </div>
                            <div class="row">
                                <?php
                                if (
                                    $imovel->arcondicionado != 0 || $imovel->areaservico != 0 || $imovel->closet != 0 || $imovel->mobiliado != 0 || $imovel->churrasqueira != 0
                                ) :
                                ?>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <ul class="amenities">
                                            <?php if ($imovel->arcondicionado == 1) : ?>
                                                <li>
                                                    <i class="fa fa-check-square"></i> Ar Condicionado
                                                </li>
                                            <?php endif ?>
                                            <?php if ($imovel->areaservico == 1) : ?>
                                                <li>
                                                    <i class="fa fa-check-square"></i>Área de Serviço
                                                </li>
                                            <?php endif ?>
                                            <?php if ($imovel->closet == 1) : ?>
                                                <li>
                                                    <i class="fa fa-check-square"></i>Closet
                                                </li>
                                            <?php endif ?>
                                            <?php if ($imovel->mobiliado == 1) : ?>
                                                <li>
                                                    <i class="fa fa-check-square"></i>Mobiliado
                                                </li>
                                            <?php endif ?>
                                            <?php if ($imovel->churrasqueira == 1) : ?>
                                                <li>
                                                    <i class="fa fa-check-square"></i>Churrasqueira
                                                </li>
                                            <?php endif ?>
                                        </ul>
                                    </div>
                                <?php endif ?>
                                <?php
                                if (
                                    $imovel->hidromassagem != 0 || $imovel->piscina != 0 || $imovel->quadraesportiva != 0 || $imovel->salaofestas != 0
                                ) :
                                ?>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <ul class="amenities">
                                            <?php if ($imovel->hidromassagem == 1) : ?>
                                                <li>
                                                    <i class="fa fa-check-square"></i>Hidromassagem
                                                </li>
                                            <?php endif ?>
                                            <?php if ($imovel->piscina == 1) : ?>
                                                <li>
                                                    <i class="fa fa-check-square"></i>Piscina
                                                </li>
                                            <?php endif ?>
                                            <?php if ($imovel->quadraesportiva == 1) : ?>
                                                <li>
                                                    <i class="fa fa-check-square"></i>Quadra Esportiva
                                                </li>
                                            <?php endif ?>
                                            <?php if ($imovel->salaofestas == 1) : ?>
                                                <li>
                                                    <i class="fa fa-check-square"></i>Salão de festa
                                                </li>
                                            <?php endif ?>
                                        </ul>
                                    </div>
                                <?php endif ?>

                                <?php
                                if (
                                    $imovel->interfone != 0 || $imovel->portaria24horas != 0 || $imovel->playground != 0 || $imovel->areaprivativa != 0
                                ) :
                                ?>

                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <ul class="amenities">
                                            <?php if ($imovel->interfone == 1) : ?>
                                                <li>
                                                    <i class="fa fa-check-square"></i>Interfone
                                                </li>
                                            <?php endif ?>
                                            <?php if ($imovel->portaria24horas == 1) : ?>
                                                <li>
                                                    <i class="fa fa-check-square"></i>Portaria 24 Horas
                                                </li>
                                            <?php endif ?>
                                            <?php if ($imovel->playground == 1) : ?>
                                                <li>
                                                    <i class="fa fa-check-square"></i>Playground
                                                </li>
                                            <?php endif ?>
                                            <?php if ($imovel->areaprivativa == 1) : ?>
                                                <li>
                                                    <i class="fa fa-check-square"></i>Área Privativa
                                                </li>
                                            <?php endif ?>
                                        </ul>
                                    </div>
                                <?php endif ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>




        <!--        <div class="container-fluid">
                    <div class="row">
        
        <?php //if ($imovel->fotos360[0]->url != "") :  
        ?>
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <h4 style="color:#189dc3">Foto 360º</h4>
                                <div id="map"  style="height: 324px;width: 600px; "></div>
                            </div>
        <?php //endif  
        ?>
                        <script>
        
                            function initPano() {
                                // Set up Street View and initially set it visible. Register the
                                // custom panorama provider function. Set the StreetView to display
                                // the custom panorama 'reception' which we check for below.
                                var panorama = new google.maps.StreetViewPanorama(
                                        document.getElementById('map'), {
                                    pano: 'reception',
                                    visible: true,
                                    panoProvider: getCustomPanorama,
                                    pov: {
                                        heading: 355,
                                        pitch: 0,
                                        zoom: 0.0
                                    },
                                });
                            }
        
                            // Return a pano image given the panoID.
                            function getCustomPanoramaTileUrl(pano, zoom, tileX, tileY) {
                                // Note: robust custom panorama methods would require tiled pano data.
                                // Here we're just using a single tile, set to the tile size and equal
                                // to the pano "world" size.
                                console.log(zoom);
                                return '<?php //echo $imovel->fotos360[0]->url       
                                        ?>';
                            }
        
                            // Construct the appropriate StreetViewPanoramaData given
                            // the passed pano IDs.
                            function getCustomPanorama(pano, zoom, tileX, tileY) {
                                if (pano === 'reception') {
                                    return {
                                        //                location: {
                                        //                    pano: 'reception',
                                        //                    description: 'Universal Software 2016'
                                        //                },
                                        links: [],
                                        // The text for the copyright control.
                                        copyright: 'Universal Software',
                                        // The definition of the tiles for this panorama.
                                        tiles: {
                                            tileSize: new google.maps.Size(1024, 512),
                                            worldSize: new google.maps.Size(1024, 512),
                                            centerHeading: 625,
                                            getTileUrl: getCustomPanoramaTileUrl
                                        }
                                    };
                                }
                            }
        
        
                        </script>
                    </div>
                </div>-->




        <section id="team" class="wow fadeInUp">
            <div class="container-fluid">
                <div class="section-header text-center">
                    <h2>Você também pode gostar</h2>
                </div>

                <div class="row">
                    <div class="owl-carousel testimonials-carousel imoveis-destaque">


                        <?php foreach ($imoveisRelacionados as $key => $value) : ?>

                            <a href="<?php echo BASE_URL ?>imoveis/<?php echo $value->titulo ?>/<?php echo $value->codigo ?>">

                                <div class="testimonial-item corpo-card">
                                    <div class="member">
                                        <div class="pic"><img src="<?php echo ($value->urlfotoprincipal != '') ? $value->urlfotoprincipal : 'https://www.imoview.com.br/demo/Front/img/house1.png' ?>" alt=""></div>

                                        <div class="valor-card-destaque">
                                            <h5 class="valor-texto"><?php echo $value->valor ?></h5>
                                        </div>
                                        <div class="details">
                                            <div class="social">
                                                <ul class="list-group list-group-flush corpoListDetalhe">
                                                    <li class="list-group-item d-flex  justify-content-between align-items-center">
                                                        <span class="badge badge-primary badge-light"></span>
                                                        <span class="badge badge-primary badge-light"><?php echo $value->tipo ?></span>
                                                        <span class="badge badge-primary badge-light"></span>
                                                    </li>
                                                    <li class="list-group-item d-flex  justify-content-between align-items-center">
                                                        <span class="badge badge-primary badge-light"></span>
                                                        <span class="badge badge-primary badge-light"><?php echo $value->bairro ?> | <?php echo $value->cidade ?> </span>
                                                        <span class="badge badge-primary badge-light"></span>
                                                    </li>
                                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <img class="mr-1 iconeListaCardListagem" src="<?php echo BASE_URL ?>assets/images/icons/quartos.svg" alt="Generic placeholder image">
                                                            <div class="col txt-card-itens">
                                                                <span class="badge badge-primary badge-light"><?php echo $value->numeroquartos ?></span>
                                                                <span class="badge badge-primary badge-light text-cards">Quartos</span>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <img class="mr-1 iconeListaCardListagem" src="<?php echo BASE_URL ?>assets/images/icons/banheiro1.png" alt="Generic placeholder image">
                                                            <div class="col txt-card-itens">
                                                                <span class="badge badge-primary badge-light"><?php echo $value->numerobanhos ?></span>
                                                                <span class="badge badge-primary badge-light text-cards">Banheiros</span>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex justify-content-between align-items-center">

                                                            <img class="mr-1 iconeListaCardListagem" src="<?php echo BASE_URL ?>assets/images/icons/car-solid.svg" alt="Generic placeholder image">

                                                            <div class="col txt-card-itens">
                                                                <span class="badge badge-primary badge-light"><?php echo $value->numerovagas ?></span>
                                                                <span class="badge badge-primary badge-light text-cards">Vagas</span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <!--                                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                                                                                <img class="mr-1 iconeListaCardListagem"  src="<?php //echo BASE_URL                                       
                                                                                                                                                                                ?>assets/images/icons/preco.svg" alt="Generic placeholder image">
                                                                            
                                                                                                                                <span class="badge badge-primary badge-light" style="font-size:20px"><?php //echo $value->valor                                       
                                                                                                                                                                                                        ?></span>
                                                                                                                                <span class="badge badge-primary badge-light "></span>
                                                                                                                            </li>-->
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </a>

                        <?php endforeach; ?>

                    </div>
                </div>

            </div>
        </section>



        <section id="testimonials" class="wow fadeInUp">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="container">
                    <div class="section-header">
                        <h2 class="text-center">FALE AGORA COM A <?php echo TITULO_AUXILIAR ?></h2>
                    </div>
                    <div class="form">
                        <div id="sendmessage">
                            <h4 class='text-center'>Gostou do Imóvel ? Entre em contato com a gente.</h4>
                        </div>
                        <div id="errormessage"></div>
                        <form action="" method="post" role="form" class="contactForm">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <input id="name-lead-2" type="text" name="name" class="form-control" id="name" placeholder="Nome" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group col-md-6">
                                    <input id="email-lead-2" type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Entre com o email" />
                                    <div class="validation"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input id="tel-lead-2" type="text" class="form-control" name="subject" id="tel" placeholder="Telefone" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                                <div class="validation"></div>
                            </div>
                            <div class="form-group">
                                <textarea id="text-lead-2" class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Mensagem"><?php echo "Olá, gostaria de ter mais informações sobre o imóvel de código " . $imovel->codigo . ". Aguardo retorno, obrigado." ?></textarea>
                                <div class="validation"></div>
                            </div>
                            <div class="form-group">
                                <div id="alert-leed2" style="display:none" class="alert alert-success" role="alert">
                                    Informações enviados com sucesso
                                </div>

                            </div>
                            <div class="form-group">
                                <div id="alert-leed-danger2" style="display:none" class="alert alert-danger" role="alert">
                                    Preencha todos os campos do formulário
                                </div>

                            </div>
                            <div class="text-center"><button id="button-lead-2" class="btn btn-primary" type="button" style="background-color: var(--cor-primaria); border-color: var(--cor-primaria);">Enviar Mensagem</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- #testimonials -->


        <!--==========================
          About Section
        ============================-->

        <section class="area-call-to-action-mapa">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 content">
                        <h2 class="text-center h3-branco h3-cinza">Conte com a <?php echo NOME_IMOBILIARIA ?> para encontrar o seu imóvel e negociá-lo pelo melhor preço de mercado</h2>
                    </div>

                    <div class="col-md-6">
                        <div class="contact-phone">
                            <h2 class="text-center h3-branco h3-cinza">Aqui voce encontra o imóvel ideal diretamente no mapa</h2>

                            <div class="text-center "><a href="<?php echo BASE_URL ?>busca"><button class="btn btn-outline-dark btBuscar" type="submit">Buscar pelo mapa</button></a></div>
                        </div>
                    </div>
                </div>
            </div>

            </div>
        </section>



        <?php include 'includes/contatos.php'; ?>


    </main>


    <?php include('includes/whatsapp.php'); ?>
    <?php include('includes/footer.php'); ?>



    <!-----------------------------MODAL PARA EXIBIR BUSCA POR CÓDIGO---------------------------------------- -->
    <div class="modal fade modal-fullscreen force-fullscreen " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="overflow:auto">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center tituloModal">Imóveis encontrados</h4>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <!--                        <button id="fecharModal" type="button" class="btn btn-danger">Fechar</button>-->
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 conteudoModal">
                            </div>
                            <div class="col-md-4 ml-auto">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!--<button id="fecharModal" type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                    <!--                <button id="fecharModal" type="button" class="btn btn-primary">Fechar</button>-->
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!--   ---------------------------MODAL PARA EXIBIR BUSCA POR CÓDIGO---------------------------------------- -->

    <script src="<?php echo BASE_URL; ?>assets/js/base_url.js"></script>


    <!--LINK PARA TOPO DA PAGINA;-->
    <!--<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>-->

    <!-- JavaScript Libraries -->
    <script src="<?php echo BASE_URL; ?>assets/lib/jquery/jquery.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/jquery/jquery-migrate.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/easing/easing.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/superfish/hoverIntent.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/superfish/superfish.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/wow/wow.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/owlcarousel/owl.carousel.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/magnific-popup/magnific-popup.min.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/sticky/sticky.js"></script>
    <script src="<?php echo BASE_URL; ?>assets/lib/mask/jquery.mask.js"></script>



    <!-- Contact Form JavaScript File -->
    <script src="<?php echo BASE_URL; ?>assets/js/contactform/contactform.js"></script>

    <!-- importação da api google maps -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaEyYifvU0mZmsnFnGRWkvhqha0gKmmVY&callback=initMap" type="text/javascript"></script>

    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->

    <script src="<?php echo BASE_URL; ?>/assets/bootstrap/js/bootstrap.js"></script>


    <!--import filtro por movimentação do mapa-->
    <!--<script src="<?php //echo BASE_URL;                                
                        ?>assets/api-maps/filtro-mapa.js"></script>-->

    <!-- Template Main Javascript File -->
    <script src="<?php echo BASE_URL; ?>assets/js/main.js"></script>


    <!--BUSCAR DE IMOVEIS-->
    <!--<script src="<?php //echo BASE_URL;                   
                        ?>assets/js/busca/busca.js"></script>-->

    <!--    <script src="--><?php //echo BASE_URL; 
                            ?>
    <!--assets/js/home/busca.js"></script>-->

    <!--auto complite do input busca por endereço-->
    <!--<script src="<?php //echo BASE_URL;                               
                        ?>assets/api-maps/auto-complete-mapa.js"></script>;-->
    <!--<script src="<?php // BASE_URL;                                
                        ?>assets/js/imoveis-em-destaque.js"></script>-->


    <!--FORMULARIO CONTATOS-->
    <!--    <script src="--><?php //echo BASE_URL; 
                            ?>
    <!--assets/js/contatos/contato.js"></script>-->

    <!--FORMULARIO Imoveis-->
    <!--    <script src="--><?php //echo BASE_URL; 
                            ?>
    <!--assets/js/anunciar/anunciar.js"></script>-->

    <!--FORMULARIO HOME-->
    <!--    <script src="--><?php //echo BASE_URL; 
                            ?>
    <!--assets/js/home/form_busca_home.js"></script>-->

    <!--MODAL DE WATS APP-->
    <script src="<?php echo BASE_URL; ?>assets/js/watsapp/watsapp.js"></script>

    <!--SCRIPT FORMULARIO DE LEAD-->
    <script src="<?php echo BASE_URL; ?>assets/js/formlead/leads.js"></script>

</body>



<!--//SCROOL DETALHES-->
<script src="<?php echo BASE_URL; ?>assets/js/detalhes/detalhes.js"></script>




<script>
    //
    //
    //
    ///REDIRECIONAMENTO PAR OS DESTAQUES VINDOS DE OUTRA PAGINA





    //                                        $(document).ready(function () {
    //                                            try {
    //
    //                                                var url_direct = window.location.href.slice(window.location.href.indexOf('=') + 1);
    //
    //                                            } catch (e) {
    //
    //
    //                                            }
    //
    //                                            if (url_direct == 'aluguel_destaque') {
    //
    //                                                $('html, body').animate({scrollTop: $('#aluguel_destaque').offset().top}, 'slow');
    //
    //
    //                                            } else if (url_direct == 'venda_destaque') {
    //
    //                                                $('html, body').animate({scrollTop: $('#imoveis_venda').offset().top}, 'slow');
    //
    //
    //                                            } else if (url_direct == 'lancamento_destaque') {
    //
    //                                                $('html, body').animate({scrollTop: $('#imoveis_lancament').offset().top}, 'slow');
    //
    //
    //                                            } else {
    //
    //                                            }
    //
    //                                        });


    //        $('#fecharModal').click(function(){
    //
    //            $('#myModal').toogkke
    //
    //        });








    $('.carousel-item').click(function() {

        setTimeout(function() {

            $('.mfp-arrow-right').trigger('click');

        }, 1000)


    })
</script>
<?php include('includes/scripts_bottom.php') ?>
</html>