<?php

require 'environment.php';
global $config;
global $db;

//CHAVE DEMONSTRAÇÃO UNIVERSAL - NÃO SUBSTITUIR, APENAS COMENTAR
define("CHAVE", "zspa19PLlW97CeFcdBLM/L89d5YwVm8EnUAHdlO5Lfk=");

//DEFINE URL LOCAL OU PRODUCAO
define("LOCAL_PRODUCAO","producao");

//CHAVE DO CLIENTE
// define("CHAVE", "qxJT1tMz/jSPy/Qkc28wQABz4Kh6/5TgNG5uhjzLQBY=");

//CRECI
define("CRECI", "0000-J");

//CONFIGURAÇÕES
define("EXIBIR_MAPA", "S");

//ÁREA DO CLIENTE
define("AREA_CLIENTE", "#");
//TÍTULO ABA SITE
define("TITULO_AUXILIAR", "Site Demonstração Universal software");
//NOME DA IMOBILIÁRIA
define("NOME_IMOBILIARIA","Site Demonstração Universal software");
//INFORMAÇÕES CONTATOS SITE
//define("EMAIL", "contato@lamoradaimoveis.com.br");
define("EMAIL", "comercial@universalsoftware.com.br");
//FIXO
define("TELEFONE_FIXO","(31) 3064-6600");
define("TELEFONE_FIXO_SCRIPT","(31) 3064-6600");
define("TELEFONE2", "");
define("TELEFONE2_SCRIPT", "");
//CELULAR
define("WHATSAPP", "(31) 3064-6600");
define("CELULAR", "(31) 3064-6600");
define("CELULAR_SCRIPT", "(31) 3064-6600");
//CRECI

//ENDERECO
define("ENDERECO", "Av. Barão Homem de Melo, 4500 - Estoril, Belo Horizonte - MG");
define("LINK_ENDERECO", "");

define("CIDADE_ESTADO", "Belo Horizonte/MG");
//REDES SOCIAIS
define("FACEBOOK", "#");
define("INSTAGRAM", "#");
define('LINKEDIN','#');
define('TWITTER','#');
define('YOUTUBE','#');
//WHATSAPP


//URL API
define("URLAPI", "https://api.imoview.com.br/");

$config = array();

define('BASE_URL', 'http://modelo_diamond.test/');

// META SEO
define("META_TITLE","Encontre seu imóvel na ". TITULO_AUXILIAR);
define("KEYWORDS","Imobiliária em " . CIDADE_ESTADO);
define("DESCRIPTION","Para alugar ou comprar apartamentos, casas, terrenos consulte a ". TITULO_AUXILIAR .". Muitas opções de imóveis à venda ou locação em ". CIDADE_ESTADO ." | ". TITULO_AUXILIAR);

$config['default_lang'] = 'en';
?>
