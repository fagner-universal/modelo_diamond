
<?php


class Leads_model extends Model {
    
    public $chave =  CHAVE;
    
    private $api;


    public function __construct($api) {
        parent::__construct();
        
        $this->api = $api;
     
    }
    

    public function incluirLeads($nome,$tel,$email,$midia,$codigoimovel,$text) {

//        echo $nome;exit;
        
        $resultado = $this->api->POST($this->urlApi.'/Lead/IncluirLead?parametros={"nome":"' . $nome . '","telefone":"' . $tel . '", "email":"' . $email . '", "midia":"' . $midia . '","codigoimovel":"'.$codigoimovel.'","anotacoes":"'.$text.'"}',  $this->chave);

        return $resultado;
    }

    
    }
    