<?php

//finalidade: OBRIGATÓRIO - Enviar 1 para ALUGUEL ou 2 para VENDA
//codigounidade: OPCIONAL - Enviar código da unidade ou vazio para todas
//codigosimoveis: OPCIONAL - Enviar os códigos dos imóveis separados por vírgula (,) ou vazio para todos
//codigoTipo: OPCIONAL - Enviar o código do tipo de imóvel selecionado de acordo com a lista existente (RetornarTiposImoveisDisponiveis), para mais de um tipo, separar por vírgula (,) ou vazio para todos
//codigocidade: OPCIONAL - Enviar o código da cidade selecionada de acordo com a lista existente (RetornarCidadesDisponiveis) ou 0 para todos
//codigoregiao: OPCIONAL - Enviar o código da região selecionada de acordo com a lista existente (RetornarRegioesDisponiveis) ou 0 para todos
//codigosbairros: OPCIONAL - Enviar os códigos dos bairros selecionados de acordo com a lista existente (RetornarBairrosDisponiveis) separados por vírgula (,) ou vazio para todos
//endereco: OPCIONAL - Enviar parte do logradouro do endereço ou vazio para todos
//numeroquartos: OPCIONAL - Enviar nº de quartos a partir, 0 para todos
//numerovagas: OPCIONAL - Enviar nº de vagas a partir, 0 para todos
//numerobanhos: OPCIONAL - Enviar nº de banheiros a partir, 0 para todos
//numerosuite: OPCIONAL - Enviar nº de suítes a partir, 0 para todos
//numerovaranda: OPCIONAL - Enviar nº de varandas a partir, 0 para todos
//numeroelevador: OPCIONAL - Enviar nº de elevadores a partir, 0 para todos
//valorde: OPCIONAL - Enviar valor a partir, 0 para todos
//valorate: OPCIONAL - Enviar valor até, 0 para todos
//areade:	OPCIONAL - Enviar área a partir, 0 para todos
//areaate: OPCIONAL - Enviar área até, 0 para todos
//extras: OPCIONAL - Enviar código gerado no CRM para o campo extra, separados por vírgula (,) ou vazio para não filtrar
//interfone: OPCIONAL - Enviar true ou false
//mobiliado: OPCIONAL - Enviar true ou false
//dce: OPCIONAL - Enviar true ou false
//piscina: OPCIONAL - Enviar true ou false
//sauna: OPCIONAL - Enviar true ou false
//salaofestas: OPCIONAL - Enviar true ou false
//academia: OPCIONAL - Enviar true ou false
//boxDespejo: OPCIONAL - Enviar true ou false
//portaria24h: OPCIONAL - Enviar true ou false
//aceitafinanciamento: OPCIONAL - Enviar true ou false
//arealazer: OPCIONAL - Enviar true ou false
//quartoqtdeexata: OPCIONAL - Enviar true ou false
//vagaqtdexata: OPCIONAL - Enviar true ou false
//destaque: OPCIONAL - Enviar 1 para simples, 2 para destaque ou 3 para super destaque, 0 para todos
//opcaoimovel: OPCIONAL - Enviar 1 para somente avulsos, 2 para somente lançamentos, 3 para unidades de lançamentos, 4 para avulsos e lançamentos mãe, 0 para todos (avulsos e lançamentos por tipo e m²)
//retornomapa: OPCIONAL - Enviar true ou false, usado para exibir os imóveis no mapa (retorno com até 100 registros e JSON reduzido)
//retornomapaapp: OPCIONAL - Enviar true ou false, usado para exibir os imóveis no mapa (retorno com até 100 registros e JSON reduzido)
//numeropagina: OBRIGATÓRIO - Usado para paginação, enviar o nº da página atual
//numeroregistros: OBRIGATÓRIO - Usado máximo de imóveis para retorno, máximo 50
//ordenacao: OPCIONAL - Tipo de ordenação, valorasc para valor crescente, valordesc para valor decrescente, ou vazio para assumir destaque decrescente

class Destaques_model extends Model {

    public $chave = CHAVE;
    private $api;
    private $finalidade; //OPCIONAL - Enviar 1 para ALUGUEL, 2 para VENDA ou 0 para todos
    private $numeroRegistros = 20;

    public function __construct($api, $finalidade) {
        parent::__construct();

        $this->api = $api;
        $this->finalidade = $finalidade;
    }

    public function getImoveisDestaques() {

        $resultado = $this->api->GET($this->urlApi . 'Imovel/RetornarImoveisDisponiveis?parametros={"finalidade":"' . $this->finalidade . '","numeroPagina":"1","numeroRegistros":"' . $this->numeroRegistros . '","destaque":"2","retornodestaque":"true"}', $this->chave);
        return $resultado;
    }

    public function getImoveisLancamentos() {
        $resultado = $this->api->GET($this->urlApi . 'Imovel/RetornarImoveisDisponiveis?parametros={"finalidade":"' . $this->finalidade. '","numeroPagina":"1","numeroRegistros":"' . $this->numeroRegistros . '","opcaoimovel":"2","retornodestaque":"true"}', $this->chave);

        return $resultado;
    }

    //RETORNA IMOVEIS EM LANCAMENTO
    public function setFinalidade($finalidade) {
        $this->finalidade = $finalidade;
    }

    public function getFinalidade() {
        return $this->finalidade;
    }

}
