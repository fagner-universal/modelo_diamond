<?php

class Depoimentos_model extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function getDepoimentos() {

        $resultado = array();

        $query = "SELECt * FROM depoimentos";

        $sql = $this->db->prepare($query);

        $sql->execute();

        if ($sql->rowCount() > 0) {

            $resultado = $sql->fetchAll();
        }
        
  

        return $resultado;
    }

}
