<?php

class Funcionario extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function getFuncionaros($cargo) {

        $resultado = array();


        $query = "SELECT funcionario.id,funcionario.nome as nome_funcioario,"
                . "funcionario.email,"
                . "funcionario.foto,"
                . "funcionario.wats,"
                . "funcionario.descricao,"
                . "funcionario.telefone,"
                . " cargo.nome as nome_cargo,"
                . " cargo.id as id_cargo from funcionario
                    INNER JOIN cargo
                    on funcionario.id_cargo = cargo.id
                    WHERE cargo.id = :cargo
                    GROUP BY funcionario.nome";


        $sql = $this->db->prepare($query);
        $sql->bindValue(':cargo', $cargo);
        $sql->execute();

        if ($sql->rowCount() > 0) {

            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        }


        return $resultado;
    }

    public function getFuncionario($funcionario,$id_cargo) {



        $resultado = array();

//        SELECT * FROM funcionario
//        INNER JOIN cargo
//        on funcionario.id = id_cargo
//        WHERE funcionario.id = 1
//        GROUP BY funcionario.nome





        $query = "SELECT  funcionario.id,funcionario.nome as nome_funcioario,"
                . "funcionario.email,"
                . "funcionario.foto,"
                . "funcionario.descricao,"
                . "funcionario.telefone,"
                . " cargo.nome as nome_cargo,"
                . " cargo.id as id_cargo  FROM funcionario
                  INNER JOIN cargo
                  on funcionario.id_cargo = cargo.id
                WHERE funcionario.id = :id
                GROUP BY funcionario.nome";




        $sql = $this->db->prepare($query);
        $sql->bindValue(':id', $funcionario);
//        $sql->bindValue(':id_cargo', $id_cargo);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        }
        

       

        return $resultado;
    }

}
