<?php

class Condominio_Model extends Model {

    private $api;
    private $finalidade; //OPCIONAL - Enviar 1 para ALUGUEL, 2 para VENDA ou 0 para todos
    private $codigoCidade; // OPCIONAL - Enviar o código da cidade selecionada de acordo com a lista existente (RetornarCidadesDisponiveis) ou 0 para todos
    private $opcaoimovel; //OPCIONAL - Enviar 1 para somente avulsos, 2 para somente lançamentos, 3 para unidades de lançamentos, 4 para avulsos e lançamentos mãe, 0 para todos (avulsos e lançamentos por tipo e m²)

    public function __construct($api, $finalidade) {
        parent::__construct();

        $this->api = $api;
        $this->finalidade = $finalidade;
    }

    public function getCondominios() {
        $resultado = $this->api->GET($this->urlApi .'Imovel/RetornarCondominiosDisponiveis?parametros={"finalidade":"' . $this->finalidade . '","retornoReduzido":"true"}', $this->chave);
        return $resultado = json_decode($resultado);
    }
}