<?php

class Buscar extends Model {

    public function __construct() {
        parent::__construct();
    }

    public function buscar($parametro) {

        $resultado = array();

        $query = "SELECt * FROM imoveis inner join cidade on cidade.id = imoveis.id_cidade where cidade.nome LIKE '" . $parametro . "%'";

        $sql = $this->db->prepare($query);
        $sql->bindValue(":param", $parametro);

        $sql->execute();

        if ($sql->rowCount() > 0) {

            $resultado = $sql->fetchAll();
        }

        return $resultado;
    }

}
