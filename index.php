<?php

session_start();
require 'config.php';
require 'routers.php';

spl_autoload_register(function ($class){
    if(file_exists('controllers/'.$class.'.php')) { 
            require_once 'controllers/'.$class.'.php';
            
    } elseif(file_exists('models/'.$class.'.php')) {
            require_once 'models/'.$class.'.php';
    } elseif(file_exists('core/'.$class.'.php')) {
            require_once 'core/'.$class.'.php';
    }
    elseif(file_exists('classes/'.$class.'.php')) {
            require_once 'classes/'.$class.'.php';
    }
    elseif(file_exists('interface/'.$class.'.php')) {
            require_once 'interface/'.$class.'.php';
    }
});


$core = new Core();
$core->run();
?>